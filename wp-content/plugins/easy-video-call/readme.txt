=== Easy Video Call [GWE]===
Contributors: mukulwp
Donate link: https://getwebexperts.com/
Tags: video call, easy video call, agora, agora video call, group video call
Requires at least: 4.7
Tested up to: 6.3.0
Stable tag: 1.0.8
Requires PHP: 7.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Easy Video Call is a simple plugin for making video call easily. To display the video call option simply add this [easy-video-call] shortcode inside your desired location.

== Description ==

Easy Video Call plugin was created using Agora’s API. To use this plugin, you have to create an account on agora.com. Note that without an SSL certificate on your website, this plugin won’t work.



== Features==

* Through a shortcode, you can display the ‘Start Call’ button anywhere on your website.
* You can turn on or off your camera.
* You can mute or unmute your speaker.



== Frequently Asked Questions ==

= How can I get app id? =

To get an app id, create an account on agora.com. Then go to the console and create a new project. Note that while creating a project you must set the Authentication Mechanis Testing mode: APP ID. Otherwise, it will not work.

= Does it work with any theme? =

Yes, it will work with any theme.

= Is there any shortcode to use it in a page or post? =

Yes. You can use this shortcode [easy-video-call] to use it in a page or post or anywhere on your website.


== Screenshots ==

1. Plugin Setting Page.
2. Plugin Shortcode.
3. Frontend View.
4. Video Call Running