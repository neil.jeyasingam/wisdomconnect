<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<?php astra_content_bottom(); ?>
	</div> <!-- ast-container -->
	</div><!-- #content -->
<?php 
	astra_content_after();
		
	astra_footer_before();
		
	astra_footer();
		
	astra_footer_after(); 
?>
	</div><!-- #page -->
<?php 
	astra_body_bottom();    
	wp_footer(); 
?>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    jQuery(document).ready(function($) {
     $('#open_verification_modal').click(function() {
      $('#verification_modal').show();
    });
		
	$('#senior_verification_form').submit(function(e) {
    e.preventDefault();
    var formElement = document.getElementById("senior_verification_form");
    var formData = new FormData(formElement);
	  formData.append('action', 'handle_senior_card_upload');
	  for (var pair of formData.entries()) {
    console.log(pair[0] + ', ' + pair[1]);
     }
    console.log(formData)
    $.ajax({
        url: '/wordpress/wp-admin/admin-ajax.php',  
        type: 'POST',
		   data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
            if (response.success) {
                alert('Upload Successful！');
            } else {
                alert('Upload Failed: ' + response.data);
            }
        },
        error: function() {
            alert('Upload Failed！');
        }
    });
   });
	$('#close_upload_modal').click(function() {
        $('#verification_modal').hide();
    });
});
	// Upload image through camera capture
	jQuery(document).ready(function($) {
    var video = document.getElementById('camera_stream');
    var canvas = document.getElementById('photo_canvas');
    var context = canvas.getContext('2d');

    $('#open_camera_modal').click(function() {
        $('#camera_modal').show();
    });

    // activate the camera
    $('#start_camera').click(function() {
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                video.srcObject = stream;
                video.play();
            }).catch(function(err) {
                alert("Error starting the camera: " + err.message);
            });
        }
    });

    // Capture
    $('#capture_photo').click(function() {
        context.drawImage(video, 0, 0, 400, 300);
        canvas.style.display = "block"; 
        $('#submit_captured_photo').show(); 
    });

    // Upload captured image
    $('#submit_captured_photo').click(function() {
        var imageData = canvas.toDataURL('image/png');
        $.ajax({
            url: '/wordpress/wp-admin/admin-ajax.php',
            type: 'POST',
            data: {
                action: 'handle_senior_card_capture',
                image_data: imageData
            },
            success: function(response) {
                if (response.success) {
                    alert('Image uploaded successfully!');
                } else {
                    alert('Upload failed: ' + response.data);
                }
            },
            error: function() {
                alert('Upload failed!');
            }
        });
    });
		 $('#close_camera_modal').click(function() {
        $('#camera_modal').hide();
    });
});

</script>






	</body>
</html>
