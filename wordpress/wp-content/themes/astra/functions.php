<?php
/**
 * Astra functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Define Constants
 */
define( 'ASTRA_THEME_VERSION', '4.1.4' );
define( 'ASTRA_THEME_SETTINGS', 'astra-settings' );
define( 'ASTRA_THEME_DIR', trailingslashit( get_template_directory() ) );
define( 'ASTRA_THEME_URI', trailingslashit( esc_url( get_template_directory_uri() ) ) );
define( 'ASTRA_PRO_UPGRADE_URL', 'https://wpastra.com/pro/?utm_source=dashboard&utm_medium=free-theme&utm_campaign=upgrade-now' );
define( 'ASTRA_PRO_CUSTOMIZER_UPGRADE_URL', 'https://wpastra.com/pro/?utm_source=customizer&utm_medium=free-theme&utm_campaign=upgrade' );

/**
 * Minimum Version requirement of the Astra Pro addon.
 * This constant will be used to display the notice asking user to update the Astra addon to the version defined below.
 */
define( 'ASTRA_EXT_MIN_VER', '4.1.0' );

/**
 * Setup helper functions of Astra.
 */
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-theme-options.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-theme-strings.php';
require_once ASTRA_THEME_DIR . 'inc/core/common-functions.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-icons.php';

/**
 * Update theme
 */
require_once ASTRA_THEME_DIR . 'inc/theme-update/astra-update-functions.php';
require_once ASTRA_THEME_DIR . 'inc/theme-update/class-astra-theme-background-updater.php';

/**
 * Fonts Files
 */
require_once ASTRA_THEME_DIR . 'inc/customizer/class-astra-font-families.php';
if ( is_admin() ) {
	require_once ASTRA_THEME_DIR . 'inc/customizer/class-astra-fonts-data.php';
}

require_once ASTRA_THEME_DIR . 'inc/lib/webfont/class-astra-webfont-loader.php';
require_once ASTRA_THEME_DIR . 'inc/customizer/class-astra-fonts.php';

require_once ASTRA_THEME_DIR . 'inc/dynamic-css/custom-menu-old-header.php';
require_once ASTRA_THEME_DIR . 'inc/dynamic-css/container-layouts.php';
require_once ASTRA_THEME_DIR . 'inc/dynamic-css/astra-icons.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-walker-page.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-enqueue-scripts.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-gutenberg-editor-css.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-wp-editor-css.php';
require_once ASTRA_THEME_DIR . 'inc/dynamic-css/block-editor-compatibility.php';
require_once ASTRA_THEME_DIR . 'inc/dynamic-css/inline-on-mobile.php';
require_once ASTRA_THEME_DIR . 'inc/dynamic-css/content-background.php';
require_once ASTRA_THEME_DIR . 'inc/class-astra-dynamic-css.php';
require_once ASTRA_THEME_DIR . 'inc/class-astra-global-palette.php';

/**
 * Custom template tags for this theme.
 */
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-attr.php';
require_once ASTRA_THEME_DIR . 'inc/template-tags.php';

require_once ASTRA_THEME_DIR . 'inc/widgets.php';
require_once ASTRA_THEME_DIR . 'inc/core/theme-hooks.php';
require_once ASTRA_THEME_DIR . 'inc/admin-functions.php';
require_once ASTRA_THEME_DIR . 'inc/core/sidebar-manager.php';

/**
 * Markup Functions
 */
require_once ASTRA_THEME_DIR . 'inc/markup-extras.php';
require_once ASTRA_THEME_DIR . 'inc/extras.php';
require_once ASTRA_THEME_DIR . 'inc/blog/blog-config.php';
require_once ASTRA_THEME_DIR . 'inc/blog/blog.php';
require_once ASTRA_THEME_DIR . 'inc/blog/single-blog.php';

/**
 * Markup Files
 */
require_once ASTRA_THEME_DIR . 'inc/template-parts.php';
require_once ASTRA_THEME_DIR . 'inc/class-astra-loop.php';
require_once ASTRA_THEME_DIR . 'inc/class-astra-mobile-header.php';

/**
 * Functions and definitions.
 */
require_once ASTRA_THEME_DIR . 'inc/class-astra-after-setup-theme.php';

// Required files.
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-admin-helper.php';

require_once ASTRA_THEME_DIR . 'inc/schema/class-astra-schema.php';

/* Setup API */
require_once ASTRA_THEME_DIR . 'admin/includes/class-astra-api-init.php';

if ( is_admin() ) {
	/**
	 * Admin Menu Settings
	 */
	require_once ASTRA_THEME_DIR . 'inc/core/class-astra-admin-settings.php';
	require_once ASTRA_THEME_DIR . 'admin/class-astra-admin-loader.php';
	require_once ASTRA_THEME_DIR . 'inc/lib/astra-notices/class-astra-notices.php';
}

/**
 * Metabox additions.
 */
require_once ASTRA_THEME_DIR . 'inc/metabox/class-astra-meta-boxes.php';

require_once ASTRA_THEME_DIR . 'inc/metabox/class-astra-meta-box-operations.php';

/**
 * Customizer additions.
 */
require_once ASTRA_THEME_DIR . 'inc/customizer/class-astra-customizer.php';

/**
 * Astra Modules.
 */
require_once ASTRA_THEME_DIR . 'inc/modules/posts-structures/class-astra-post-structures.php';
require_once ASTRA_THEME_DIR . 'inc/modules/related-posts/class-astra-related-posts.php';

/**
 * Compatibility
 */
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-gutenberg.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-jetpack.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/woocommerce/class-astra-woocommerce.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/edd/class-astra-edd.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/lifterlms/class-astra-lifterlms.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/learndash/class-astra-learndash.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-beaver-builder.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-bb-ultimate-addon.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-contact-form-7.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-visual-composer.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-site-origin.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-gravity-forms.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-bne-flyout.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-ubermeu.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-divi-builder.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-amp.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-yoast-seo.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-starter-content.php';
require_once ASTRA_THEME_DIR . 'inc/addons/transparent-header/class-astra-ext-transparent-header.php';
require_once ASTRA_THEME_DIR . 'inc/addons/breadcrumbs/class-astra-breadcrumbs.php';
require_once ASTRA_THEME_DIR . 'inc/addons/scroll-to-top/class-astra-scroll-to-top.php';
require_once ASTRA_THEME_DIR . 'inc/addons/heading-colors/class-astra-heading-colors.php';
require_once ASTRA_THEME_DIR . 'inc/builder/class-astra-builder-loader.php';

// Elementor Compatibility requires PHP 5.4 for namespaces.
if ( version_compare( PHP_VERSION, '5.4', '>=' ) ) {
	require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-elementor.php';
	require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-elementor-pro.php';
	require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-web-stories.php';
}

// Beaver Themer compatibility requires PHP 5.3 for anonymus functions.
if ( version_compare( PHP_VERSION, '5.3', '>=' ) ) {
	require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-beaver-themer.php';
}

require_once ASTRA_THEME_DIR . 'inc/core/markup/class-astra-markup.php';

/**
 * Load deprecated functions
 */
require_once ASTRA_THEME_DIR . 'inc/core/deprecated/deprecated-filters.php';
require_once ASTRA_THEME_DIR . 'inc/core/deprecated/deprecated-hooks.php';
require_once ASTRA_THEME_DIR . 'inc/core/deprecated/deprecated-functions.php';

// Ensure you have the hashing function available
if(!function_exists('hash')) {
    // Install the Hash extension for PHP or switch to another hashing method
    return;
}

function generate_meeting_name() {
    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;

    // key: A fixed secret string to add extra unpredictability.
    $key = '$2a$12$yIMrGz8zNjF1aoY2Wlzy7e';

    // Generate a hash based on user ID and key
    $hash = hash('sha256', $user_id . $salt);

    // Use the first few characters of the hash for the meeting URL, for example the first 10 chars
    $meeting_name = "FriendMeeting_" . substr($hash, 0, 10);

    return $meeting_name;
}


function jitsi_video_with_user_id() {
    $current_user = wp_get_current_user();
    
    if (0 == $current_user->ID) {
        return 'You are not logged in. If you have successfully logged in, please refresh the page.';
    } else {
        $user_id = $current_user->ID;
        $meeting_name = generate_meeting_name();
        
        return '<div class="wp-block-jitsi-meet-wp-jitsi-meet jitsi-wrapper" data-name="' . $meeting_name . '" data-width="1080" data-height="720" data-mute="false" data-videomute="false" data-screen="false" data-invite="false" style="width:1080px"></div>';
    }
}

add_shortcode('jitsi_video', 'jitsi_video_with_user_id');

function jitsi_video_link() {
    $current_user = wp_get_current_user();
    if ( 0 == $current_user->ID ) {
        return 'You are not logged in. If you have successfully logged in, please refresh the page.';
    } else {
        $user_id = $current_user->ID;
			$meeting_name = generate_meeting_name();
        $url = 'https://8x8.vc/meeting/' . $meeting_name;
        return 'Invite you to have a video chat: <a href="' . $url . '">' . 'Click Here' . '</a>';
    }
}
add_action('init', 'send_video_link_to_friend');

function send_video_link_to_friend() {
    error_log('Video Check');
    if (isset($_POST['friend_username'])) {
        // Get username
        $friend_username = sanitize_text_field($_POST['friend_username']);
        error_log('Form submitted. Friend username: ' . $friend_username);
        
        // Get current user friend list
        $current_user = wp_get_current_user();
        $friend_user = get_user_by('login', $friend_username);
        
        // Get uid via username
        $friend_user_id = $friend_user->ID;
        
        // Check user id 
        if ($friend_user_id) {
            error_log('Friend user ID: ' . $friend_user_id);
        } else {
            error_log('Friend user not found');
        }
        
        if ($friend_user && class_exists('ProfileMagic_Chat')) {
            // Generate Video URL
			    $video_link = jitsi_video_link();
            error_log('Video Link:' . $video_link);
            // use ProfileGrid function to send message
            $pm = new ProfileMagic_Chat();
            $is_msg_sent = $pm->pm_messenger_send_new_message($friend_user_id, $video_link);
            
            if ($is_msg_sent) {
                error_log('Message sent successfully');
            } else {
                error_log('Failed to send message');
            }
        }
    }
}

// Upload Senior Card image
function handle_senior_card_upload() {

    $uploadedfile = $_FILES['senior_card'];
    $upload_overrides = array('test_form' => false);
    $movefile = wp_handle_upload($uploadedfile, $upload_overrides);
	  error_log('handle_senior_card_upload function was called.');
	  error_log(print_r($_FILES, true));
    if ($movefile && !isset($movefile['error'])) {
		$current_user_id = get_current_user_id();
      update_user_meta($current_user_id, 'senior_card_url', $movefile['url']);
      wp_send_json_success();
    } else {
        wp_send_json_error($movefile['error']);
    }
}
add_action('wp_ajax_handle_senior_card_upload', 'handle_senior_card_upload');

function handle_senior_card_capture() {
    if(isset($_POST['image_data'])) {
        $image_data = $_POST['image_data'];
        $upload_dir = wp_upload_dir();
        $image_data = str_replace('data:image/png;base64,', '', $image_data);
        $image_data = str_replace(' ', '+', $image_data);
        $data = base64_decode($image_data);
        $file_name = 'senior_card_' . uniqid() . '.png';
        $file_path = $upload_dir['path'] . '/' . $file_name;
        file_put_contents($file_path, $data);
        $file_url = $upload_dir['url'] . '/' . $file_name;

        // Save image for user
        $current_user_id = get_current_user_id();
        update_user_meta($current_user_id, 'senior_card_url', $file_url);
        
        wp_send_json_success();
    } else {
        wp_send_json_error('No image data received.');
    }
}
add_action('wp_ajax_handle_senior_card_capture', 'handle_senior_card_capture');

// Show user vertification status
function verification_status_shortcode() {
    // Ensure user is logged in
    if (!is_user_logged_in()) {
        return '';  // Return empty if user not logged in
    }

    $current_user = wp_get_current_user();
    $senior_card_url = get_user_meta($current_user->ID, 'senior_card_url', true);
    $verified = get_user_meta($current_user->ID, 'verified', true);

	 // Check if senior_card_url exists, if not, set verified to false
    if (!$senior_card_url) {
        $verified = false;
    }
    $html = '<div class="verification-status">';
    
    if ($verified) {
        $image_src = get_stylesheet_directory_uri() . '/images/vertification_yes.png';
		   $message = 'Verified as an Senior User';
	} else {
        $image_src = get_stylesheet_directory_uri() . '/images/vertification_no.png';
        $message = 'Not Yet Verified as an Senior User';
    }

    $html .= '<img src="' . esc_url($image_src) . '" alt="Verification status" style="vertical-align: middle;" />';
    $html .= '<span style="margin-left: 10px;">' . esc_html($message) . '</span>';
    $html .= '</div>';

    return $html;
}
add_shortcode('verification_status', 'verification_status_shortcode');


