<?php
/*
 * To display lowest rated users rating in widget. 
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (!class_exists('Profilegrid_Groups_Slider_Widget')) :

    class Profilegrid_Groups_Slider_Widget extends WP_Widget {
        /*
         *  registers basic widget information.
         */

        public function __construct() {
            $widget_options = array(
                'classname' => 'pg_groups_slider_widget',
                'description' => __('The widget will display groups in slider.', 'profilegrid-group-slider'),
            );
            parent::__construct('pg_groups_slider_widget', __('Profilegrid Groups Slider', 'profilegrid-group-slider'), $widget_options);
        }

        public function form($instance) { 
            $title = !empty($instance['title']) ? $instance['title'] : '';
            $auto_play = !empty($instance['auto_play']) ? $instance['auto_play'] : '5';
            $profile_link = !empty($instance['profile_link']) ? $instance['profile_link'] : '5';
            $slide = !empty($instance['slide']) ? $instance['slide'] : '5';

            //$dots = !empty($instance['dots']) ? $instance['dots'] : '5';
            $gid = !empty($instance['gid']) ? $instance['gid'] : '';
            $dbhandler = new PM_DBhandler;
            //print_r($gid);
            $groups = $dbhandler->get_all_result('GROUPS');
            ?>

            <p>
                <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'profilegrid-group-slider'); ?>
                    <input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr($title); ?>" />
                </label>
            </p>

            <p>
                <label for="<?php echo $this->get_field_id('gid'); ?>"><?php _e('Select groups to show:', 'profilegrid-group-slider'); ?></label>
                <select multiple class="widefat" id="<?php echo $this->get_field_id('gid'); ?>" name="<?php echo $this->get_field_name('gid') . '[]'; ?>">
                    <?php foreach ($groups as $group):
                        
                        ?>
                        <option value="<?php echo $group->id; ?>" <?php if ($gid<>""){if(is_array($instance['gid'])){ if(in_array($group->id, $gid)) {
                    echo 'selected';
                        }}} ?>><?php echo $group->group_name; ?></option> 
            <?php endforeach; ?>
                </select>
            </p>

            <p>
                <input name="<?php echo $this->get_field_name('auto_play'); ?>" id="<?php echo $this->get_field_id('auto_play'); ?>" type="checkbox" <?php checked($auto_play, '1'); ?>  value="1"/>
                <label for="<?php echo $this->get_field_id('auto_play'); ?>"><?php _e('Auto play', 'profilegrid-group-slider'); ?></label> <br>
                <input name="<?php echo $this->get_field_name('profile_link'); ?>" id="<?php echo $this->get_field_id('profile_link'); ?>" type="checkbox" <?php checked($profile_link, '1'); ?> value="1"  />
                <label for="<?php echo $this->get_field_id('profile_link'); ?>"><?php _e('Profile Link', 'profilegrid-group-slider'); ?></label> 
            </p>

            <p>
                <label for="<?php echo $this->get_field_id('slide'); ?>"><?php _e('Slides to show:', 'profilegrid-group-slider'); ?></label>
                <select class="widefat" id="<?php echo $this->get_field_id('slide'); ?>" name="<?php echo $this->get_field_name('slide'); ?>">
                    <?php for ($slide = 1; $slide <= 5; $slide++) { ?>
                        <option value="<?php echo $slide; ?>" <?php if(isset($instance['slide'])){if ($slide == $instance['slide']) {
                    echo 'selected';
                        }} ?>><?php echo $slide; ?></option> 
            <?php } ?>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('slide_to_scroll'); ?>"><?php _e('No. of slides to scroll when the slider moves', 'profilegrid-group-slider'); ?></label>
                <select class="widefat" id="<?php echo $this->get_field_id('slide_to_scroll'); ?>" name="<?php echo $this->get_field_name('slide_to_scroll'); ?>">
            <?php for ($slide_to_scroll = 1; $slide_to_scroll <= 5; $slide_to_scroll++) { ?>
                        <option value="<?php echo $slide_to_scroll; ?>" <?php if(isset($instance['slide_to_scroll'])){if ($slide_to_scroll == $instance['slide_to_scroll']) {
                    echo 'selected';
                        }} ?>><?php echo $slide_to_scroll; ?></option> 
            <?php } ?>
                </select>
            </p>
            
            <?php
        }

        public function widget($args, $instance) {

            $title = apply_filters('widget_title', $instance['title']);
            //$selected_groups = $instance['gid'];
            $profile_link  = $instance['profile_link'];
            $auto_play = $instance['auto_play'];  
            $slides_to_show = $instance['slide'];
            $slide_to_scroll = $instance['slide_to_scroll'];
            //$dots = $instance['dots'];
            $dots = 'false';
//             if($instance['dots'] == 1)
//             {
//                $dots ='true';
//             }
//             else
//             {
//                 $dots = 'false';
//             }
            // before and after widget arguments are defined by themes
            echo $args['before_widget'];
            if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];
            $profilegrid_group_slider = '';
            $version = '';
            $public_class = new Profilegrid_groups_slider_Public($profilegrid_group_slider, $version);
            $dbhandler = new PM_DBhandler;
            //globel saved groups. 
            
            //$selected_groups_global = maybe_unserialize($dbhandler->get_global_option_value('pm_selected_groups',array()));
            if($instance['gid']== 0)
            {
                $select_groups = maybe_unserialize($dbhandler->get_global_option_value('pm_selected_groups',array()));
                
            }
            else
            {
                $select_groups = $instance['gid'];
            }
            
            
            if (class_exists('Profile_Magic')) {
                if ($dbhandler->get_global_option_value('pm_enable_group_slider') == 1) {
 
                    $public_class->show_groups_slider($select_groups, $profile_link, $auto_play, $slide_to_scroll, $slides_to_show, $dots, $args['widget_id']);
                }
            }

            echo $args['after_widget'];
        }

        public function update($new_instance, $old_instance) {
            //$instance = array();
            $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
            $instance['gid'] = (!empty($new_instance['gid']) ) ? esc_sql($new_instance['gid']) : '';
            $instance['profile_link'] = (!empty($new_instance['profile_link']) ) ? strip_tags($new_instance['profile_link']) : 0;
            $instance['auto_play'] = (!empty($new_instance['auto_play']) ) ? strip_tags($new_instance['auto_play']) : 0;  
            $instance['slide_to_scroll'] = (!empty($new_instance['slide_to_scroll']) ) ? strip_tags($new_instance['slide_to_scroll']) : 0;
            $instance['slide'] = (!empty($new_instance['slide']) ) ? strip_tags($new_instance['slide']) : 0;
            //$instance['dots'] = (!empty($new_instance['dots']) ) ? strip_tags($new_instance['dots']) : 0;
            return $instance;
        }

    }

    
endif;