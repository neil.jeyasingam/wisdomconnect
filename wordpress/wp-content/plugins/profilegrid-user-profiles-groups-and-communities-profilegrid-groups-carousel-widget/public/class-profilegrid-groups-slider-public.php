<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Profilegrid_groups_slider
 * @subpackage Profilegrid_groups_slider/public
 */
/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Profilegrid_groups_slider
 * @subpackage Profilegrid_groups_slider/public
 * @author     Your Name <email@example.com>
 */

/**
 * short code variables 
 * ------------------------
 * Group_Ids_Show = "1,2,3"
 * Slides_To_Scroll = "1"
 * Slides_To_Show = "10" 
 * Dots = "1"
 * group_link="1"
 * pm_auto_play = "1"
 *  */
class Profilegrid_groups_slider_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $Profilegrid_groups_slider    The ID of this plugin.
     */
    private $Profilegrid_groups_slider;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $Profilegrid_groups_slider       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($Profilegrid_groups_slider, $version) {

        $this->Profilegrid_groups_slider = $Profilegrid_groups_slider;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Profilegrid_groups_slider_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Profilegrid_groups_slider_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->Profilegrid_groups_slider, plugin_dir_url(__FILE__) . 'css/profilegrid-groups-slider-public.css', array(), $this->version, 'all');
        wp_enqueue_style('pg_groups_slick_css', plugin_dir_url(__FILE__) . 'slick/slick.css', array(), $this->version, 'all');
        wp_enqueue_style('pg_groups_slick_theme', plugin_dir_url(__FILE__) . 'slick/slick-theme.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Profilegrid_groups_slider_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Profilegrid_groups_slider_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script('jquery');
        wp_enqueue_script('pg_groups_slick', plugin_dir_url(__FILE__) . 'slick/slick.js', array('jquery'), $this->version, false);
        wp_enqueue_script($this->Profilegrid_groups_slider, plugin_dir_url(__FILE__) . 'js/profilegrid-groups-slider-public.js', array(), $this->version, true);
        
        
    }

    public function pg_groups_slider_widget() {
        register_widget('Profilegrid_Groups_Slider_Widget');
    }

    public function register_shortcodes() {
        add_shortcode('profilegrid_groups_slider', array($this, 'profilegrid_groups_slider_fun'));
    }
    
    public function show_groups_slider($group_ids_show, $profile_link, $auto_play, $slides_to_scroll_fun, $slides_to_show, $dots, $arg)
    {   
        
        $dbhandler = new PM_DBhandler;
        $pmrequests = new PM_request;
        if($dbhandler->get_global_option_value('pm_enable_group_slider') == 1) 
        {
          
            if(!empty($group_ids_show))
            {
                $gids = implode(',',$group_ids_show);
            }
            else
            {   
            ?><div class='pg-alert-warning pg-alert-info'> <?php
                 _e('No groups selected. Please select groups to display in this slider from Global Settings. ', 'profilegrid-group-slider');
                ?> </div><?php
                
            }
            $identifier = 'GROUPS';
            if(!empty($group_ids_show))
            {
                $additional = "id in($gids)";
            } 
            if ($dbhandler->get_global_option_value('pm_enable_group_slider') == 1)
            {
                if(!empty($group_ids_show))
                {
                    $groups = $dbhandler->get_all_result($identifier,'*',1,'results',0,false,null,false, $additional);
                }
            }
       
            
            ?>
            <div id="pg_group_slider">
                <input type="hidden" id="slider_id" value="<?php echo $arg; ?>">
            </div>
            <section class="<?php echo $arg; ?> slider pg-group-slider">
                <?php 
                if(!empty($group_ids_show))
                {
                    foreach($groups as $group){
                    $group_page_url = $pmrequests->profile_magic_get_frontend_url('pm_group_page', '', $group->id);
                    $group_page_link = add_query_arg('gid', $group->id, $group_page_url);
                    $image = $pmrequests->profile_magic_get_group_icon($group, 'pm-group-badge');
                    ?>
                    <div> 
                        <div class="pg-group-slider-wrap">
                              <?php if($profile_link == 1)
                                    { ?>
                                    <a href='<?php echo esc_url($group_page_link); ?>' title="<?php echo $group->group_name; ?>" >
                                        
                              <?php }
                                        echo $image; 
                                    if($profile_link == 1)
                                    {?>    
                                    </a>
                                   <?php } 
                                    if($profile_link == 1)
                                    { ?>
                                        <a href='<?php echo esc_url($group_page_link); ?>' title="<?php echo $group->group_name; ?>" >
                              <?php } ?>    
                                        <div class="pg-slider-group-name"><?php echo $group->group_name; ?></div>   
                              <?php if($profile_link == 1)
                                    { ?>    
                                        </a>
                              <?php } ?>
                        </div>    
                    </div>
                <?php }
                }?>
            </section>
        <?php }?>

            <script type="text/javascript">
            jQuery(document).on('ready', function() {

              jQuery(".<?php echo $arg; ?>").pg_groups_slick({
                dots: <?php echo $dots; ?>,
                infinite: true,
                slidesToShow: <?php echo $slides_to_show; ?>,
                slidesToScroll: <?php echo $slides_to_scroll_fun;  ?>,
                autoplay:<?php echo $auto_play; ?>
              });

            });
            </script>
        <?php
    }
    public function profilegrid_groups_slider_fun($content) {
        ob_start();
        $this->enqueue_scripts();
        $this->enqueue_styles();
        $dbhandler = new PM_DBhandler;
        $pmrequests = new PM_request;
        
        if (!empty($content))
            extract($content);
        //print_r($content);
        if (isset($group_ids_show)) 
        {
            $group_ids = explode(',', $group_ids_show);    
        }
        else 
        {
            $group_ids = maybe_unserialize($dbhandler->get_global_option_value('pm_selected_groups', array()));
        }
        
        if (isset($content['group_link'])) 
        {
            if($content['group_link'] == 1)
            {
                $profile_link = 1;
            }
            else
            {
                $profile_link = 0;
            }           
        }
        else 
        { 
            if($dbhandler->get_global_option_value('pm_profile_link') == 1)
                {
                    $profile_link = 1;
                }
                else 
                {
                    $profile_link = 0;
                }
        }
        
        if (isset($content['auto_play'])) 
        {
            if($content['auto_play'] == 1)
            {
                $auto_play ='true';
            }
            else
            {
                $auto_play = 'false';
            }
        }
        else
        {
            if($dbhandler->get_global_option_value('pm_auto_play') == 1)
            {
                $auto_play = 'true';
            }
            else 
            {
                $auto_play = 'false';
            }
        }
        
        
        if (isset($content['slides_to_scroll'])) 
        {
            $slides_to_scrol2 = (int)$content['slides_to_scroll'];  
        }
        else
        {
            $slides_to_scrol2 = (int)$dbhandler->get_global_option_value('pm_no_of_groups_slide_click')[0];
        }
        
 
        if (isset($content['slides_to_show'])) 
        {
            $slides_to_show = $content['slides_to_show'];
        }
        else
        {
             $slides_to_show = maybe_unserialize($dbhandler->get_global_option_value('pm_selected_groups_slider', array()))[0];
        }

        if(isset($content['dots'])) 
        {
            if($content['dots'] == 1)
                {
                    $dots ='true';
                }
                else
                {
                    $dots = 'false';
                }
        } 
        else 
        {
            if($dbhandler->get_global_option_value('pm_slider_bottom_dots') == 1)
                {
                    $dots = 'true';
                }
                else 
                {
                    $dots = 'false';
                }
        }
 
        if (class_exists('Profile_Magic')) {
            if ($dbhandler->get_global_option_value('pm_enable_group_slider') == 1) {
                static $i = 1;
                $this->show_groups_slider($group_ids, $profile_link, $auto_play, $slides_to_scrol2, $slides_to_show, $dots, 'pg_shortcode_' . $i);
                $i++;
            }
        }
        
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
        
    }

}
