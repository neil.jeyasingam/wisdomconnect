<?php
$dbhandler = new PM_DBhandler;
$pmrequests = new PM_request;
$path =  plugin_dir_url(__FILE__);
$identifier = 'SETTINGS';
if(filter_input(INPUT_POST,'submit_settings'))
{
	$retrieved_nonce = filter_input(INPUT_POST,'_wpnonce');
	if (!wp_verify_nonce($retrieved_nonce, 'save_custom_notification_settings' ) ) die( 'Failed security check' );
	$exclude = array("_wpnonce","_wp_http_referer","submit_settings");
	if(!isset($_POST['pm_enable_group_slider'])) $_POST['pm_enable_group_slider'] = 0;
         if(!isset($_POST['pm_selected_groups'])) $_POST['pm_selected_groups'] = 0;
         if(!isset($_POST['pm_profile_link'])) $_POST['pm_profile_link'] = 0;
         if(!isset($_POST['pm_auto_play'])) $_POST['pm_auto_play'] = 0;
         if(!isset($_POST['pm_slider_bottom_dots'])) $_POST['pm_slider_bottom_dots'] = 0;
         if(!isset($_POST['pm_selected_groups_slider'])) $_POST['pm_selected_groups_slider'] = 0;
         if(!isset($_POST['pm_no_of_groups_slide_click'])) $_POST['pm_no_of_groups_slide_click'] = 0;
         if(!isset($_POST['pm_allow_selected_group_type'])) $_POST['pm_allow_selected_group_type'] = 0;
         
	$post = $pmrequests->sanitize_request($_POST,$identifier,$exclude);
	if($post!=false)
	{
		foreach($post as $key=>$value)
		{
			$dbhandler->update_global_option_value($key,$value);
		}
	}
	
	wp_redirect('admin.php?page=pm_settings');exit;
}
$selected_content_type = maybe_unserialize($dbhandler->get_global_option_value('pm_selected_groups',array()));
$no_of_groups_in_slider = maybe_unserialize($dbhandler->get_global_option_value('pm_selected_groups_slider',array())); 
$no_of_groups_slide_on_click = maybe_unserialize($dbhandler->get_global_option_value('pm_no_of_groups_slide_click',array())); 
$enable_group = $dbhandler->get_global_option_value('pm_allow_selected_group_type');
$all_groups = $dbhandler->get_global_option_value('pm_all_groups');

?>

<div class="uimagic">
  <form name="pm_groups_slider_settings" id="pm_groups_slider_settings" method="post">
    <!-----Dialogue Box Starts----->
    <div class="content">
      <div class="uimheader">
        <?php _e( 'Groups Carousel Widget Settings','profilegrid-group-slider' ); ?>
      </div>
     
      <div class="uimsubheader">
        <?php
		//Show subheadings or message or notice
		?>
      </div>
    
        <div class="uimrow">
        <div class="uimfield">
          <?php _e( 'Enable Group Slider','profilegrid-group-slider' ); ?>
        </div>
        <div class="uiminput">
           <input name="pm_enable_group_slider" id="pm_enable_group_slider" type="checkbox" <?php checked($dbhandler->get_global_option_value('pm_enable_group_slider','0'),'1'); ?> class="pm_toggle" value="1" style="display:none;"  onClick="pm_show_hide(this,'pm_custom_notifications_child')"/>
          <label for="pm_enable_group_slider"></label>
        </div>
        <div class="uimnote"><?php _e("Enable to turn-on the Group Slider Widget in Appearance &#8594; Widgets area of your Dashboard. ",'profilegrid-group-slider');?></div>
      </div>
        
          <div class="uimrow" >         
              
   
        <!-- start -->
        <div class="uimrow">
            <div class="uimfield">
              <?php _e( 'Select Groups to Display ','profilegrid-group-slider' );  ?>
            </div>
            <div class="uiminput pm_select_required">
            <select multiple name="pm_selected_groups[]" id="pm_selected_groups" >
                 <?php
                        $dbhandler = new PM_DBhandler;
                        $groups = $dbhandler->get_all_result('GROUPS');

                       
                        foreach ($groups as $group) 
                        {
                                ?>
                                    <option value="<?php echo $group->id; ?>" <?php if(is_array($selected_content_type)){if(in_array($group->id, $selected_content_type)){echo 'selected';}} ?> ><?php echo $group->group_name; ?></option>
                                <?php    
                        } ?>
               
            </select>
                <div class="errortext"></div>
            <p class="description"><?php _e('Press ctrl or ⌘ (in Mac) while clicking to assign multiple ProfileGrid Groups','profilegrid-group-slider') ?></p>
            </div>
            <div class="uimnote"><?php _e("Select the groups you wish to display in the slider. ",'profilegrid-group-slider');?></div>
        </div>
          
        <!-- end multiselect dropdown. -->
        
        
       <div class="uimrow">
            <div class="uimfield">
              <?php _e( 'Auto play ','profilegrid-group-slider' ); ?>
            </div>
            <div class="uiminput">
                <input name="pm_auto_play"   id="pm_auto_play" type="checkbox" <?php checked($dbhandler->get_global_option_value('pm_auto_play','0'),'1'); ?> class="pm_toggle" value="1" style="display:none;"  />
                <label for="pm_auto_play"></label>
            </div>
            <div class="uimnote"><?php _e("Automatically change slides on the slider after fixed interval. ",'profilegrid-group-slider');?></div>
        </div>
        
        
        <div class="uimrow">
            <div class="uimfield">
              <?php _e( 'Link Slides ','profilegrid-group-slider' ); ?>
            </div>
            <div class="uiminput">
                <input name="pm_profile_link" id="pm_profile_link" type="checkbox" <?php checked($dbhandler->get_global_option_value('pm_profile_link','0'),'1'); ?> class="pm_toggle" value="1" style="display:none;" />
                <label for="pm_profile_link"></label>
            </div>
            <div class="uimnote"><?php _e("Enable to link group images with individual group pages. ",'profilegrid-group-slider');?></div>
        </div>
        
        
        <div class="uimrow">
            <div class="uimfield">
              <?php _e( 'Show Slider Controls ','profilegrid-group-slider' ); ?>
            </div>
            <div class="uiminput">
                <input name="pm_slider_bottom_dots" id="pm_slider_bottom_dots" type="checkbox" <?php checked($dbhandler->get_global_option_value('pm_slider_bottom_dots','0'),'1'); ?> class="pm_toggle" value="1" style="display:none;" />
                <label for="pm_slider_bottom_dots"></label>
            </div>
            <div class="uimnote"><?php _e("Enable to show clickable slider dots below the slider. ",'profilegrid-group-slider');?></div>
        </div>
        
        <div class="uimrow">
            <div class="uimfield">
              <?php _e( 'Number of Groups to Display ','profilegrid-group-slider' );  ?>
            </div>
            <div class="uiminput">
            <select name="pm_selected_groups_slider[]" id="pm_selected_groups_slider">
                 <?php
                        
                        for($i=1; $i<=5; $i++) 
                        {
                                ?>
                                    <option value="<?php echo $i; ?>" <?php if(is_array($no_of_groups_in_slider)){if(in_array($i, $no_of_groups_in_slider)){echo 'selected';}} ?> ><?php echo $i; ?></option>
                                <?php    
                        } ?>
               
            </select>    
            </div>
            <div class="uimnote"><?php _e("Define the number of Groups the slider shows at a time. ",'profilegrid-group-slider');?></div>
        </div>
        <div class="uimrow">
            <div class="uimfield">
              <?php _e( 'Number of Groups on Scroll ','profilegrid-group-slider' );  ?>
            </div>
            <div class="uiminput">
            <select name="pm_no_of_groups_slide_click[]" id="pm_no_of_groups_slide_click">
                 <?php
                        
                        for ($i=1; $i<=5; $i++) 
                        {
                                ?>
                                    <option value="<?php echo $i; ?>" <?php if(is_array($no_of_groups_slide_on_click)){if(in_array($i, $no_of_groups_slide_on_click)){echo 'selected';}} ?> ><?php echo $i; ?></option>
                                <?php    
                        } ?>
               
            </select>
            </div>
            <div class="uimnote"><?php _e("Define the number of groups to scroll every time the slider moves. ",'profilegrid-group-slider');?></div>
        </div>
        
         
        </div>
        

      <div class="buttonarea"> 
          <a href="admin.php?page=pm_settings">
        <div class="cancel">&#8592; &nbsp;
          <?php _e('Cancel','profilegrid-group-slider');?>
        </div>
        </a>
        <?php wp_nonce_field('save_custom_notification_settings'); ?>
        <input type="submit" value="<?php _e('Save','profilegrid-group-slider');?>" name="submit_settings" id="submit_settings" />
        <div class="all_error_text" style="display:none;"></div>
      </div>
    </div>
   
  </form>
</div>


