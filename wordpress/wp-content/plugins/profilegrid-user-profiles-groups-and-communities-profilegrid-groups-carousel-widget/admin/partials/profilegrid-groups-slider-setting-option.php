<?php
$path =  plugin_dir_url(__FILE__);
?>
<div class="uimrow"> <a href="admin.php?page=pm_groups_slider_settings">
  <div class="pm_setting_image"> <img src="<?php echo $path;?>images/group-carousel-slider.png" class="options" alt="options"> </div>
  <div class="pm-setting-heading"> <span class="pm-setting-icon-title">
    <?php _e( 'User Groups Slider','profilegrid-group-slider' ); ?>
    </span> <span class="pm-setting-description">
    <?php _e( 'Turn Groups Carousel Widget on/off.', 'profilegrid-group-slider' ); ?>
    </span> </div>
  </a> </div>