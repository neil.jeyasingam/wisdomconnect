��    %      D      l      l     m     �     �     �  ,   �  ?   �     
       
   *     5  	   F     P     W  :   ^     �     �  [   �  7   
     B  ]   b     �     �     �  %   �       6   !     X     k     |     �     �  E   �     �       F        ^  �  r                0     8  *   A  E   l     �     �  	   �     �  
   �     �     	  J   	     X	     \	  r   m	  <   �	  .   
  k   L
      �
     �
     �
  0   �
     +  2   4     g     z  	   �     �     �     �  "   @     c  F   k     �   Can't get any meeting Click to set Code Connect Create a new Hello I am here! from this page Create intsant meetings with your clients or potential clients. Create meet! Create new meeting Created at Created meetings Date/Time Delete Domain From a mobile device, you'll need to install the Jitsi App Go! Hello I am here! Hello I am here! not created for security reasons (WP "nonce"). Refresh page and try again! If you are not using your own Jitsi servers, set it to  Insert code and click 'Connect' Insert shortcode <code>[show-meet]</code> in any page. Or create a new page and insert there. Insert your meeting code Instructions Invalid Code Link generated. Click Go! to connect. Loading Make a donation and help the development of the plugin Meeting created at Meeting created! Schedule it Scheduled Date Select date and time. Send your client/student/friend/... the meeting code and the page url Set meeting title (optional). Title You and your client/studen/friend/... put the meeting code in the page goliver79@gmail.com Language: es_ES
POT-Creation-Date: 2020-09-25 10:22+0000
Plural-Forms: nplurals=2; plural=n != 1;
PO-Revision-Date: 2020-09-25 10:22+0000
X-Generator: Loco https://localise.biz/
Project-Id-Version: Hello I am here!
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: Spanish (Spain)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Loco-Version: 2.4.3; wp-5.5.1 No existen reuniones Click para usar Código Conectar Crea una nueva reunión desde esta página Crea reuniones instantáneas con tus clientes o potenciales clientes. Crear reunión! Crear nueva reunión Creado el Reuniones creadas Fecha/Hora Borrar Dominio Desde un dispositivo móvil, necesitarás instalar la aplicación de Jitsi Go! Hello I am here! Hello I am here! no creado por cuestiones de seguridad (WP 'nonce'). ¡Actualiza la página e inténtalo de nuevo! Si no estás usando tus propios servidores Jitsi, déjalo en Introduce el código y haz click en 'Conectar' Inserta el shortcode <code>[show-meet]</code> en alguna página. O crea una página nueva e insértalo ahí Introduce tu código de reunión Instrucciones Código no válido Enlace generado. Haz click en Go! para conectar. Cargando Haz una donación y ayuda al desarrollo del plugin Reunión creada el ¡Reunión creada! Programar Fecha programada Selecciona fecha y hora Envía a tu cliente/estudiante/amigo/... el código de reunión y la dirección de la página donde está el shortcode anterior Título de la reunión (opcional). Título Tú y tu cliente/estudiante/amigo/... ponéis el código en la página goliver79@gmail.com 