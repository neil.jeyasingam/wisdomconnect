��    %      D      l      l     m     �     �     �  ,   �  ?   �     
       
   *     5  	   F     P     W  :   ^     �     �  [   �  7   
     B  ]   b     �     �     �  %   �       6   !     X     k     |     �     �  E   �     �       F        ^  �  r                )  	   .  +   8  D   d     �     �     �     �  	   �     �     �  D   �     B	     H	  ]   Y	  E   �	  %   �	  i   #
     �
     �
     �
  1   �
  	   �
  6        9     K  	   [     e     u  v   �          $  A   +     m   Can't get any meeting Click to set Code Connect Create a new Hello I am here! from this page Create intsant meetings with your clients or potential clients. Create meet! Create new meeting Created at Created meetings Date/Time Delete Domain From a mobile device, you'll need to install the Jitsi App Go! Hello I am here! Hello I am here! not created for security reasons (WP "nonce"). Refresh page and try again! If you are not using your own Jitsi servers, set it to  Insert code and click 'Connect' Insert shortcode <code>[show-meet]</code> in any page. Or create a new page and insert there. Insert your meeting code Instructions Invalid Code Link generated. Click Go! to connect. Loading Make a donation and help the development of the plugin Meeting created at Meeting created! Schedule it Scheduled Date Select date and time. Send your client/student/friend/... the meeting code and the page url Set meeting title (optional). Title You and your client/studen/friend/... put the meeting code in the page goliver79@gmail.com Language: ca
POT-Creation-Date: 2020-09-25 10:22+0000
Plural-Forms: nplurals=2; plural=n != 1;
PO-Revision-Date: 2020-09-25 10:22+0000
X-Generator: Loco https://localise.biz/
Project-Id-Version: Hello I am here!
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: Catalan
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Loco-Version: 2.4.3; wp-5.5.1 No hi ha cap reunió Click per utilitzar Codi Connectar Crea una nova reunió des d'aquesta pàgina Crea reunions instantànees amb el teus clients o potencial clients. Crear reunió! Crear nova reunió Creat el Reunions creades Data/Hora Borrar Domini Des d'un dispositiu mòbil, teniu que instal·lar l'aplicació Jitsi Anar! Hello I am here! Reunió no creada per motius de seguretat (WP "nonce"). Refresca la pàgina i torna a provar! Si no estàs utilitzant els teus propis servidors de Jitsi, utilitza  Escriu el codi i clicka a "Connectar" Inserta el shortcode <code>[show-meet]</code> a alguna pàgina. O crea una nova pàgina i inserta'l allí Escriu el teu codi de reunió Instruccions Codi invàlid Enllaç generat. Fes click a Anar! per connectar. Carregant Fes una donació i ajuda al desenvolupament del plugin Reunió creada el Reunió creada! Programar Data programada Selecciona data i hora. Envía al teu client/estudiant/amic/... el codi de reunió y la direcció de la pàgina on està el shortcode anterior Títol de la reunió (opcional) Títol Tú i el teu client/estudiant/amic/... poseu el codi a la pàgina goliver79@gmail.com 