   (function( $ ) {
	'use strict';
   
   
   var featureGroup = $('.pg-featured-group-wrap').innerWidth();
    if (featureGroup <= 200) {
        $('.pg-featured-group-wrap').addClass('pg-featured-narrow');
    } else {
        $('.pg-featured-group-wrap').addClass('pg-featured-wide'); 
    }
    
    })(jQuery);