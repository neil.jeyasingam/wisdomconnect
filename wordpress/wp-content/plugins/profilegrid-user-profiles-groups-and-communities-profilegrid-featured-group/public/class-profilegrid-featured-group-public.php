<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Profilegrid_featured_group
 * @subpackage Profilegrid_featured_group/public
 */
/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Profilegrid_featured_group
 * @subpackage Profilegrid_featured_group/public
 * @author     Your Name <email@example.com>
 */

class Profilegrid_featured_group_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $Profilegrid_featured_group    The ID of this plugin.
     */
    private $Profilegrid_featured_group;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $Profilegrid_featured_group       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($Profilegrid_featured_group, $version) {

        $this->Profilegrid_featured_group = $Profilegrid_featured_group;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Profilegrid_featured_group_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Profilegrid_featured_group_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->Profilegrid_featured_group, plugin_dir_url(__FILE__) . 'css/profilegrid-featured-group-public.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Profilegrid_featured_group_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Profilegrid_featured_group_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script('jquery');
        wp_enqueue_script($this->Profilegrid_featured_group, plugin_dir_url(__FILE__) . 'js/profilegrid-featured-group-public.js', array(), $this->version, true);
        
        
    }

    public function pg_featured_group_widget() {
        register_widget('Profilegrid_Featured_Group_Widget');
    }

    public function show_featured_group( $group_ids_show, $show_join_group, $show_view_members, $arg )
    {   
        
        $dbhandler = new PM_DBhandler;
        $pmrequests = new PM_request;
        $current_user = wp_get_current_user();
        $gid = 'gid';
        
        if(!empty($group_ids_show))
            {
                $gid = implode( $group_ids_show );
            }
            else
            {   
            ?><div class='pg-alert-warning pg-alert-info'> <?php
                 _e('No group selected. Please select a group in the widgets area of dashboard, for Featured Group widget.', 'profilegrid-featured-group');
                ?> </div><?php
            }
            $identifier = 'GROUPS';
            if(!empty($group_ids_show))
            {
                $additional = "id in($gid)";
            } 
            
//            $groups = $dbhandler->get_all_result($identifier,'*',1,'results',0,false,null,false, $additional);
            
            $group = $dbhandler->get_row($identifier,$gid);
            
        ?>
        <div class="pmagic">
            <div class="<?php echo $arg; ?> pg-featured-group">
                <?php 
                if(!empty($group_ids_show))
                {
                    if($group){
                    $group_page_url = $pmrequests->profile_magic_get_frontend_url('pm_group_page', '', $group->id);
                    $group_page_link = add_query_arg('gid', $group->id, $group_page_url);
                    $image = $pmrequests->profile_magic_get_group_icon($group, 'pm-group-badge');
                    
                    ?>
                    <div class="pg-featured-group-wrap">
                        <div class="pg-featured-group-box">
                            
                        <div class="pg-featured-group-image">
                            <a href='<?php echo esc_url($group_page_link); ?>' title="<?php echo $group->group_name; ?>" >
                        <?php
                        echo $image; 
                        ?>    
                        </a>
                        </div>
                            
                        <div class="pg-featured-group-name"> 
                        <a href='<?php echo esc_url($group_page_link); ?>' title="<?php echo $group->group_name; ?>" >
                        <?php echo $group->group_name; ?></a></div>
                        
                        <div class="pg-featured-group-button">
                        
                        <?php if($show_join_group == 1)
                            {
                            
                            $pmrequests->profile_magic_get_join_group_button($gid,'yes');
                             
                             } ?>
                        
                        <?php if($show_view_members == 1)
                            { ?>
                        <div class="pg-view-member-button">
                        <div class="pm-group-signup">
                            <a class="pm_button" onclick="href='<?php echo esc_url($group_page_link); ?>'">
                            <button>View Members</button>
                            </a>
                        </div>
                            <!--<a href='<?php echo esc_url($group_page_link); ?>'>View Members</a>-->
                        </div>
                        <?php } ?>
                        
                        </div>
                        </div>    
                    </div>
                <?php }
                }?>
            </div>
        </div>
            <div class="pm-popup-mask"></div>    

            <div id="pm-edit-group-popup" style="display: none;">
                <div class="pm-popup-container" id="pg_edit_group_html_container">

                </div>
            </div>

        <?php
    }
}
