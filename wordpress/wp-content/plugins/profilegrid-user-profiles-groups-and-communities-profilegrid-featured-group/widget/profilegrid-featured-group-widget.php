<?php
/*
 * To display lowest rated users rating in widget. 
 */

if ( !defined ( 'ABSPATH' )  ) {
    exit; // Exit if accessed directly.
}

if ( !class_exists ( 'Profilegrid_Featured_Group_Widget' ) ) :

    class Profilegrid_Featured_Group_Widget extends WP_Widget {
        /*
         *  registers basic widget information.
         */

        public function __construct() {
            $widget_options = array(
                'classname'   => 'pg_featured_group_widget',
                'description' => __('Showcase a ProfileGrid membership group.', 'profilegrid-featured-group'),
            );
            parent::__construct('pg_featured_group_widget', __('ProfileGrid Featured Group', 'profilegrid-featured-group'), $widget_options);
        }

        public function form( $instance ) 
        { 
            $title = ! empty ( $instance [ 'title' ] ) ? $instance[ 'title' ] : '';
            $gid = ! empty ( $instance [ 'gid' ] ) ? $instance [ 'gid' ] : '';
            $show_join_group = ! empty ( $instance [ 'show_join_group' ] ) ? $instance [ 'show_join_group' ] : '0';
            $show_view_members = ! empty ( $instance [ 'show_view_members' ] ) ? $instance [ 'show_view_members' ] : '0';

            $dbhandler = new PM_DBhandler;
//            print_r($gid);
            $groups = $dbhandler->get_all_result('GROUPS');
            ?>

            <p>
                <label for="<?php echo $this->get_field_id ( 'title' ) ; ?>"><?php _e( 'Title:' , 'profilegrid-featured-group' ) ; ?>
                    <input type="text" class="widefat" id="<?php echo $this->get_field_id ( 'title' ) ; ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr($title); ?>" />
                </label>
            </p>

            <p>
                <label for="<?php echo $this->get_field_id ( 'gid' ); ?>"><?php _e('Select Featured Group to show:', 'profilegrid-featured-group' ) ; ?></label>
                <select class="widefat" id="<?php echo $this->get_field_id( 'gid' ) ; ?>" name="<?php echo $this->get_field_name( 'gid' ) . '[]'; ?>">
                    <?php foreach ( $groups as $group ):
                    ?>
                        <option value="<?php echo $group->id; ?>" <?php if ( $gid<>"" ){ if( is_array( $instance[ 'gid' ] ) ) { if (in_array ( $group->id, $gid ) ) {
                        echo 'selected';
                        } } } ?> ><?php echo $group->group_name; ?></option> 
                    <?php endforeach; ?>
                </select>
            </p>

            <p>                
                <input name="<?php echo $this->get_field_name ( 'show_join_group' ) ; ?>" class="checkbox widefat" id="<?php echo $this->get_field_id ( 'show_join_group' ) ; ?>" type="checkbox" <?php checked($show_join_group, '1'); ?> value="1"  />
                <label for="<?php echo $this->get_field_id ( 'show_join_group' ) ; ?>"><?php _e( 'Display ‘Join Group’ Button', 'profilegrid-featured-group' ); ?></label></br>
                <input name="<?php echo $this->get_field_name ( 'show_view_members' ) ; ?>" class="checkbox widefat" id="<?php echo $this->get_field_id ( 'show_view_members' ) ; ?>" type="checkbox" <?php checked($show_view_members, '1'); ?> value="1"  />
                <label for="<?php echo $this->get_field_id ( 'show_view_members' ) ; ?>"><?php _e( 'Display ‘View Members’ Button', 'profilegrid-featured-group' ); ?></label> 
            </p>
            
            <?php
        }

        public function widget( $args, $instance ) {

            $title = apply_filters ( 'widget_title' , $instance['title']);
            $select_group = $instance['gid'];
            $show_join_group  = $instance['show_join_group'];
            $show_view_members  = $instance['show_view_members'];

      
            // before and after widget arguments are defined by themes
            echo $args[ 'before_widget' ];
            if ( ! empty( $title ) )
            echo $args[ 'before_title' ] . $title . $args[ 'after_title' ];
            $profilegrid_featured_group = '';
            $version = '';
            $public_class = new Profilegrid_featured_group_Public( $profilegrid_featured_group, $version );
            $dbhandler = new PM_DBhandler;

            $public_class->show_featured_group( $select_group, $show_join_group, $show_view_members, $args[ 'widget_id' ] );
            echo $args[ 'after_widget' ] ;
        }

        public function update( $new_instance, $old_instance ) {
            $instance = array();
            $instance[ 'title' ] = ( ! empty( $new_instance[ 'title' ] ) ) ? strip_tags( $new_instance[ 'title' ] ) : '';
            $instance[ 'gid' ] = ( ! empty( $new_instance[ 'gid'] ) ) ? esc_sql( $new_instance[ 'gid' ] ) : '';
            $instance[ 'show_join_group' ] = ( ! empty( $new_instance[ 'show_join_group' ] ) ) ? strip_tags( $new_instance[ 'show_join_group' ] ) : '';
            $instance[ 'show_view_members' ] = ( ! empty( $new_instance[ 'show_view_members' ] ) ) ? strip_tags( $new_instance[ 'show_view_members' ] ) : '';
            
            return $instance;
        }
    }
endif;