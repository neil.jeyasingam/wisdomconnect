<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Profilegrid_featured_group
 * @subpackage Profilegrid_featured_group/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Profilegrid_featured_group
 * @subpackage Profilegrid_featured_group/admin
 * @author     Your Name <email@example.com>
 */
class Profilegrid_featured_group_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $Profilegrid_featured_group    The ID of this plugin.
	 */
	private $Profilegrid_featured_group;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $Profilegrid_featured_group       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $Profilegrid_featured_group, $version ) {

		$this->Profilegrid_featured_group = $Profilegrid_featured_group;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Profilegrid_featured_group_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Profilegrid_featured_group_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
            if (class_exists('Profile_Magic') ) {
                wp_enqueue_style( $this->Profilegrid_featured_group, plugin_dir_url( __FILE__ ) . 'css/profilegrid-featured-group-admin.css', array(), $this->version, 'all' );
                
            }
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
            
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Profilegrid_featured_group_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Profilegrid_featured_group_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
            if (class_exists('Profile_Magic') ) {
                $dbhandler = new PM_DBhandler;
                wp_enqueue_script( $this->Profilegrid_featured_group, plugin_dir_url( __FILE__ ) . 'js/profilegrid-featured-group-admin.js', array( 'jquery','wp-color-picker' ), $this->version, true );
              
            }
	}
        
        
        public function profile_magic_custom_notifications()
        {
            if (!class_exists('Profile_Magic') ) {
                    
                $this->profilegrid_custom_notification_installation();
                
            }
            
        }
        
        public function profilegrid_custom_notification_installation()
        {
            $plugin_slug= 'profilegrid-user-profiles-groups-and-communities';
            $installUrl = admin_url('update.php?action=install-plugin&plugin=' . $plugin_slug);
            $installUrl = wp_nonce_url($installUrl, 'install-plugin_' . $plugin_slug);
            ?>
            <div class="notice notice-success is-dismissible">
                <p><?php echo sprintf(__( "Profilegrid Featured Group work with ProfileGrid Plugin. You can install it  from <a href='%s'>Here</a>.", 'profilegrid-featured-group'),$installUrl ); ?></p>
            </div>
            <?php
            $plugin = trim(basename(plugin_dir_path(dirname( __FILE__))).'/profilegrid-featured-group.php');
            deactivate_plugins($plugin);

        }
        
        public function activate_sitewide_plugins($blog_id)
        {
            // Switch to new website
            $dbhandler = new PM_DBhandler;
            $activator = new Profile_Magic_Activator;
            switch_to_blog( $blog_id );
            // Activate
            foreach( array_keys( get_site_option( 'active_sitewide_plugins' ) ) as $plugin ) {
                do_action( 'activate_'  . $plugin, false );
                do_action( 'activate'   . '_plugin', $plugin, false );
                $activator->activate();
                
            }
            // Restore current website 
            restore_current_blog();
        }
        
       
        
     

}
