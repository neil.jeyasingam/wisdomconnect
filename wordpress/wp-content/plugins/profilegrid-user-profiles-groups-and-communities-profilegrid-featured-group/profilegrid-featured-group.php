<?php

/**
 * @link              http://profilegrid.co
 * @since             1.0.0
 * @package           Profilegrid_featured_group
 *
 * @wordpress-plugin
 * Plugin Name:       ProfileGrid Featured Group
 * Plugin URI:        http://profilegrid.co
 * Description:       A customizable frontend ProfileGrid Widget that allows you to display featured membership groups.
 * Version:           1.1
 * Author:            profilegrid
 * Author URI:        http://profilegrid.co
 * License:           Commercial/ Proprietary
 * Text Domain:       profilegrid-notifications
 * Domain Path:       /languages
 * WC requires at least: 3.0.0
 * WC tested up to: 3.5.4
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-profilegrid-notifications-activator.php
 */
function activate_Profilegrid_featured_group() {
	$pm_woocommerce_activator = new Profilegrid_featured_group_Activator;
	$pm_woocommerce_activator->activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-profilegrid-notifications-deactivator.php
 */
function deactivate_Profilegrid_featured_group() {
        $pm_woocommerce_deactivator = new Profilegrid_featured_group_Deactivator();
	$pm_woocommerce_deactivator->deactivate();
}

register_activation_hook( __FILE__, 'activate_Profilegrid_featured_group' );
register_deactivation_hook( __FILE__, 'deactivate_Profilegrid_featured_group' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-profilegrid-featured-group.php';
require_once plugin_dir_path( __FILE__ ) . 'plugin-updates/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker('http://profilegrid.co/profilegrid_featured_group_metadata.json', __FILE__, 'profilegrid-user-profiles-groups-and-communities-profilegrid-featured-group');
 
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_Profilegrid_featured_group() {

	$plugin = new Profilegrid_featured_group();
	$plugin->run();

}
run_Profilegrid_featured_group();
