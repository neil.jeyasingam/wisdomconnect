<?php
/**
 * Licence CTA
 *
 * @package JITSI_MEET_WP
 */
?>
<div class="jitsi-meet-licence-activation-area">
	<h3> Please Active your license <a href="<?php echo esc_url( admin_url('admin.php?page=jitsi-meet-pro-license') ); ?>">Activate</a> </h3>
</div>

