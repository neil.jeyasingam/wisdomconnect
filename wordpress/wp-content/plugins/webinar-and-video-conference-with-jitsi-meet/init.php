<?php //phpcs:ignore
/**
 * Exit if accessed directly
 *
 * @package JITSI_MEET_WP
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Make sure the same class is not loaded twice.
if ( ! class_exists( 'Jitsi_Meet_WP' ) ) {
	/**
	 * Main Jiti Meet WP Class
	 *
	 * The main class that initiates and runs the Jitsi Meet WP plugin.
	 *
	 * @since 1.0.0
	 */
	final class Jitsi_Meet_WP {

		/**
		 * Instance
		 *
		 * Holds a single instance of the `Jitsi_Meet_WP` class.
		 *
		 * @since 1.0.0
		 *
		 * @access private
		 * @static
		 *
		 * @var Jitsi_Meet_WP A single instance of the class.
		 */
		private static $instance = null;

		/**
		 * Instance
		 *
		 * Ensures only one instance of the class is loaded or can be loaded.
		 *
		 * @return Jitsi_Meet_WP An instance of the class.
		 * @since 1.0.0
		 *
		 * @access public
		 * @static
		 */
		public static function instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Clone
		 *
		 * Disable class cloning.
		 *
		 * @return void
		 * @since 1.0.0
		 *
		 * @access protected
		 */
		public function __clone() {
			// Cloning instances of the class is forbidden.
			_doing_it_wrong( __FUNCTION__, esc_html__( 'Cheatin&#8217; huh?', 'jitsi-meet-wp' ), '1.0.0' );
		}

		/**
		 * Wakeup
		 *
		 * Disable unserializing the class.
		 *
		 * @return void
		 * @since 1.7.0
		 *
		 * @access protected
		 */
		public function __wakeup() {
			// Unserializing instances of the class is forbidden.
			_doing_it_wrong( __FUNCTION__, esc_html__( 'Cheatin&#8217; huh?', 'jitsi-meet-wp' ), '1.0.0' );
		}

		/**
		 * Constructor
		 *
		 * Initialize the Jitsi Meet WP plugins.
		 *
		 * @since 1.0.0
		 *
		 * @access public
		 */
		public function __construct() {
			register_activation_hook( __FILE__, [ $this, 'jitsi_meet_wp_activate' ] );
			add_action( 'init', [ $this, 'i18n' ] );
			add_action( 'admin_notices', [ $this, 'print_notices' ], 15 );
			add_filter( 'clean_url', [ $this, 'add_async_forscript' ], 11, 1 );
			add_action( 'admin_enqueue_scripts', [ $this, 'jitsi_meet_wp_editor_scripts' ] );
			$this->jitsi_meet_wp_admin_files();
			add_filter( 'plugin_action_links_' . plugin_basename( JITSI_MEET_WP__FILE__ ), [ $this, 'jitsi_meet_add_action_links' ] );
			add_shortcode( 'jitsi-meet-wp', [ $this, 'jitsi_shortcode_render' ] );
			$this->jitsi_init_elementor();
		}

		/**
		 * Get template
		 *
		 * @param string $name  Template name.
		 *
		 * @return  boolean
		 */
		public function get_template( $name = null ) {

			$template = locate_template( 'webinar-and-video-conference-with-jitsi-meet/' . $name . '.php' );

			if ( ! $template ) {
				$template = JITSI_MEET_WP_TEMPLATES . "/$name.php";
			}

			if ( file_exists( $template ) ) {
				include $template;
			} else {
				return false;
			}
		}

		/**
		 * Add jitsi meet notice
		 *
		 * @param  mixed  $class [$class description].
		 * @param string $message  [$message description].
		 *
		 * @return  void
		 */
		public function add_notice( $class, $message ) {
			$notices = get_option( sanitize_key( 'jitsi_meet_wp_notices' ), [] );
			if ( is_string( $message ) && is_string( $class )
				&& ! wp_list_filter( $notices, [ 'message' => $message ] ) ) {
				$notices[] = [
					'message' => $message,
					'class'   => $class,
				];

				update_option( sanitize_key( 'jitsi_meet_wp_notices' ), $notices );
			}
		}

		/**
		 * Display jitsi meet notice
		 *
		 * @return  void
		 */
		public function print_notices() {
			$notices = get_option( sanitize_key( 'jitsi_meet_wp_notices' ), [] );
			foreach ( $notices as $notice ) {
				?>
				<div class="notice notice-large is-dismissible notice-<?php echo esc_attr( $notice['class'] ); ?>">
					<?php echo wp_kses_post( $notice['message'] ); ?>
				</div>
				<?php
				update_option( sanitize_key( 'jitsi_meet_wp_notices' ), [] );
			}
		}

		/**
		 * Load jitsi meet admin files
		 *
		 * @return  void
		 */
		public function jitsi_meet_wp_admin_files() {
			if ( is_admin() ) {
				require_once JITSI_MEET_WP_FILE_PATH . 'inc/admin/class-admin.php';
			}
		}

		/**
		 * Plugin action links
		 *
		 * Jitsi_meet_add_action_links method use for show jitsi meet GET PRO button
		 *
		 * @param array $actions The plugin links.
		 *
		 * @return  mixed
		 */
		public function jitsi_meet_add_action_links( $actions ) {
			$mylinks = [
				'<a href="' . esc_url( admin_url( 'admin.php?page=jitsi-meet' ) ) . '">' . esc_html( 'Settings' ) . '</a>',
				'<a class="special-action-link" target="_blank" href="' . esc_url( 'https://go.wppool.dev/Gv5' ) . '">' . esc_html( 'Upgrade to Pro' ) . '</a>',
			];
			$actions = array_merge( $actions, $mylinks );
			return $actions;
		}

		/**
		 * On activation hook
		 */
		private static function jitsi_meet_wp_activate() {
			update_option( 'jitsi_meet_wp_version', JITSI_MEET_WP_VERSION );

			$install_date = get_option( 'jitsi_meet_wp_install_time' );

			if ( empty( $install_date ) ) {
				update_option( 'jitsi_meet_wp_install_time', time() );
			}

			set_transient( 'jitsi_meet_wp_review_notice_interval', 'off', 7 * DAY_IN_SECONDS );
			set_transient( 'jitsi_meet_wp_affiliate_notice_interval', 'off', 10 * DAY_IN_SECONDS );

		}

		/**
		 * Load Textdomain
		 *
		 * Load plugin localization files.
		 *
		 * @since 1.0.0
		 *
		 * @access public
		 */
		public function i18n() {
			load_plugin_textdomain( 'jitsi-meet-wp', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
		}

		/**
		 * Editor Scripts
		 */
		public function jitsi_meet_wp_editor_scripts() {
			wp_enqueue_script( 'jitsi-admin-script', plugins_url( '/blocks/dist/jitsi.admin.js', __FILE__ ), [ 'jquery' ], filemtime( plugin_dir_path( __FILE__ ) . '/blocks/dist/jitsi.admin.js' ), true );
			wp_enqueue_script( 'jquery.syotimer', plugins_url( '/assets/js/jquery.syotimer.min.js', __FILE__ ), [ 'jquery' ], '3.1.1', true );

			wp_localize_script('jitsi-admin-script', 'jitsiMeet', [
				'url'   => esc_url( admin_url( 'admin-ajax.php' ) ),
				'nonce' => wp_create_nonce( 'jitsi_meet_admin_nonce' ),
			]);
		}

		/**
		 * Add async forscript
		 *
		 * @param string $url  forscript url.
		 *
		 * @return string
		 */
		public function add_async_forscript( $url ) {
			if ( strpos( $url, '#asyncload' ) === false ) {
				return $url;
			} else {
				return str_replace( '#asyncload', '', $url ) . "' async='async";
			}
		}


		/**
		 * Render meet shortcode
		 *
		 * @param array $args  [$args description].
		 *
		 * @return  array
		 */
		public function jitsi_shortcode_render( $args ) {

			$params = shortcode_atts([
				'name'                => '',
				'width'               => 1080,
				'height'              => 720,
				'startwithaudiomuted' => get_option( 'jitsi_opt_start_local_audio_muted', 0 ) ? '1' : '0',
				'startwithvideomuted' => get_option( 'jitsi_opt_startWithVideoMuted', 0 ) ? '1' : '0',
				'startscreensharing'  => get_option( 'jitsi_opt_startScreenSharing', 0 ) ? '1' : '0',
				'invite'              => get_option( 'jitsi_opt_invite', 1 ) ? '1' : '0',
			], $args);

			if ( is_user_logged_in() ) {
				$current_user          = wp_get_current_user();
				$user_info             = $current_user->display_name . ',' . $current_user->user_email;
				$param_arr['userinfo'] = $user_info;
			}

			$name                = $params['name'];
			$width               = $params['width'];
			$height              = $params['height'];
			$startwithaudiomuted = $params['startwithaudiomuted'];
			$startwithvideomuted = $params['startwithvideomuted'];
			$startscreensharing  = $params['startscreensharing'];
			$invite              = $params['invite'];

			if ( '' === $name ) {
				$x    = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$name = substr( str_shuffle( str_repeat( $x, ceil( 10 / strlen( $x ) ) ) ), 1, 10 );
			}
			//phpcs:ignore
			$output = sprintf( '<div class="jitsi-wrapper" data-name="%1$s" data-width="%2$s" data-height="%3$s" style="width:%2$spx" data-mute="%4$s" data-videomute="%5$s" data-screen="%6$s" data-invite="%7$s"></div>', $name, $width, $height, $startwithaudiomuted, $startwithvideomuted, $startscreensharing, $invite );
			return $output;
		}

		/**
		 * Elementor
		 */
		public function jitsi_init_elementor() {
			// Check if Elementor installed and activated.
			if ( ! did_action( 'elementor/loaded' ) ) {
				return;
			}
			add_action( 'elementor/widgets/widgets_registered', [ $this, 'jitsi_el_widgets_registered' ] );
		}

		/**
		 * Registered widgets
		 *
		 * @return  void
		 */
		public function jitsi_el_widgets_registered() {
			$this->jitsi_el_include_widgets();
			$this->jitsi_el_register_widgets();
		}

		/**
		 * Include_widgets
		 *
		 * @return  void
		 */
		private function jitsi_el_include_widgets() {
			require_once __DIR__ . '/inc/elementor.php';
		}

		/**
		 * Register_widgets
		 *
		 * @return  void
		 */
		private function jitsi_el_register_widgets() {
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Jitsi\Widgets\Jitsi_Elementor() );
		}

	}
}

if ( ! function_exists( 'jitsi_meet_wp' ) ) {
	/**
	 * Jitsi_Meet_WP class instance
	 *
	 * @return  object
	 */
	function jitsi_meet_wp() {
		return Jitsi_Meet_WP::instance();
	}
}

/** Fire off the plugin */
jitsi_meet_wp();
