/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./blocks/src/blocks.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./blocks/src/blocks.js":
/*!******************************!*\
  !*** ./blocks/src/blocks.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./blocks/src/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _editor_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./editor.scss */ "./blocks/src/editor.scss");
/* harmony import */ var _editor_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_editor_scss__WEBPACK_IMPORTED_MODULE_1__);
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var InspectorControls = wp.editor.InspectorControls;
var registerBlockType = wp.blocks.registerBlockType;
var __ = wp.i18n.__;
var _wp$element = wp.element,
    Component = _wp$element.Component,
    Fragment = _wp$element.Fragment;
var _wp$components = wp.components,
    PanelBody = _wp$components.PanelBody,
    TextControl = _wp$components.TextControl,
    RangeControl = _wp$components.RangeControl,
    CheckboxControl = _wp$components.CheckboxControl;

var blockIcon = function blockIcon() {
  return wp.element.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    height: "40",
    viewBox: "0 0 49 28",
    fill: "none"
  }, wp.element.createElement("path", {
    "fill-rule": "evenodd",
    "clip-rule": "evenodd",
    d: "M34.4757 22.0614V17.2941L43.0323 23.4061C43.5361 23.7659 44.1987 23.814 44.7491 23.5307C45.2996 23.2474 45.6455 22.6803 45.6455 22.0612V5.53492C45.6455 4.91587 45.2996 4.34873 44.7491 4.06545C44.1987 3.78219 43.5361 3.8303 43.0323 4.19012L34.4757 10.3021V5.53504C34.4757 2.61741 31.8784 0.577148 29.0998 0.577148H8.62239C5.84387 0.577148 3.24658 2.61741 3.24658 5.53504V22.0614C3.24658 24.979 5.84387 27.0193 8.62239 27.0193H29.0998C31.8784 27.0193 34.4757 24.979 34.4757 22.0614ZM20.3316 18.1759C17.8232 16.8906 15.7668 14.8431 14.4904 12.3347L16.4404 10.3847C16.6886 10.1365 16.7596 9.79081 16.6621 9.48059C16.3341 8.48784 16.1568 7.42421 16.1568 6.31627C16.1568 5.82876 15.758 5.4299 15.2704 5.4299H12.1681C11.6807 5.4299 11.2818 5.82876 11.2818 6.31627C11.2818 14.6393 18.027 21.3845 26.35 21.3845C26.8375 21.3845 27.2364 20.9856 27.2364 20.4981V17.4047C27.2364 16.9172 26.8375 16.5183 26.35 16.5183C25.2509 16.5183 24.1784 16.341 23.1857 16.0131C22.8755 15.9068 22.5209 15.9865 22.2816 16.2258L20.3316 18.1759ZM25.8625 5.42103L26.4918 6.04149L20.8989 11.6345H24.5773V12.5209H19.2591V7.20264H20.1455V11.0051L25.8625 5.42103Z",
    fill: "#407BFF"
  }));
};




var EditBlock = /*#__PURE__*/function (_Component) {
  _inherits(EditBlock, _Component);

  var _super = _createSuper(EditBlock);

  function EditBlock(props) {
    _classCallCheck(this, EditBlock);

    return _super.call(this, props);
  }

  _createClass(EditBlock, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          setAttributes = _this$props.setAttributes,
          _this$props$attribute = _this$props.attributes,
          name = _this$props$attribute.name,
          fromGlobal = _this$props$attribute.fromGlobal;

      var _newName = Math.random().toString(36).substring(2, 15);

      if (!name) {
        setAttributes({
          name: _newName
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          attributes = _this$props2.attributes,
          setAttributes = _this$props2.setAttributes;
      var name = attributes.name,
          fromGlobal = attributes.fromGlobal,
          width = attributes.width,
          height = attributes.height,
          audioMuted = attributes.audioMuted,
          videoMuted = attributes.videoMuted,
          screenSharing = attributes.screenSharing,
          invite = attributes.invite;
      return wp.element.createElement(Fragment, null, wp.element.createElement(InspectorControls, null, wp.element.createElement(PanelBody, {
        title: __('Settings'),
        initialOpen: true
      }, wp.element.createElement(TextControl, {
        label: __('Name'),
        value: name,
        onChange: function onChange(val) {
          return setAttributes({
            name: val
          });
        }
      }), wp.element.createElement(CheckboxControl, {
        label: __('Config from global'),
        checked: fromGlobal,
        onChange: function onChange(val) {
          setAttributes({
            fromGlobal: val
          });

          if (!fromGlobal) {
            setAttributes({
              width: parseInt(jitsi.meeting_width),
              height: parseInt(jitsi.meeting_height),
              audioMuted: parseInt(jitsi.startwithaudiomuted) ? true : false,
              videoMuted: parseInt(jitsi.startwithvideomuted) ? true : false,
              screenSharing: parseInt(jitsi.startscreensharing) ? true : false,
              invite: parseInt(jitsi.invite) ? true : false
            });
          }
        }
      }), !fromGlobal && wp.element.createElement("div", null, wp.element.createElement(RangeControl, {
        label: __('Width'),
        value: width,
        onChange: function onChange(val) {
          return setAttributes({
            width: val
          });
        },
        min: 100,
        max: 2000,
        step: 10
      }), wp.element.createElement(RangeControl, {
        label: __('Height'),
        value: height,
        onChange: function onChange(val) {
          return setAttributes({
            height: val
          });
        },
        min: 100,
        max: 2000,
        step: 10
      }), wp.element.createElement(CheckboxControl, {
        label: __('Start with audio muted'),
        checked: audioMuted,
        onChange: function onChange(val) {
          return setAttributes({
            audioMuted: val
          });
        }
      }), wp.element.createElement(CheckboxControl, {
        label: __('Start with video muted'),
        checked: videoMuted,
        onChange: function onChange(val) {
          return setAttributes({
            videoMuted: val
          });
        }
      }), wp.element.createElement(CheckboxControl, {
        label: __('Start with screen sharing'),
        checked: screenSharing,
        onChange: function onChange(val) {
          return setAttributes({
            screenSharing: val
          });
        }
      }), wp.element.createElement(CheckboxControl, {
        label: __('Enable Inviting'),
        checked: invite,
        onChange: function onChange(val) {
          return setAttributes({
            invite: val
          });
        }
      })))), wp.element.createElement("div", {
        className: "jitsi-wrapper",
        "data-name": name,
        "data-width": width,
        "data-height": height,
        "data-mute": audioMuted,
        "data-videomute": videoMuted,
        "data-screen": screenSharing,
        "data-invite": invite
      }, "https://meet.jit.si/".concat(name)));
    }
  }]);

  return EditBlock;
}(Component);

registerBlockType('jitsi-meet-wp/jitsi-meet', {
  title: __('Jitsi Meet', 'jitsi-meet-wp'),
  icon: blockIcon,
  category: 'embed',
  keywords: [__('jitsi', 'jitsi-meet-wp'), __('meeting', 'jitsi-meet-wp'), __('video', 'jitsi-meet-wp'), __('conference', 'jitsi-meet-wp'), __('zoom', 'jitsi-meet-wp')],
  attributes: {
    name: {
      type: 'string',
      "default": ''
    },
    width: {
      type: 'number',
      "default": 1080
    },
    height: {
      type: 'number',
      "default": 720
    },
    fromGlobal: {
      type: 'boolean',
      "default": false
    },
    audioMuted: {
      type: 'boolean',
      "default": false
    },
    videoMuted: {
      type: 'boolean',
      "default": true
    },
    screenSharing: {
      type: 'boolean',
      "default": false
    },
    invite: {
      type: 'boolean',
      "default": true
    }
  },
  edit: EditBlock,
  save: function save(props) {
    var _props$attributes = props.attributes,
        name = _props$attributes.name,
        width = _props$attributes.width,
        height = _props$attributes.height,
        audioMuted = _props$attributes.audioMuted,
        videoMuted = _props$attributes.videoMuted,
        screenSharing = _props$attributes.screenSharing,
        invite = _props$attributes.invite;
    return wp.element.createElement("div", {
      className: "jitsi-wrapper",
      "data-name": name,
      "data-width": width,
      "data-height": height,
      "data-mute": audioMuted,
      "data-videomute": videoMuted,
      "data-screen": screenSharing,
      "data-invite": invite,
      style: {
        width: "".concat(width, "px")
      }
    });
  }
});

/***/ }),

/***/ "./blocks/src/editor.scss":
/*!********************************!*\
  !*** ./blocks/src/editor.scss ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./blocks/src/style.scss":
/*!*******************************!*\
  !*** ./blocks/src/style.scss ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vYmxvY2tzL3NyYy9ibG9ja3MuanMiLCJ3ZWJwYWNrOi8vLy4vYmxvY2tzL3NyYy9lZGl0b3Iuc2NzcyIsIndlYnBhY2s6Ly8vLi9ibG9ja3Mvc3JjL3N0eWxlLnNjc3MiXSwibmFtZXMiOlsiSW5zcGVjdG9yQ29udHJvbHMiLCJ3cCIsImVkaXRvciIsInJlZ2lzdGVyQmxvY2tUeXBlIiwiYmxvY2tzIiwiX18iLCJpMThuIiwiZWxlbWVudCIsIkNvbXBvbmVudCIsIkZyYWdtZW50IiwiY29tcG9uZW50cyIsIlBhbmVsQm9keSIsIlRleHRDb250cm9sIiwiUmFuZ2VDb250cm9sIiwiQ2hlY2tib3hDb250cm9sIiwiYmxvY2tJY29uIiwiRWRpdEJsb2NrIiwicHJvcHMiLCJzZXRBdHRyaWJ1dGVzIiwiYXR0cmlidXRlcyIsIm5hbWUiLCJmcm9tR2xvYmFsIiwiX25ld05hbWUiLCJNYXRoIiwicmFuZG9tIiwidG9TdHJpbmciLCJzdWJzdHJpbmciLCJ3aWR0aCIsImhlaWdodCIsImF1ZGlvTXV0ZWQiLCJ2aWRlb011dGVkIiwic2NyZWVuU2hhcmluZyIsImludml0ZSIsInZhbCIsInBhcnNlSW50Iiwiaml0c2kiLCJtZWV0aW5nX3dpZHRoIiwibWVldGluZ19oZWlnaHQiLCJzdGFydHdpdGhhdWRpb211dGVkIiwic3RhcnR3aXRodmlkZW9tdXRlZCIsInN0YXJ0c2NyZWVuc2hhcmluZyIsInRpdGxlIiwiaWNvbiIsImNhdGVnb3J5Iiwia2V5d29yZHMiLCJ0eXBlIiwiZWRpdCIsInNhdmUiXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsRkEsSUFBUUEsaUJBQVIsR0FBOEJDLEVBQUUsQ0FBQ0MsTUFBakMsQ0FBUUYsaUJBQVI7QUFDQSxJQUFRRyxpQkFBUixHQUE4QkYsRUFBRSxDQUFDRyxNQUFqQyxDQUFRRCxpQkFBUjtBQUNBLElBQVFFLEVBQVIsR0FBZUosRUFBRSxDQUFDSyxJQUFsQixDQUFRRCxFQUFSO0FBQ0Esa0JBQWdDSixFQUFFLENBQUNNLE9BQW5DO0FBQUEsSUFBUUMsU0FBUixlQUFRQSxTQUFSO0FBQUEsSUFBbUJDLFFBQW5CLGVBQW1CQSxRQUFuQjtBQUNBLHFCQUFrRVIsRUFBRSxDQUFDUyxVQUFyRTtBQUFBLElBQVFDLFNBQVIsa0JBQVFBLFNBQVI7QUFBQSxJQUFtQkMsV0FBbkIsa0JBQW1CQSxXQUFuQjtBQUFBLElBQWdDQyxZQUFoQyxrQkFBZ0NBLFlBQWhDO0FBQUEsSUFBOENDLGVBQTlDLGtCQUE4Q0EsZUFBOUM7O0FBRUEsSUFBTUMsU0FBUyxHQUFHLFNBQVpBLFNBQVksR0FBTTtFQUN0QixPQUNFO0lBQ0UsS0FBSyxFQUFDLDRCQURSO0lBRUUsTUFBTSxFQUFDLElBRlQ7SUFHRSxPQUFPLEVBQUMsV0FIVjtJQUlFLElBQUksRUFBQztFQUpQLEdBTUU7SUFDRSxhQUFVLFNBRFo7SUFFRSxhQUFVLFNBRlo7SUFHRSxDQUFDLEVBQUMsMG1DQUhKO0lBSUUsSUFBSSxFQUFDO0VBSlAsRUFORixDQURGO0FBZUQsQ0FoQkQ7O0FBa0JBO0FBQ0E7O0lBRU1DLFM7Ozs7O0VBQ0osbUJBQVlDLEtBQVosRUFBbUI7SUFBQTs7SUFBQSx5QkFDWEEsS0FEVztFQUVsQjs7OztXQUVELDZCQUFvQjtNQUNsQixrQkFHSSxLQUFLQSxLQUhUO01BQUEsSUFDRUMsYUFERixlQUNFQSxhQURGO01BQUEsd0NBRUVDLFVBRkY7TUFBQSxJQUVnQkMsSUFGaEIseUJBRWdCQSxJQUZoQjtNQUFBLElBRXNCQyxVQUZ0Qix5QkFFc0JBLFVBRnRCOztNQUlBLElBQU1DLFFBQVEsR0FBR0MsSUFBSSxDQUFDQyxNQUFMLEdBQWNDLFFBQWQsQ0FBdUIsRUFBdkIsRUFBMkJDLFNBQTNCLENBQXFDLENBQXJDLEVBQXdDLEVBQXhDLENBQWpCOztNQUNBLElBQUksQ0FBQ04sSUFBTCxFQUFXO1FBQ1RGLGFBQWEsQ0FBQztVQUFFRSxJQUFJLEVBQUVFO1FBQVIsQ0FBRCxDQUFiO01BQ0Q7SUFDRjs7O1dBRUQsa0JBQVM7TUFDUCxtQkFBc0MsS0FBS0wsS0FBM0M7TUFBQSxJQUFRRSxVQUFSLGdCQUFRQSxVQUFSO01BQUEsSUFBb0JELGFBQXBCLGdCQUFvQkEsYUFBcEI7TUFFQSxJQUNFRSxJQURGLEdBU0lELFVBVEosQ0FDRUMsSUFERjtNQUFBLElBRUVDLFVBRkYsR0FTSUYsVUFUSixDQUVFRSxVQUZGO01BQUEsSUFHRU0sS0FIRixHQVNJUixVQVRKLENBR0VRLEtBSEY7TUFBQSxJQUlFQyxNQUpGLEdBU0lULFVBVEosQ0FJRVMsTUFKRjtNQUFBLElBS0VDLFVBTEYsR0FTSVYsVUFUSixDQUtFVSxVQUxGO01BQUEsSUFNRUMsVUFORixHQVNJWCxVQVRKLENBTUVXLFVBTkY7TUFBQSxJQU9FQyxhQVBGLEdBU0laLFVBVEosQ0FPRVksYUFQRjtNQUFBLElBUUVDLE1BUkYsR0FTSWIsVUFUSixDQVFFYSxNQVJGO01BV0EsT0FDRSx5QkFBQyxRQUFELFFBQ0UseUJBQUMsaUJBQUQsUUFDRSx5QkFBQyxTQUFEO1FBQVcsS0FBSyxFQUFFM0IsRUFBRSxDQUFDLFVBQUQsQ0FBcEI7UUFBa0MsV0FBVyxFQUFFO01BQS9DLEdBQ0UseUJBQUMsV0FBRDtRQUNFLEtBQUssRUFBRUEsRUFBRSxDQUFDLE1BQUQsQ0FEWDtRQUVFLEtBQUssRUFBRWUsSUFGVDtRQUdFLFFBQVEsRUFBRSxrQkFBQ2EsR0FBRDtVQUFBLE9BQVNmLGFBQWEsQ0FBQztZQUFFRSxJQUFJLEVBQUVhO1VBQVIsQ0FBRCxDQUF0QjtRQUFBO01BSFosRUFERixFQU1FLHlCQUFDLGVBQUQ7UUFDRSxLQUFLLEVBQUU1QixFQUFFLENBQUMsb0JBQUQsQ0FEWDtRQUVFLE9BQU8sRUFBRWdCLFVBRlg7UUFHRSxRQUFRLEVBQUUsa0JBQUNZLEdBQUQsRUFBUztVQUNqQmYsYUFBYSxDQUFDO1lBQUVHLFVBQVUsRUFBRVk7VUFBZCxDQUFELENBQWI7O1VBQ0EsSUFBSSxDQUFDWixVQUFMLEVBQWlCO1lBQ2ZILGFBQWEsQ0FBQztjQUNaUyxLQUFLLEVBQUVPLFFBQVEsQ0FBQ0MsS0FBSyxDQUFDQyxhQUFQLENBREg7Y0FFWlIsTUFBTSxFQUFFTSxRQUFRLENBQUNDLEtBQUssQ0FBQ0UsY0FBUCxDQUZKO2NBR1pSLFVBQVUsRUFBRUssUUFBUSxDQUFDQyxLQUFLLENBQUNHLG1CQUFQLENBQVIsR0FDUixJQURRLEdBRVIsS0FMUTtjQU1aUixVQUFVLEVBQUVJLFFBQVEsQ0FBQ0MsS0FBSyxDQUFDSSxtQkFBUCxDQUFSLEdBQ1IsSUFEUSxHQUVSLEtBUlE7Y0FTWlIsYUFBYSxFQUFFRyxRQUFRLENBQUNDLEtBQUssQ0FBQ0ssa0JBQVAsQ0FBUixHQUNYLElBRFcsR0FFWCxLQVhRO2NBWVpSLE1BQU0sRUFBRUUsUUFBUSxDQUFDQyxLQUFLLENBQUNILE1BQVAsQ0FBUixHQUF5QixJQUF6QixHQUFnQztZQVo1QixDQUFELENBQWI7VUFjRDtRQUNGO01BckJILEVBTkYsRUE2QkcsQ0FBQ1gsVUFBRCxJQUNDLHNDQUNFLHlCQUFDLFlBQUQ7UUFDRSxLQUFLLEVBQUVoQixFQUFFLENBQUMsT0FBRCxDQURYO1FBRUUsS0FBSyxFQUFFc0IsS0FGVDtRQUdFLFFBQVEsRUFBRSxrQkFBQ00sR0FBRDtVQUFBLE9BQVNmLGFBQWEsQ0FBQztZQUFFUyxLQUFLLEVBQUVNO1VBQVQsQ0FBRCxDQUF0QjtRQUFBLENBSFo7UUFJRSxHQUFHLEVBQUUsR0FKUDtRQUtFLEdBQUcsRUFBRSxJQUxQO1FBTUUsSUFBSSxFQUFFO01BTlIsRUFERixFQVNFLHlCQUFDLFlBQUQ7UUFDRSxLQUFLLEVBQUU1QixFQUFFLENBQUMsUUFBRCxDQURYO1FBRUUsS0FBSyxFQUFFdUIsTUFGVDtRQUdFLFFBQVEsRUFBRSxrQkFBQ0ssR0FBRDtVQUFBLE9BQVNmLGFBQWEsQ0FBQztZQUFFVSxNQUFNLEVBQUVLO1VBQVYsQ0FBRCxDQUF0QjtRQUFBLENBSFo7UUFJRSxHQUFHLEVBQUUsR0FKUDtRQUtFLEdBQUcsRUFBRSxJQUxQO1FBTUUsSUFBSSxFQUFFO01BTlIsRUFURixFQWlCRSx5QkFBQyxlQUFEO1FBQ0UsS0FBSyxFQUFFNUIsRUFBRSxDQUFDLHdCQUFELENBRFg7UUFFRSxPQUFPLEVBQUV3QixVQUZYO1FBR0UsUUFBUSxFQUFFLGtCQUFDSSxHQUFEO1VBQUEsT0FBU2YsYUFBYSxDQUFDO1lBQUVXLFVBQVUsRUFBRUk7VUFBZCxDQUFELENBQXRCO1FBQUE7TUFIWixFQWpCRixFQXNCRSx5QkFBQyxlQUFEO1FBQ0UsS0FBSyxFQUFFNUIsRUFBRSxDQUFDLHdCQUFELENBRFg7UUFFRSxPQUFPLEVBQUV5QixVQUZYO1FBR0UsUUFBUSxFQUFFLGtCQUFDRyxHQUFEO1VBQUEsT0FBU2YsYUFBYSxDQUFDO1lBQUVZLFVBQVUsRUFBRUc7VUFBZCxDQUFELENBQXRCO1FBQUE7TUFIWixFQXRCRixFQTJCRSx5QkFBQyxlQUFEO1FBQ0UsS0FBSyxFQUFFNUIsRUFBRSxDQUFDLDJCQUFELENBRFg7UUFFRSxPQUFPLEVBQUUwQixhQUZYO1FBR0UsUUFBUSxFQUFFLGtCQUFDRSxHQUFEO1VBQUEsT0FBU2YsYUFBYSxDQUFDO1lBQUVhLGFBQWEsRUFBRUU7VUFBakIsQ0FBRCxDQUF0QjtRQUFBO01BSFosRUEzQkYsRUFnQ0UseUJBQUMsZUFBRDtRQUNFLEtBQUssRUFBRTVCLEVBQUUsQ0FBQyxpQkFBRCxDQURYO1FBRUUsT0FBTyxFQUFFMkIsTUFGWDtRQUdFLFFBQVEsRUFBRSxrQkFBQ0MsR0FBRDtVQUFBLE9BQVNmLGFBQWEsQ0FBQztZQUFFYyxNQUFNLEVBQUVDO1VBQVYsQ0FBRCxDQUF0QjtRQUFBO01BSFosRUFoQ0YsQ0E5QkosQ0FERixDQURGLEVBeUVFO1FBQ0UsU0FBUyxFQUFDLGVBRFo7UUFFRSxhQUFXYixJQUZiO1FBR0UsY0FBWU8sS0FIZDtRQUlFLGVBQWFDLE1BSmY7UUFLRSxhQUFXQyxVQUxiO1FBTUUsa0JBQWdCQyxVQU5sQjtRQU9FLGVBQWFDLGFBUGY7UUFRRSxlQUFhQztNQVJmLGlDQVN5QlosSUFUekIsRUF6RUYsQ0FERjtJQXNGRDs7OztFQXBIcUJaLFM7O0FBdUh4QkwsaUJBQWlCLENBQUMsMEJBQUQsRUFBNkI7RUFDNUNzQyxLQUFLLEVBQUVwQyxFQUFFLENBQUMsWUFBRCxFQUFlLGVBQWYsQ0FEbUM7RUFFNUNxQyxJQUFJLEVBQUUzQixTQUZzQztFQUc1QzRCLFFBQVEsRUFBRSxPQUhrQztFQUk1Q0MsUUFBUSxFQUFFLENBQ1J2QyxFQUFFLENBQUMsT0FBRCxFQUFVLGVBQVYsQ0FETSxFQUVSQSxFQUFFLENBQUMsU0FBRCxFQUFZLGVBQVosQ0FGTSxFQUdSQSxFQUFFLENBQUMsT0FBRCxFQUFVLGVBQVYsQ0FITSxFQUlSQSxFQUFFLENBQUMsWUFBRCxFQUFlLGVBQWYsQ0FKTSxFQUtSQSxFQUFFLENBQUMsTUFBRCxFQUFTLGVBQVQsQ0FMTSxDQUprQztFQVc1Q2MsVUFBVSxFQUFFO0lBQ1ZDLElBQUksRUFBRTtNQUNKeUIsSUFBSSxFQUFFLFFBREY7TUFFSixXQUFTO0lBRkwsQ0FESTtJQUtWbEIsS0FBSyxFQUFFO01BQ0xrQixJQUFJLEVBQUUsUUFERDtNQUVMLFdBQVM7SUFGSixDQUxHO0lBU1ZqQixNQUFNLEVBQUU7TUFDTmlCLElBQUksRUFBRSxRQURBO01BRU4sV0FBUztJQUZILENBVEU7SUFhVnhCLFVBQVUsRUFBRTtNQUNWd0IsSUFBSSxFQUFFLFNBREk7TUFFVixXQUFTO0lBRkMsQ0FiRjtJQWlCVmhCLFVBQVUsRUFBRTtNQUNWZ0IsSUFBSSxFQUFFLFNBREk7TUFFVixXQUFTO0lBRkMsQ0FqQkY7SUFxQlZmLFVBQVUsRUFBRTtNQUNWZSxJQUFJLEVBQUUsU0FESTtNQUVWLFdBQVM7SUFGQyxDQXJCRjtJQXlCVmQsYUFBYSxFQUFFO01BQ2JjLElBQUksRUFBRSxTQURPO01BRWIsV0FBUztJQUZJLENBekJMO0lBNkJWYixNQUFNLEVBQUU7TUFDTmEsSUFBSSxFQUFFLFNBREE7TUFFTixXQUFTO0lBRkg7RUE3QkUsQ0FYZ0M7RUE2QzVDQyxJQUFJLEVBQUU5QixTQTdDc0M7RUE4QzVDK0IsSUFBSSxFQUFFLGNBQVU5QixLQUFWLEVBQWlCO0lBQ3JCLHdCQVFJQSxLQUFLLENBQUNFLFVBUlY7SUFBQSxJQUNFQyxJQURGLHFCQUNFQSxJQURGO0lBQUEsSUFFRU8sS0FGRixxQkFFRUEsS0FGRjtJQUFBLElBR0VDLE1BSEYscUJBR0VBLE1BSEY7SUFBQSxJQUlFQyxVQUpGLHFCQUlFQSxVQUpGO0lBQUEsSUFLRUMsVUFMRixxQkFLRUEsVUFMRjtJQUFBLElBTUVDLGFBTkYscUJBTUVBLGFBTkY7SUFBQSxJQU9FQyxNQVBGLHFCQU9FQSxNQVBGO0lBVUEsT0FDRTtNQUNFLFNBQVMsRUFBQyxlQURaO01BRUUsYUFBV1osSUFGYjtNQUdFLGNBQVlPLEtBSGQ7TUFJRSxlQUFhQyxNQUpmO01BS0UsYUFBV0MsVUFMYjtNQU1FLGtCQUFnQkMsVUFObEI7TUFPRSxlQUFhQyxhQVBmO01BUUUsZUFBYUMsTUFSZjtNQVNFLEtBQUssRUFBRTtRQUNMTCxLQUFLLFlBQUtBLEtBQUw7TUFEQTtJQVRULEVBREY7RUFlRDtBQXhFMkMsQ0FBN0IsQ0FBakIsQzs7Ozs7Ozs7Ozs7QUNsSkEseUM7Ozs7Ozs7Ozs7O0FDQUEseUMiLCJmaWxlIjoiYmxvY2tzLmJ1aWxkLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9ibG9ja3Mvc3JjL2Jsb2Nrcy5qc1wiKTtcbiIsImNvbnN0IHsgSW5zcGVjdG9yQ29udHJvbHMgfSA9IHdwLmVkaXRvcjtcclxuY29uc3QgeyByZWdpc3RlckJsb2NrVHlwZSB9ID0gd3AuYmxvY2tzO1xyXG5jb25zdCB7IF9fIH0gPSB3cC5pMThuO1xyXG5jb25zdCB7IENvbXBvbmVudCwgRnJhZ21lbnQgfSA9IHdwLmVsZW1lbnQ7XHJcbmNvbnN0IHsgUGFuZWxCb2R5LCBUZXh0Q29udHJvbCwgUmFuZ2VDb250cm9sLCBDaGVja2JveENvbnRyb2wgfSA9IHdwLmNvbXBvbmVudHM7XHJcblxyXG5jb25zdCBibG9ja0ljb24gPSAoKSA9PiB7XHJcbiAgcmV0dXJuIChcclxuICAgIDxzdmdcclxuICAgICAgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiXHJcbiAgICAgIGhlaWdodD1cIjQwXCJcclxuICAgICAgdmlld0JveD1cIjAgMCA0OSAyOFwiXHJcbiAgICAgIGZpbGw9XCJub25lXCJcclxuICAgID5cclxuICAgICAgPHBhdGhcclxuICAgICAgICBmaWxsLXJ1bGU9XCJldmVub2RkXCJcclxuICAgICAgICBjbGlwLXJ1bGU9XCJldmVub2RkXCJcclxuICAgICAgICBkPVwiTTM0LjQ3NTcgMjIuMDYxNFYxNy4yOTQxTDQzLjAzMjMgMjMuNDA2MUM0My41MzYxIDIzLjc2NTkgNDQuMTk4NyAyMy44MTQgNDQuNzQ5MSAyMy41MzA3QzQ1LjI5OTYgMjMuMjQ3NCA0NS42NDU1IDIyLjY4MDMgNDUuNjQ1NSAyMi4wNjEyVjUuNTM0OTJDNDUuNjQ1NSA0LjkxNTg3IDQ1LjI5OTYgNC4zNDg3MyA0NC43NDkxIDQuMDY1NDVDNDQuMTk4NyAzLjc4MjE5IDQzLjUzNjEgMy44MzAzIDQzLjAzMjMgNC4xOTAxMkwzNC40NzU3IDEwLjMwMjFWNS41MzUwNEMzNC40NzU3IDIuNjE3NDEgMzEuODc4NCAwLjU3NzE0OCAyOS4wOTk4IDAuNTc3MTQ4SDguNjIyMzlDNS44NDM4NyAwLjU3NzE0OCAzLjI0NjU4IDIuNjE3NDEgMy4yNDY1OCA1LjUzNTA0VjIyLjA2MTRDMy4yNDY1OCAyNC45NzkgNS44NDM4NyAyNy4wMTkzIDguNjIyMzkgMjcuMDE5M0gyOS4wOTk4QzMxLjg3ODQgMjcuMDE5MyAzNC40NzU3IDI0Ljk3OSAzNC40NzU3IDIyLjA2MTRaTTIwLjMzMTYgMTguMTc1OUMxNy44MjMyIDE2Ljg5MDYgMTUuNzY2OCAxNC44NDMxIDE0LjQ5MDQgMTIuMzM0N0wxNi40NDA0IDEwLjM4NDdDMTYuNjg4NiAxMC4xMzY1IDE2Ljc1OTYgOS43OTA4MSAxNi42NjIxIDkuNDgwNTlDMTYuMzM0MSA4LjQ4Nzg0IDE2LjE1NjggNy40MjQyMSAxNi4xNTY4IDYuMzE2MjdDMTYuMTU2OCA1LjgyODc2IDE1Ljc1OCA1LjQyOTkgMTUuMjcwNCA1LjQyOTlIMTIuMTY4MUMxMS42ODA3IDUuNDI5OSAxMS4yODE4IDUuODI4NzYgMTEuMjgxOCA2LjMxNjI3QzExLjI4MTggMTQuNjM5MyAxOC4wMjcgMjEuMzg0NSAyNi4zNSAyMS4zODQ1QzI2LjgzNzUgMjEuMzg0NSAyNy4yMzY0IDIwLjk4NTYgMjcuMjM2NCAyMC40OTgxVjE3LjQwNDdDMjcuMjM2NCAxNi45MTcyIDI2LjgzNzUgMTYuNTE4MyAyNi4zNSAxNi41MTgzQzI1LjI1MDkgMTYuNTE4MyAyNC4xNzg0IDE2LjM0MSAyMy4xODU3IDE2LjAxMzFDMjIuODc1NSAxNS45MDY4IDIyLjUyMDkgMTUuOTg2NSAyMi4yODE2IDE2LjIyNThMMjAuMzMxNiAxOC4xNzU5Wk0yNS44NjI1IDUuNDIxMDNMMjYuNDkxOCA2LjA0MTQ5TDIwLjg5ODkgMTEuNjM0NUgyNC41NzczVjEyLjUyMDlIMTkuMjU5MVY3LjIwMjY0SDIwLjE0NTVWMTEuMDA1MUwyNS44NjI1IDUuNDIxMDNaXCJcclxuICAgICAgICBmaWxsPVwiIzQwN0JGRlwiXHJcbiAgICAgIC8+XHJcbiAgICA8L3N2Zz5cclxuICApO1xyXG59O1xyXG5cclxuaW1wb3J0ICcuL3N0eWxlLnNjc3MnO1xyXG5pbXBvcnQgJy4vZWRpdG9yLnNjc3MnO1xyXG5cclxuY2xhc3MgRWRpdEJsb2NrIGV4dGVuZHMgQ29tcG9uZW50IHtcclxuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgc3VwZXIocHJvcHMpO1xyXG4gIH1cclxuXHJcbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICBjb25zdCB7XHJcbiAgICAgIHNldEF0dHJpYnV0ZXMsXHJcbiAgICAgIGF0dHJpYnV0ZXM6IHsgbmFtZSwgZnJvbUdsb2JhbCB9LFxyXG4gICAgfSA9IHRoaXMucHJvcHM7XHJcbiAgICBjb25zdCBfbmV3TmFtZSA9IE1hdGgucmFuZG9tKCkudG9TdHJpbmcoMzYpLnN1YnN0cmluZygyLCAxNSk7XHJcbiAgICBpZiAoIW5hbWUpIHtcclxuICAgICAgc2V0QXR0cmlidXRlcyh7IG5hbWU6IF9uZXdOYW1lIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVuZGVyKCkge1xyXG4gICAgY29uc3QgeyBhdHRyaWJ1dGVzLCBzZXRBdHRyaWJ1dGVzIH0gPSB0aGlzLnByb3BzO1xyXG5cclxuICAgIGNvbnN0IHtcclxuICAgICAgbmFtZSxcclxuICAgICAgZnJvbUdsb2JhbCxcclxuICAgICAgd2lkdGgsXHJcbiAgICAgIGhlaWdodCxcclxuICAgICAgYXVkaW9NdXRlZCxcclxuICAgICAgdmlkZW9NdXRlZCxcclxuICAgICAgc2NyZWVuU2hhcmluZyxcclxuICAgICAgaW52aXRlLFxyXG4gICAgfSA9IGF0dHJpYnV0ZXM7XHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgPEZyYWdtZW50PlxyXG4gICAgICAgIDxJbnNwZWN0b3JDb250cm9scz5cclxuICAgICAgICAgIDxQYW5lbEJvZHkgdGl0bGU9e19fKCdTZXR0aW5ncycpfSBpbml0aWFsT3Blbj17dHJ1ZX0+XHJcbiAgICAgICAgICAgIDxUZXh0Q29udHJvbFxyXG4gICAgICAgICAgICAgIGxhYmVsPXtfXygnTmFtZScpfVxyXG4gICAgICAgICAgICAgIHZhbHVlPXtuYW1lfVxyXG4gICAgICAgICAgICAgIG9uQ2hhbmdlPXsodmFsKSA9PiBzZXRBdHRyaWJ1dGVzKHsgbmFtZTogdmFsIH0pfVxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICA8Q2hlY2tib3hDb250cm9sXHJcbiAgICAgICAgICAgICAgbGFiZWw9e19fKCdDb25maWcgZnJvbSBnbG9iYWwnKX1cclxuICAgICAgICAgICAgICBjaGVja2VkPXtmcm9tR2xvYmFsfVxyXG4gICAgICAgICAgICAgIG9uQ2hhbmdlPXsodmFsKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBzZXRBdHRyaWJ1dGVzKHsgZnJvbUdsb2JhbDogdmFsIH0pO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFmcm9tR2xvYmFsKSB7XHJcbiAgICAgICAgICAgICAgICAgIHNldEF0dHJpYnV0ZXMoe1xyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiBwYXJzZUludChqaXRzaS5tZWV0aW5nX3dpZHRoKSxcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IHBhcnNlSW50KGppdHNpLm1lZXRpbmdfaGVpZ2h0KSxcclxuICAgICAgICAgICAgICAgICAgICBhdWRpb011dGVkOiBwYXJzZUludChqaXRzaS5zdGFydHdpdGhhdWRpb211dGVkKVxyXG4gICAgICAgICAgICAgICAgICAgICAgPyB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgICA6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIHZpZGVvTXV0ZWQ6IHBhcnNlSW50KGppdHNpLnN0YXJ0d2l0aHZpZGVvbXV0ZWQpXHJcbiAgICAgICAgICAgICAgICAgICAgICA/IHRydWVcclxuICAgICAgICAgICAgICAgICAgICAgIDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgc2NyZWVuU2hhcmluZzogcGFyc2VJbnQoaml0c2kuc3RhcnRzY3JlZW5zaGFyaW5nKVxyXG4gICAgICAgICAgICAgICAgICAgICAgPyB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgICA6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIGludml0ZTogcGFyc2VJbnQoaml0c2kuaW52aXRlKSA/IHRydWUgOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgeyFmcm9tR2xvYmFsICYmIChcclxuICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPFJhbmdlQ29udHJvbFxyXG4gICAgICAgICAgICAgICAgICBsYWJlbD17X18oJ1dpZHRoJyl9XHJcbiAgICAgICAgICAgICAgICAgIHZhbHVlPXt3aWR0aH1cclxuICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh2YWwpID0+IHNldEF0dHJpYnV0ZXMoeyB3aWR0aDogdmFsIH0pfVxyXG4gICAgICAgICAgICAgICAgICBtaW49ezEwMH1cclxuICAgICAgICAgICAgICAgICAgbWF4PXsyMDAwfVxyXG4gICAgICAgICAgICAgICAgICBzdGVwPXsxMH1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8UmFuZ2VDb250cm9sXHJcbiAgICAgICAgICAgICAgICAgIGxhYmVsPXtfXygnSGVpZ2h0Jyl9XHJcbiAgICAgICAgICAgICAgICAgIHZhbHVlPXtoZWlnaHR9XHJcbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsodmFsKSA9PiBzZXRBdHRyaWJ1dGVzKHsgaGVpZ2h0OiB2YWwgfSl9XHJcbiAgICAgICAgICAgICAgICAgIG1pbj17MTAwfVxyXG4gICAgICAgICAgICAgICAgICBtYXg9ezIwMDB9XHJcbiAgICAgICAgICAgICAgICAgIHN0ZXA9ezEwfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDxDaGVja2JveENvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdTdGFydCB3aXRoIGF1ZGlvIG11dGVkJyl9XHJcbiAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e2F1ZGlvTXV0ZWR9XHJcbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsodmFsKSA9PiBzZXRBdHRyaWJ1dGVzKHsgYXVkaW9NdXRlZDogdmFsIH0pfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDxDaGVja2JveENvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdTdGFydCB3aXRoIHZpZGVvIG11dGVkJyl9XHJcbiAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e3ZpZGVvTXV0ZWR9XHJcbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsodmFsKSA9PiBzZXRBdHRyaWJ1dGVzKHsgdmlkZW9NdXRlZDogdmFsIH0pfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDxDaGVja2JveENvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdTdGFydCB3aXRoIHNjcmVlbiBzaGFyaW5nJyl9XHJcbiAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e3NjcmVlblNoYXJpbmd9XHJcbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsodmFsKSA9PiBzZXRBdHRyaWJ1dGVzKHsgc2NyZWVuU2hhcmluZzogdmFsIH0pfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDxDaGVja2JveENvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdFbmFibGUgSW52aXRpbmcnKX1cclxuICAgICAgICAgICAgICAgICAgY2hlY2tlZD17aW52aXRlfVxyXG4gICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHZhbCkgPT4gc2V0QXR0cmlidXRlcyh7IGludml0ZTogdmFsIH0pfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKX1cclxuICAgICAgICAgIDwvUGFuZWxCb2R5PlxyXG4gICAgICAgIDwvSW5zcGVjdG9yQ29udHJvbHM+XHJcbiAgICAgICAgPGRpdlxyXG4gICAgICAgICAgY2xhc3NOYW1lPVwiaml0c2ktd3JhcHBlclwiXHJcbiAgICAgICAgICBkYXRhLW5hbWU9e25hbWV9XHJcbiAgICAgICAgICBkYXRhLXdpZHRoPXt3aWR0aH1cclxuICAgICAgICAgIGRhdGEtaGVpZ2h0PXtoZWlnaHR9XHJcbiAgICAgICAgICBkYXRhLW11dGU9e2F1ZGlvTXV0ZWR9XHJcbiAgICAgICAgICBkYXRhLXZpZGVvbXV0ZT17dmlkZW9NdXRlZH1cclxuICAgICAgICAgIGRhdGEtc2NyZWVuPXtzY3JlZW5TaGFyaW5nfVxyXG4gICAgICAgICAgZGF0YS1pbnZpdGU9e2ludml0ZX1cclxuICAgICAgICA+e2BodHRwczovL21lZXQuaml0LnNpLyR7bmFtZX1gfTwvZGl2PlxyXG4gICAgICA8L0ZyYWdtZW50PlxyXG4gICAgKTtcclxuICB9XHJcbn1cclxuXHJcbnJlZ2lzdGVyQmxvY2tUeXBlKCdqaXRzaS1tZWV0LXdwL2ppdHNpLW1lZXQnLCB7XHJcbiAgdGl0bGU6IF9fKCdKaXRzaSBNZWV0JywgJ2ppdHNpLW1lZXQtd3AnKSxcclxuICBpY29uOiBibG9ja0ljb24sXHJcbiAgY2F0ZWdvcnk6ICdlbWJlZCcsXHJcbiAga2V5d29yZHM6IFtcclxuICAgIF9fKCdqaXRzaScsICdqaXRzaS1tZWV0LXdwJyksXHJcbiAgICBfXygnbWVldGluZycsICdqaXRzaS1tZWV0LXdwJyksXHJcbiAgICBfXygndmlkZW8nLCAnaml0c2ktbWVldC13cCcpLFxyXG4gICAgX18oJ2NvbmZlcmVuY2UnLCAnaml0c2ktbWVldC13cCcpLFxyXG4gICAgX18oJ3pvb20nLCAnaml0c2ktbWVldC13cCcpLFxyXG4gIF0sXHJcbiAgYXR0cmlidXRlczoge1xyXG4gICAgbmFtZToge1xyXG4gICAgICB0eXBlOiAnc3RyaW5nJyxcclxuICAgICAgZGVmYXVsdDogJycsXHJcbiAgICB9LFxyXG4gICAgd2lkdGg6IHtcclxuICAgICAgdHlwZTogJ251bWJlcicsXHJcbiAgICAgIGRlZmF1bHQ6IDEwODAsXHJcbiAgICB9LFxyXG4gICAgaGVpZ2h0OiB7XHJcbiAgICAgIHR5cGU6ICdudW1iZXInLFxyXG4gICAgICBkZWZhdWx0OiA3MjAsXHJcbiAgICB9LFxyXG4gICAgZnJvbUdsb2JhbDoge1xyXG4gICAgICB0eXBlOiAnYm9vbGVhbicsXHJcbiAgICAgIGRlZmF1bHQ6IGZhbHNlLFxyXG4gICAgfSxcclxuICAgIGF1ZGlvTXV0ZWQ6IHtcclxuICAgICAgdHlwZTogJ2Jvb2xlYW4nLFxyXG4gICAgICBkZWZhdWx0OiBmYWxzZSxcclxuICAgIH0sXHJcbiAgICB2aWRlb011dGVkOiB7XHJcbiAgICAgIHR5cGU6ICdib29sZWFuJyxcclxuICAgICAgZGVmYXVsdDogdHJ1ZSxcclxuICAgIH0sXHJcbiAgICBzY3JlZW5TaGFyaW5nOiB7XHJcbiAgICAgIHR5cGU6ICdib29sZWFuJyxcclxuICAgICAgZGVmYXVsdDogZmFsc2UsXHJcbiAgICB9LFxyXG4gICAgaW52aXRlOiB7XHJcbiAgICAgIHR5cGU6ICdib29sZWFuJyxcclxuICAgICAgZGVmYXVsdDogdHJ1ZSxcclxuICAgIH0sXHJcbiAgfSxcclxuICBlZGl0OiBFZGl0QmxvY2ssXHJcbiAgc2F2ZTogZnVuY3Rpb24gKHByb3BzKSB7XHJcbiAgICBjb25zdCB7XHJcbiAgICAgIG5hbWUsXHJcbiAgICAgIHdpZHRoLFxyXG4gICAgICBoZWlnaHQsXHJcbiAgICAgIGF1ZGlvTXV0ZWQsXHJcbiAgICAgIHZpZGVvTXV0ZWQsXHJcbiAgICAgIHNjcmVlblNoYXJpbmcsXHJcbiAgICAgIGludml0ZSxcclxuICAgIH0gPSBwcm9wcy5hdHRyaWJ1dGVzO1xyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgIDxkaXZcclxuICAgICAgICBjbGFzc05hbWU9XCJqaXRzaS13cmFwcGVyXCJcclxuICAgICAgICBkYXRhLW5hbWU9e25hbWV9XHJcbiAgICAgICAgZGF0YS13aWR0aD17d2lkdGh9XHJcbiAgICAgICAgZGF0YS1oZWlnaHQ9e2hlaWdodH1cclxuICAgICAgICBkYXRhLW11dGU9e2F1ZGlvTXV0ZWR9XHJcbiAgICAgICAgZGF0YS12aWRlb211dGU9e3ZpZGVvTXV0ZWR9XHJcbiAgICAgICAgZGF0YS1zY3JlZW49e3NjcmVlblNoYXJpbmd9XHJcbiAgICAgICAgZGF0YS1pbnZpdGU9e2ludml0ZX1cclxuICAgICAgICBzdHlsZT17e1xyXG4gICAgICAgICAgd2lkdGg6IGAke3dpZHRofXB4YCxcclxuICAgICAgICB9fVxyXG4gICAgICA+PC9kaXY+XHJcbiAgICApO1xyXG4gIH0sXHJcbn0pO1xyXG4iLCIvLyByZW1vdmVkIGJ5IGV4dHJhY3QtdGV4dC13ZWJwYWNrLXBsdWdpbiIsIi8vIHJlbW92ZWQgYnkgZXh0cmFjdC10ZXh0LXdlYnBhY2stcGx1Z2luIl0sInNvdXJjZVJvb3QiOiIifQ==