<?php
/*
Plugin Name: ProfileGrid Extension
Description: Extension to add camera capture functionality to ProfileGrid.
Version: 1.0
Author: Xiaodan Du
*/

// Hook into the avatar generation function to add our custom button
add_filter('pm_get_user_avatar_html', 'extend_avatar_functionality');

function extend_avatar_functionality($avatar_html) {
    // Add the "Take Photo" button beside the "Change Image" button
    $take_photo_button = '<button id="take_photo_btn">Take Photo</button>'; // Add necessary attributes and styles

    return str_replace('<div class="pg_edit_avatar">', '<div class="pg_edit_avatar">' . $take_photo_button, $avatar_html);
}

// Enqueue necessary scripts and styles for the "Take Photo" functionality
add_action('wp_enqueue_scripts', 'enqueue_extended_scripts_styles');
function enqueue_extended_scripts_styles() {
    // Add your scripts and styles here, e.g., JavaScript to handle the camera functionality
}