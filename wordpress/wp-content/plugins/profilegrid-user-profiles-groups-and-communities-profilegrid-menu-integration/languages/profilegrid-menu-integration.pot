#, fuzzy
msgid ""
msgstr ""
"Language: \n"
"POT-Creation-Date: 2019-06-03 03:50+0000\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: Loco https://localise.biz/"

#: admin/class-profilegrid-menu-integration-admin.php:109
msgid "Userid Slug Changer Settings"
msgstr ""

#: admin/class-profilegrid-menu-integration-admin.php:138
#, php-format
msgid ""
"ProfileGrid Userid Slug Changer work with ProfileGrid Plugin. You can "
"install it  from <a href='%s'>Here</a>."
msgstr ""

#: admin/partials/profilegrid-menu-integration-admin-display.php:29
msgid "Custom Profile Slugs Settings"
msgstr ""

#: admin/partials/profilegrid-menu-integration-admin-display.php:40
msgid "Customize Profile Slug"
msgstr ""

#: admin/partials/profilegrid-menu-integration-admin-display.php:46
msgid "Turn on to customize the slug in next option."
msgstr ""

#: admin/partials/profilegrid-menu-integration-admin-display.php:52
msgid "Profile URL"
msgstr ""

#: admin/partials/profilegrid-menu-integration-admin-display.php:56
msgid "User ID"
msgstr ""

#: admin/partials/profilegrid-menu-integration-admin-display.php:57
msgid "User Name"
msgstr ""

#: admin/partials/profilegrid-menu-integration-admin-display.php:58
msgid "Random Number"
msgstr ""

#: admin/partials/profilegrid-menu-integration-admin-display.php:62
msgid "Select slug from dropdown."
msgstr ""

#: admin/partials/profilegrid-menu-integration-admin-display.php:70
msgid "Cancel"
msgstr ""

#: admin/partials/profilegrid-menu-integration-admin-display.php:74
msgid "Save"
msgstr ""

#: admin/partials/profilegrid-menu-integration-setting-option.php:7
msgid "Custom Profile Slugs"
msgstr ""

#: admin/partials/profilegrid-menu-integration-setting-option.php:9
msgid "Edit and define slugs in profile permalinks"
msgstr ""

#. Name of the plugin
msgid "ProfileGrid Custom Profile Slugs"
msgstr ""

#. Description of the plugin
msgid ""
"Define how your user profile URL's will appear to site visitors and search "
"engines. Take control of your user profile permalinks and add dynamic slugs."
msgstr ""

#. URI of the plugin
#. Author URI of the plugin
msgid "http://profilegrid.co"
msgstr ""

#. Author of the plugin
msgid "profilegrid"
msgstr ""
