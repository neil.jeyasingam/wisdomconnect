<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Profilegrid_Menu_Integration
 * @subpackage Profilegrid_Menu_Integration/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Profilegrid_Menu_Integration
 * @subpackage Profilegrid_Menu_Integration/public
 * @author     Your Name <email@example.com>
 */
class Profilegrid_Menu_Integration_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $profilegrid_menu_integration    The ID of this plugin.
	 */
	private $profilegrid_menu_integration;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $profilegrid_menu_integration       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $profilegrid_menu_integration, $version ) {

		$this->profilegrid_menu_integration = $profilegrid_menu_integration;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Profilegrid_Menu_Integration_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Profilegrid_Menu_Integration_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->profilegrid_menu_integration, plugin_dir_url( __FILE__ ) . 'css/profilegrid-menu-integration-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Profilegrid_Menu_Integration_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Profilegrid_Menu_Integration_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
                wp_enqueue_script( $this->profilegrid_menu_integration, plugin_dir_url( __FILE__ ) . 'js/profilegrid-menu-integration-public.js', array( 'jquery' ), $this->version, false );
             
	}

 
        
        public function pm_menu_integration_links( $items, $args ) 
        {
             if (class_exists('Profile_Magic') ) {
            
            $dbhandler = new PM_DBhandler;
            $pmrequests = new PM_request;
           
            $pm_menu_function = new PM_Helper_MENUINTEGRATION;
            $custom_menus = $pm_menu_function->pm_menus_integration();
             $menu_html = "";
	    $enable_menu_integration = $dbhandler->get_global_option_value('pm_enable_menu_integration','0');
            $pg_theme_location = $dbhandler->get_global_option_value('pm_menu_theme_location','');
            if($enable_menu_integration == 1 && !empty($pg_theme_location) && is_array($pg_theme_location))
            {
                foreach($pg_theme_location as $location)
                {
                    if ($args->theme_location == $location) 
                    {
                        
                        if (!empty($custom_menus)):                  
                            foreach($custom_menus as $key=>$menu):

                                $menu_url = $pm_menu_function->generate_menu_links($menu['id'],$menu); 
                                $menu_html .= $menu_url;   
                            endforeach;
                        endif;
                                       
                    }
                    
                }
                
            }
            $items .= $menu_html;   
            
            
            
        }
        return $items;
             }
}