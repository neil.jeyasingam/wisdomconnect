<?php

/**
 * @link              http://profilegrid.co
 * @since             1.0.0
 * @package           ProfileGrid_Group_photos
 *
 * @wordpress-plugin
 * Plugin Name:       ProfileGrid Login Logout Menu
 * Plugin URI:        http://profilegrid.co
 * Description:       Now you can add contextual login menu item to your website menu(s) with few simple clicks. The menu item changes based on user login state. Additionally, you have option to add User Profile, User Groups and Password Recovery items too.
 * Version:           1.4
 * Author:            profilegrid
 * Author URI:        http://profilegrid.co
 * License:           Commercial/ Proprietary 
 * Text Domain:       profilegrid-menu-integration
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-profilegrid-menu-integration-activator.php
 */
function activate_profilegrid_menu_integration() {
	$pm_display_name_activator = new Profilegrid_Menu_Integration_Activator;
	$pm_display_name_activator->activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-profilegrid-menu-integration-deactivator.php
 */
function deactivate_profilegrid_menu_integration() {
        $pm_display_name_deactivator = new Profilegrid_Menu_Integration_Deactivator();
	$pm_display_name_deactivator->deactivate();
}

register_activation_hook( __FILE__, 'activate_profilegrid_menu_integration' );
register_deactivation_hook( __FILE__, 'deactivate_profilegrid_menu_integration' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-profilegrid-menu-integration.php';
require_once plugin_dir_path( __FILE__ ) . 'plugin-updates/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker('http://profilegrid.co/login_logout_menu_metadata.json', __FILE__, 'profilegrid-user-profiles-groups-and-communities-profilegrid-menu-integration');
  
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_profilegrid_menu_integration() {

	$plugin = new Profilegrid_Menu_Integration();
	$plugin->run();

}
run_profilegrid_menu_integration();
