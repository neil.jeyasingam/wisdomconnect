<?php
class PM_Helper_MENUINTEGRATION{

public function pm_menus_integration()
    {
       $dbhandler = new PM_DBhandler;
       $custom_menus = array();
     $login_menu_status = $dbhandler->get_global_option_value('pm_enable_login_menu_integration','1');
     $forgotpassword_menu_status = $dbhandler->get_global_option_value('pm_enable_forgot_password_menu_integration','1');
     $myprofile_menu_status = $dbhandler->get_global_option_value('pm_enable_myprofile_menu_integration','1');
     $allgroups_menu_status = $dbhandler->get_global_option_value('pm_enable_all_groups_menu_integration','1');
	
        $custom_menus['pg-login-menu'] = array('id'=>'pg-login-menu','title'=>'Login','status'=>$login_menu_status,'class'=>'pg-login-menu');
        $custom_menus['pg-forgotpassword-menu'] = array('id'=>'pg-forgotpassword-menu','title'=>'Reset Password','status'=>$forgotpassword_menu_status,'class'=>'pg-forgotpassword-menu');
        $custom_menus['pg-myprofile-menu'] = array('id'=>'pg-myprofile-menu','title'=>'User Profile','status'=>$myprofile_menu_status,'class'=>'pg-myprofile-menu');
        $custom_menus['pg-allgroups-menu'] = array('id'=>'pg-allgroups-menu','title'=>'User Groups','status'=>$allgroups_menu_status,'class'=>'pg-usergroups-menu');
        $pm_menus_order_status = $dbhandler->get_global_option_value('pm_menus_order_status',$custom_menus);
        return apply_filters('pm_menus_integration',$pm_menus_order_status);
 // return $custom_menus;
    }
    
    public function generate_menu_links($id,$menu)
    {
     $dbhandler = new PM_DBhandler;
      $pmrequests = new PM_request;
	$menu_html='';
     if(isset($menu) && $menu['status']=='1')
       {
           switch($id)
           {
                case 'pg-login-menu':
                  if (is_user_logged_in())
                  {
                     $login_page_url = wp_logout_url();
                     $logout_label =  $dbhandler->get_global_option_value('pm_logout_menu_label','Logout');
                     $menu_html =  '<li><a href="'. $login_page_url  .'">' . $logout_label .'</a></li>';
                  }
                  else
                  {
                      $login_page_url = $pmrequests->profile_magic_get_frontend_url('pm_user_login_page',site_url('/wp-login.php'));
                   $menu_html =  '<li><a href="'. $login_page_url  .'">' . $menu['title'] .'</a></li>';
                      
                  }
                break;
              
                case 'pg-forgotpassword-menu':
                  if (!is_user_logged_in())
                  {
                   $login_page_url=  $pmrequests->profile_magic_get_frontend_url('pm_forget_password_page',site_url('/wp-login.php?action=lostpassword'));              
                   $menu_html =  '<li><a href="'. $login_page_url  .'">' . $menu['title'] .'</a></li>';
                  }
                break;
                  
                case 'pg-myprofile-menu':
                  $login_page_url = $pmrequests->profile_magic_get_frontend_url('pm_user_profile_page','');
                  $menu_html =  '<li><a href="'. $login_page_url  .'">' . $menu['title'] .'</a></li>';
                break;
               
               case 'pg-allgroups-menu':
                 $login_page_url = $pmrequests->profile_magic_get_frontend_url('pm_groups_page','');
                 $menu_html =  '<li><a href="'. $login_page_url  .'">' . $menu['title'] .'</a></li>';
               break;
               
            }
            return $menu_html;
   }
    }
    
    
        
}

?>