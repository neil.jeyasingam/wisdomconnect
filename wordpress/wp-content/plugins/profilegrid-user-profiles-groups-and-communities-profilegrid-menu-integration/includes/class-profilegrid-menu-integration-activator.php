<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Profilegrid_Menu_Integration
 * @subpackage Profilegrid_Menu_Integration/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Profilegrid_Menu_Integration
 * @subpackage Profilegrid_Menu_Integration/includes
 * @author     Your Name <email@example.com>
 */
class Profilegrid_Menu_Integration_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
            public function activate()
         {
            if (!class_exists('Profile_Magic') ) 
            {
              //echo plugin_basename( 'profilegrid-woocommerce/profilegrid-woocommerce.php',__FILE__ );
               
               deactivate_plugins('menu-integration/menu-integration.php'); 
               $error_message = sprintf(__('This plugin requires ProfileGrid plugin to be active!  <a href="#" onclick="window.history.back()">Back</a>  ', 'bbPress'));
               wp_die($error_message);
            }
         }
	
}