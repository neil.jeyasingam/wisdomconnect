<?php

$dbhandler = new PM_DBhandler;
$pmrequests = new PM_request();
$path =  plugin_dir_url(__FILE__);
$identifier = 'SETTINGS';
$theme_locations = get_registered_nav_menus();
//print_r($theme_locations);die;
$pg_theme_location = $dbhandler->get_global_option_value('pm_menu_theme_location','');
$pm_menu_function = new PM_Helper_MENUINTEGRATION;
$custom_menus_integration = $pm_menu_function->pm_menus_integration();
//print_r($custom_menus);
//die;

if(filter_input(INPUT_POST,'submit_settings'))
{   
        
        $custom_menus = array();
        if(isset($_POST['pm_menus_order_status']))
        {
            foreach($_POST['pm_menus_order_status'] as $key=> $pm_menu)
            {
                if(!isset($pm_menu['status']))
                {
                    $pm_menu['status'] = '0';
                }
                $custom_menus[$key] = $pm_menu;
            }
        }
        
        $retrieved_nonce = filter_input(INPUT_POST,'_wpnonce');
	if (!wp_verify_nonce($retrieved_nonce, 'save_menu_integration_settings' ) ) die( __('Failed security check','profilegrid-user-profiles-groups-and-communities') );
	$exclude = array("_wpnonce","_wp_http_referer","submit_settings");
        if(!isset($_POST['pm_enable_menu_integration'])) $_POST['pm_enable_menu_integration'] = 0;
        $_POST['pm_menus_order_status'] = $custom_menus;
	$post = $pmrequests->sanitize_request($_POST,$identifier,$exclude);
       	if($post!=false)
	{
                print_r($post);
                foreach($post as $key=>$value)
		{
			$dbhandler->update_global_option_value($key,$value);
		}
	}
	wp_redirect( esc_url_raw('admin.php?page=pm_settings') );exit;
      
       
}
?>

<div class="uimagic">
  <form name="pm_menu_integration_settings" id="pm_menu_integration_settings" method="post">
    <!-----Dialogue Box Starts----->
    <div class="content">
      <div class="uimheader">
        <?php _e( 'ProfileGrid Menu Integration Settings','profilegrid-menu-integration' ); ?>
      </div>
     
      <div class="uimsubheader">
        <?php
		//Show subheadings or message or notice
		?>
      </div>
    
        <div class="uimrow">
        <div class="uimfield">
          <?php _e( 'Custom Menu Items','profilegrid-menu-integration' ); ?>
        </div>
        <div class="uiminput">
           <input name="pm_enable_menu_integration" id="pm_enable_menu_integration" type="checkbox" <?php checked($dbhandler->get_global_option_value('pm_enable_menu_integration','0'),'1'); ?> class="pm_toggle" value="1" style="display:none;" onClick="pm_show_hide(this,'pm_menu_integration_html')" />
          <label for="pm_enable_menu_integration"></label>
        </div>
        <div class="uimnote"><?php _e("Turn on to display ProfileGrid links inside one or more of your website menus.",'profilegrid-menu-integration');?></div>
      </div>
        
      <div class="childfieldsrow" id="pm_menu_integration_html" style="<?php if($dbhandler->get_global_option_value('pm_enable_menu_integration','0')=='1'){echo 'display:block;';} else { echo 'display:none;';} ?>">
       
          
       <div class="uimrow" id="redirection">
        <div class="uimfield">
          <?php _e( 'Select Menu(s)','profilegrid-menu-integration' ); ?>
        </div>
        <div class="uiminput pm_checkbox_required">
         <ul class="uimradio">
             <?php foreach($theme_locations as $key=>$location):?>
              <li>
                <input type="checkbox" name="pm_menu_theme_location[]" id="pm_menu_theme_location_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php 
                if(is_array($pg_theme_location) && in_array($key, $pg_theme_location)){ echo "checked";}
                        ?>>
                <?php echo $location; ?>
              </li>
              <?php endforeach;?>
          </ul>
        </div>
           <div class="uimnote"><?php _e('Select one or more menus where you wish to display ProfileGrid menu items. Remember, menus are created and managed Appearance —> Menus','profilegrid-menu-integration');?></div>
      </div>
          

        <div>
        <ul id="pm_sortable_profile_tabs">
        <?php
        if (!empty($custom_menus_integration)):
            foreach($custom_menus_integration as $key=>$menu):
               ?>
        
                       <li id="<?php echo $key; ?>">
                    <div class="pm-custom-field-page-slab pg_profile_tab">
                        <div class="pm-slab-drag-handle">&nbsp;</div>
                        <div class="pm-slab-info"><?php echo $menu['title']; ?></div>
                        <div class="pm-slab-buttons"><span class="dashicons dashicons-arrow-down"></span></div>
                    </div>
                    <div class="pg_profile_tab-setting" style="display:none;">
        
                        <div class="uimrow">
                        <div class="uimfield">
                            <?php _e( 'Show Menu','profilegrid-user-profiles-groups-and-communities' ); ?>      
                         </div>
                          <div class="uiminput">
                          <input name="pm_menus_order_status[<?php echo $key;?>][status]" id="<?php echo $menu['id'].'-status';?>" type="checkbox" <?php checked($menu['status'],'1'); ?> class="pm_toggle" value="1" style="display:none;"  onClick="pm_show_hide(this,'<?php echo $menu['id']."-child" ?>')"/>
                           <label for="<?php echo $menu['id'].'-status';?>"></label>
                           </div>
                           <div class="uimnote"><?php _e('Menu Show/Hide','profilegrid-user-profiles-groups-and-communities');?></div>
                         </div>

                        <div id="<?php echo $menu['id']."-child" ?>"  style="<?php if($menu['status']=='1'){echo 'display:block;';} else { echo 'display:none;';} ?>">
                        <div class="uimrow" >
                        <div class="uimfield">
                              <label for="<?php echo $menu['id'].'-title';?>"><?php _e( 'Menu Item Name','profilegrid-user-profiles-groups-and-communities' ); ?></label>       
                         </div>
                          <div class="uiminput">
                               <input type="text" name="pm_menus_order_status[<?php echo $key;?>][title]" id="<?php echo $menu['id'].'-title';?>" autocomplete="off" value="<?php echo $menu['title'];?>">
                           </div>
                           
                         </div>
 <?php if ($menu['id']=="pg-login-menu" )
           {
           ?>     
           <div class="uimrow" >
                <div class="uimfield">
                  <?php _e( 'Logout Item Name', 'profilegrid-menu-integration' ); ?>
                </div>
                <div class="uiminput">
                 <input name="pm_logout_menu_label" id="pm_logout_menu_label" type="text" value="<?php echo $dbhandler->get_global_option_value('pm_logout_menu_label','Logout'); ?>" />
                </div>
                <div class="uimnote"><?php _e('','profilegrid-menu-integration');?></div>
            </div>    
                    <?php 
                    }
                    ?>
                        </div>   
                       <input type="hidden" name="pm_menus_order_status[<?php echo $key;?>][id]" id="<?php echo $menu['id'].'-id';?>" value="<?php echo $menu['id'];?>" />
                        <input type="hidden" name="pm_menus_order_status[<?php echo $key;?>][class]" id="<?php echo $menu['id'].'-class';?>" value="<?php echo $menu['class'];?>" />


                    </div>

                </li>
                <?php
            endforeach;
        endif;
        ?>
        
        </ul>
        </div>

        
 </div>
      <div class="buttonarea"> 
          <a href="admin.php?page=pm_settings">
        <div class="cancel">&#8592; &nbsp;
          <?php _e('Cancel','profilegrid-menu-integration');?>
        </div>
        </a>
        <?php wp_nonce_field('save_menu_integration_settings'); ?>
        <input type="submit" value="<?php _e('Save','profilegrid-menu-integration');?>" name="submit_settings" id="submit_settings" />
        <div class="all_error_text" style="display:none;"></div>
      </div>
    
  
  </form>
</div>