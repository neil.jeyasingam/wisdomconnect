<?php
$path =  plugin_dir_url(__FILE__);
?>
<div class="uimrow"> <a href="admin.php?page=pm_menu_integration_settings">
  <div class="pm_setting_image"> <img src="<?php echo $path;?>images/pg-logout-icon.png" class="options" alt="options"> </div>
  <div class="pm-setting-heading"> <span class="pm-setting-icon-title">
    <?php _e( 'Login Logout Menu','profilegrid-menu-integration' ); ?>
    </span> <span class="pm-setting-description">
    <?php _e( 'Configure menu items.', 'profilegrid-menu-integration' ); ?>
    </span> </div>
  </a> </div>