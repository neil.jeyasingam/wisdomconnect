<?php
/*
Plugin Name: Senior Card Manager
Description: A custom plugin to manage senior cards.
Version: 2.0
Author: Xiaodan Du
*/

// Add Senior Card main menu
function add_senior_card_menu() {
    add_menu_page(
        'Senior Card Verification Management', // Page title
        'Senior Card Verification',            // Menu title
        'manage_options',                      // Capability
        'senior_card_verification',            // Menu slug
        'senior_card_admin_page',              // Callback function
        'dashicons-id',                        // Icon
        6                                      // Position             
    );
}
add_action('admin_menu', 'add_senior_card_menu');

function senior_card_admin_page() {
    // Check if the form has been submitted
    if (isset($_POST['submit_verification'])) {
        $users = get_users();
        foreach ($users as $user) {
            $userID = $user->ID;
            if (isset($_POST['verified'][$userID])) {
                update_user_meta($userID, 'verified', '1');
            } else {
                // If not, then the verification for this user should be removed
                delete_user_meta($userID, 'verified');
            }
        }
    }

    $users = get_users();

    echo '<h1>Senior Card Verification Manager</h1>';
    echo '<form method="post">';
    echo '<table class="wp-list-table widefat fixed striped">';
    echo '<thead><tr><th>User</th><th>Senior Card</th><th>Verified</th></tr></thead><tbody>';

    foreach ($users as $user) {
        $senior_card_url = get_user_meta($user->ID, 'senior_card_url', true);
        $verified = get_user_meta($user->ID, 'verified', true);
        if ($senior_card_url) {
            echo '<tr>';
            echo '<td>' . esc_html($user->display_name) . '</td>';
            echo '<td><a href="' . esc_url($senior_card_url) . '" target="_blank">View</a></td>';
            echo '<td><input type="checkbox" name="verified[' . $user->ID . ']" ' . ($verified ? 'checked' : '') . '></td>';
            echo '</tr>';
        }
    }

    echo '</tbody></table>';
    echo '<br>';
    echo '<input type="submit" name="submit_verification" value="Save Verification">';
    echo '</form>';
}
