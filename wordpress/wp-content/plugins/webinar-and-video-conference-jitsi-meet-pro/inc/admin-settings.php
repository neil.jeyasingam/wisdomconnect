<?php

if (!defined('ABSPATH')) {
    exit;
}

class Jitsi_Pro_Admin_Settings
{
    public $settings;
    public $callbacks;
    public $prefix;

    public function __construct()
    {
        include_once JITSI_PRO_FILE_PATH . 'inc/jitsi-pro-admin.php';
        include_once JITSI_PRO_FILE_PATH . 'inc/mannage-callback.php';

        $this->settings  = new Jitsi_Settings();
        $this->callbacks = new Mannage_Callback();
        $this->prefix = 'jitsi_opt_';

        // Fields
        $this->setSettings();
        $this->setSections();
        $this->setFields();
    }

    public static function key()
    {
        return 'jitsi-pro';
    }

    public function setSettings()
    {
        $args = array();

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-api',
            'option_name'  	=> $this->prefix . 'select_api'
        );

        $args[] = array(
            'option_group' => 'jitsi-pro-api',
            'option_name'  => $this->prefix . 'free_domain',
        );

        $args[] = array(
            'option_group' => 'jitsi-pro-api',
            'option_name'  => $this->prefix . 'custom_domain',
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-api',
            'option_name'  	=> $this->prefix . 'api_key'
        );

        $args[] = array(
            'option_group' => 'jitsi-pro-api',
            'option_name'  => $this->prefix . 'app_id',
        );

        $args[] = array(
            'option_group' => 'jitsi-pro-api',
            'option_name'  => $this->prefix . 'private_key',
        );

        $args[] = array(
            'option_group' => 'jitsi-pro-admin',
            'option_name'  => $this->prefix . 'user_email',
        );

        $args[] = array(
            'option_group' => 'jitsi-pro-admin',
            'option_name'  => $this->prefix . 'user_name',
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-admin',
            'option_name' 	=> $this->prefix . 'user_avatar'
        );

        $args[] = array(
            'option_group' => 'jitsi-pro-admin',
            'option_name'  => $this->prefix . 'user_is_moderator',
        );

        $args[] = array(
            'option_group' => 'jitsi-pro-admin',
            'option_name'  => $this->prefix . 'other_admin_config',
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-config',
            'option_name' 	=> $this->prefix . 'enable_livestream'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-config',
            'option_name' 	=> $this->prefix . 'enableWelcomePage'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-config',
            'option_name' 	=> $this->prefix . 'invite'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-config',
            'option_name' 	=> $this->prefix . 'enable_recording'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-config',
            'option_name' 	=> $this->prefix . 'enable_outbound'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-config',
            'option_name' 	=> $this->prefix . 'enable_transcription'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-audio',
            'option_name' 	=> $this->prefix . 'start_audio_only'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-audio',
            'option_name' 	=> $this->prefix . 'start_audio_muted'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-audio',
            'option_name' 	=> $this->prefix . 'start_local_audio_muted'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-audio',
            'option_name' 	=> $this->prefix . 'start_silent'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-video',
            'option_name' 	=> $this->prefix . 'video_resolution'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-video',
            'option_name' 	=> $this->prefix . 'maxfullresolutionparticipant'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-video',
            'option_name' 	=> $this->prefix . 'startWithVideoMuted'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-config',
            'option_name' 	=> $this->prefix . 'disableSimulcast'
        );

        $args[] = array(
            'option_group'     => 'jitsi-pro-config',
            'option_name'     => $this->prefix . 'hide_jitsi_sidebar'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-video',
            'option_name' 	=> $this->prefix . 'startVideoMuted'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-video',
            'option_name' 	=> $this->prefix . 'startScreenSharing'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-config',
            'option_name' 	=> $this->prefix . 'width'
        );

        $args[] = array(
            'option_group' 	=> 'jitsi-pro-config',
            'option_name' 	=> $this->prefix . 'height'
        );

        $this->settings->setSettings($args);
    }

    public function setSections()
    {
        $args = array(
            array(
                'id'    => $this->prefix . 'api_section',
                'title' => null,
                'page' => 'jitsi-pro-api'
            ),
            array(
                'id'    => $this->prefix . 'admin_section',
                'title' => null,
                'page' => 'jitsi-pro-admin'
            ),
            array(
                'id'	=> $this->prefix . 'config_section',
                'title'	=> null,
                'page'	=> 'jitsi-pro-config'
            ),
            array(
                'id'	=> $this->prefix . 'audio_section',
                'title'	=> null,
                'page'	=> 'jitsi-pro-audio'
            ),
            array(
                'id'	=> $this->prefix . 'video_section',
                'title'	=> null,
                'page'	=> 'jitsi-pro-video'
            )
        );

        $this->settings->setSections($args);
    }

    public function setFields()
    {
        $args = array();

        $args[] = [
            'id'       => $this->prefix . 'select_api',
            'title'    => __('Select API', 'jitsi-pro') . '<span class="description">' . __('Select your api', 'jitsi-pro') . '</span>',
            'callback' => [$this->callbacks, 'jitsi_multiswitch'],
            'page'     => 'jitsi-pro-api',
            'section'  => $this->prefix . 'api_section',
            'args'     => [
                'label_for' => $this->prefix . 'select_api',
                'default'   => 'free',
                'options'   => array(
                    'free'     => '<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48" fill="none">
                    <circle cx="24" cy="24" r="24" fill="#F45162"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M17.6605 24.45L17.6644 24.4454C17.8037 24.2778 18.9284 22.9261 18.9284 21.6027C18.9284 19.2784 16.9661 18 14.5511 18C11.5322 18.029 10.2039 19.8013 10.2039 21.6027C10.2039 22.6777 10.6869 23.404 11.3812 23.9852C10.9888 24.2757 9.6001 25.4088 9.6001 27.0359C9.6001 29.0115 11.2605 30.9 14.4001 30.9C17.6001 30.9 19.2001 29.0115 19.2001 27.0939C19.2001 25.8446 18.536 25.031 17.6605 24.45ZM27.8414 22.5H24.7892L24.0848 23.8132C23.9968 24.0175 23.8207 24.251 23.8207 24.251H23.7913C23.7913 24.251 23.6446 24.0175 23.5272 23.8132L22.8523 22.5H19.8L22.2065 26.2354L19.8294 30H22.8229L23.6446 28.4533C23.7326 28.3074 23.8501 28.0447 23.8501 28.0447H23.8793C23.8793 28.0447 23.9968 28.3074 24.0848 28.4533L24.9065 30H27.9L25.5229 26.2354L27.8414 22.5ZM15.6001 21.8613C15.6001 21.1645 15.1144 20.7 14.3144 20.7C13.543 20.7 13.2001 21.1355 13.2001 21.6871C13.2001 22.5 14.0858 22.9355 15.1715 23.4C15.2858 23.1968 15.6001 22.6161 15.6001 21.8613ZM36.8605 24.4792L36.8643 24.4745C37.0037 24.3063 38.1284 22.9483 38.1284 21.619C38.1284 19.2842 36.1661 18 33.7509 18C30.7321 18 29.4038 19.7803 29.4038 21.5898C29.4038 22.6697 29.8869 23.3993 30.5812 23.983C30.1887 24.2749 28.8001 25.4131 28.8001 27.0184C28.8001 29.003 30.4604 30.9 33.6 30.9C36.8001 30.9 38.4001 29.003 38.4001 27.0768C38.4001 25.8801 37.736 25.0629 36.8605 24.4792ZM12.9002 26.7294C12.9002 27.5529 13.5701 28.2001 14.4148 28.2001C15.3468 28.2001 15.9002 27.7589 15.9002 26.9059C15.9002 26.2552 14.9596 25.8295 13.8543 25.3291H13.8542L13.8532 25.3286C13.7599 25.2864 13.6653 25.2436 13.5701 25.2C13.2788 25.5236 12.9002 26.053 12.9002 26.7294ZM33.6147 28.2001C32.77 28.2001 32.1001 27.5529 32.1001 26.7294C32.1001 26.053 32.4788 25.5236 32.77 25.2C32.8656 25.2437 32.9605 25.2866 33.0541 25.3291H33.0542C34.1595 25.8295 35.1002 26.2552 35.1002 26.9059C35.1002 27.7589 34.5467 28.2001 33.6147 28.2001ZM34.8 21.8613C34.8 21.1645 34.3144 20.7 33.5143 20.7C32.7429 20.7 32.4001 21.1355 32.4001 21.6871C32.4001 22.5 33.2857 22.9355 34.3715 23.4C34.4857 23.1968 34.8 22.6161 34.8 21.8613Z" fill="white"/>
                    </svg><span>JaaS 8x8 Free</span><span class="desc">Get access to all the free features of the video conference application Jitsi Meet.</span>',
                    'jaas'     => '<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48" fill="none">
                    <circle cx="24" cy="24" r="24" fill="#F45162"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M17.6605 24.45L17.6644 24.4454C17.8037 24.2778 18.9284 22.9261 18.9284 21.6027C18.9284 19.2784 16.9661 18 14.5511 18C11.5322 18.029 10.2039 19.8013 10.2039 21.6027C10.2039 22.6777 10.6869 23.404 11.3812 23.9852C10.9888 24.2757 9.6001 25.4088 9.6001 27.0359C9.6001 29.0115 11.2605 30.9 14.4001 30.9C17.6001 30.9 19.2001 29.0115 19.2001 27.0939C19.2001 25.8446 18.536 25.031 17.6605 24.45ZM27.8414 22.5H24.7892L24.0848 23.8132C23.9968 24.0175 23.8207 24.251 23.8207 24.251H23.7913C23.7913 24.251 23.6446 24.0175 23.5272 23.8132L22.8523 22.5H19.8L22.2065 26.2354L19.8294 30H22.8229L23.6446 28.4533C23.7326 28.3074 23.8501 28.0447 23.8501 28.0447H23.8793C23.8793 28.0447 23.9968 28.3074 24.0848 28.4533L24.9065 30H27.9L25.5229 26.2354L27.8414 22.5ZM15.6001 21.8613C15.6001 21.1645 15.1144 20.7 14.3144 20.7C13.543 20.7 13.2001 21.1355 13.2001 21.6871C13.2001 22.5 14.0858 22.9355 15.1715 23.4C15.2858 23.1968 15.6001 22.6161 15.6001 21.8613ZM36.8605 24.4792L36.8643 24.4745C37.0037 24.3063 38.1284 22.9483 38.1284 21.619C38.1284 19.2842 36.1661 18 33.7509 18C30.7321 18 29.4038 19.7803 29.4038 21.5898C29.4038 22.6697 29.8869 23.3993 30.5812 23.983C30.1887 24.2749 28.8001 25.4131 28.8001 27.0184C28.8001 29.003 30.4604 30.9 33.6 30.9C36.8001 30.9 38.4001 29.003 38.4001 27.0768C38.4001 25.8801 37.736 25.0629 36.8605 24.4792ZM12.9002 26.7294C12.9002 27.5529 13.5701 28.2001 14.4148 28.2001C15.3468 28.2001 15.9002 27.7589 15.9002 26.9059C15.9002 26.2552 14.9596 25.8295 13.8543 25.3291H13.8542L13.8532 25.3286C13.7599 25.2864 13.6653 25.2436 13.5701 25.2C13.2788 25.5236 12.9002 26.053 12.9002 26.7294ZM33.6147 28.2001C32.77 28.2001 32.1001 27.5529 32.1001 26.7294C32.1001 26.053 32.4788 25.5236 32.77 25.2C32.8656 25.2437 32.9605 25.2866 33.0541 25.3291H33.0542C34.1595 25.8295 35.1002 26.2552 35.1002 26.9059C35.1002 27.7589 34.5467 28.2001 33.6147 28.2001ZM34.8 21.8613C34.8 21.1645 34.3144 20.7 33.5143 20.7C32.7429 20.7 32.4001 21.1355 32.4001 21.6871C32.4001 22.5 33.2857 22.9355 34.3715 23.4C34.4857 23.1968 34.8 22.6161 34.8 21.8613Z" fill="white"/>
                    </svg><span>JaaS 8x8</span><span class="desc">Access to enterprise standard video meeting features and modify everything as you like.</span>',
                    'self'    => '<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48" fill="none">
                    <circle cx="24" cy="24" r="24" fill="#10D7B0"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M31.0255 19.1455C31.0255 18.2051 30.6519 17.3033 29.987 16.6384C29.3221 15.9735 28.4203 15.6 27.48 15.6H15.0655C14.1252 15.6 13.2234 15.9735 12.5585 16.6384C11.8936 17.3033 11.52 18.2051 11.52 19.1455V23.7807C12.6884 23.0532 14.0463 22.6587 15.4449 22.6587C17.4144 22.6587 19.3032 23.4411 20.6958 24.8337C22.0884 26.2263 22.8707 28.1151 22.8707 30.0845C22.8707 30.9569 22.7172 31.8135 22.4251 32.6182H27.48C28.4203 32.6182 29.3221 32.2447 29.987 31.5798C30.6519 30.9149 31.0255 30.013 31.0255 29.0727V19.1455ZM21.2893 30.0847C21.2893 30.9702 21.0884 31.8351 20.7117 32.6182H16.1536V30.7932H18.2797C18.4676 30.7932 18.6479 30.7185 18.7808 30.5856C18.9137 30.4527 18.9884 30.2725 18.9884 30.0845C18.9884 29.8965 18.9137 29.7163 18.7808 29.5834C18.6479 29.4505 18.4676 29.3758 18.2797 29.3758H16.1536V27.2498C16.1536 27.0618 16.079 26.8815 15.946 26.7486C15.8131 26.6157 15.6329 26.5411 15.4449 26.5411C15.257 26.5411 15.0767 26.6157 14.9438 26.7486C14.8109 26.8815 14.7362 27.0618 14.7362 27.2498V29.3758H12.6102C12.4222 29.3758 12.242 29.4505 12.1091 29.5834C11.9762 29.7163 11.9015 29.8965 11.9015 30.0845C11.9015 30.2725 11.9762 30.4527 12.1091 30.5856C12.242 30.7185 12.4222 30.7932 12.6102 30.7932H14.7362V32.6029C13.9167 32.5265 13.1453 32.1666 12.5585 31.5798C11.8936 30.9149 11.52 30.013 11.52 29.0727V25.7536C12.5924 24.7818 13.9906 24.24 15.4446 24.24C16.9947 24.24 18.4814 24.8558 19.5775 25.9519C20.6736 27.048 21.2893 28.5346 21.2893 30.0847ZM32.16 27.2473L36.1088 30.3309C37.0403 31.0582 38.4 30.3945 38.4 29.2134V19.0053C38.4 17.8237 37.0403 17.1606 36.1088 17.8878L32.16 20.9709V27.2473Z" fill="white"/>
                    <path d="M21.2895 30.0847C21.2895 31.6348 20.6737 33.1214 19.5776 34.2175C18.4815 35.3136 16.9949 35.9294 15.4448 35.9294C13.8947 35.9294 12.4081 35.3136 11.312 34.2175C10.2159 33.1214 9.6001 31.6348 9.6001 30.0847C9.6001 28.5346 10.2159 27.048 11.312 25.9519C12.4081 24.8558 13.8947 24.24 15.4448 24.24C16.9949 24.24 18.4815 24.8558 19.5776 25.9519C20.6737 27.048 21.2895 28.5346 21.2895 30.0847ZM16.0942 27.487C16.0942 27.3148 16.0258 27.1496 15.904 27.0278C15.7822 26.9061 15.617 26.8376 15.4448 26.8376C15.2726 26.8376 15.1074 26.9061 14.9856 27.0278C14.8638 27.1496 14.7954 27.3148 14.7954 27.487V29.4353H12.8472C12.6749 29.4353 12.5097 29.5037 12.388 29.6255C12.2662 29.7473 12.1977 29.9125 12.1977 30.0847C12.1977 30.2569 12.2662 30.4221 12.388 30.5439C12.5097 30.6657 12.6749 30.7341 12.8472 30.7341H14.7954V32.6823C14.7954 32.8546 14.8638 33.0198 14.9856 33.1415C15.1074 33.2633 15.2726 33.3318 15.4448 33.3318C15.617 33.3318 15.7822 33.2633 15.904 33.1415C16.0258 33.0198 16.0942 32.8546 16.0942 32.6823V30.7341H18.0424C18.2147 30.7341 18.3799 30.6657 18.5017 30.5439C18.6234 30.4221 18.6919 30.2569 18.6919 30.0847C18.6919 29.9125 18.6234 29.7473 18.5017 29.6255C18.3799 29.5037 18.2147 29.4353 18.0424 29.4353H16.0942V27.487Z" fill="white"/>
                    </svg><span>Self Hosted</span><span class="desc">Opportunity to host Jitsi Meet on self-hosting with a branded domain and high security.</span>'
                )
            ]
        ];

        $args[] = [
            'id'       => $this->prefix . 'free_domain',
            'title'    => __('Hosted Domain', 'jitsi-pro') . '<span class="description">' . __('Domain where jitsi hosted', 'jitsi-pro') . '</span>',
            'callback' => [$this->callbacks, 'jitsi_general_disable'],
            'page'     => 'jitsi-pro-api',
            'section'  => $this->prefix . 'api_section',
            'args'     => [
                'label_for' => $this->prefix . 'free_domain',
                'default'   => '8x8.vc',
                'depend'    => array(
                    array(
                        'field'     => $this->prefix . 'select_api',
                        'value'     => 'free'
                    )
                )
            ]
        ];

        $args[] = [
            'id'       => $this->prefix . 'custom_domain',
            'title'    => __('Hosted Domain', 'jitsi-pro') . '<span class="description">' . __('Without http or https', 'jitsi-pro') . '</span>',
            'callback' => [$this->callbacks, 'jitsi_general'],
            'page'     => 'jitsi-pro-api',
            'section'  => $this->prefix . 'api_section',
            'args'     => [
                'label_for' => $this->prefix . 'custom_domain',
                'default'   => '8x8.vc',
                'depend'    => array(
                    array(
                        'field'     => $this->prefix . 'select_api',
                        'value'     => 'self'
                    )
                )
            ]
        ];

        $args[] = [
            'id'       => $this->prefix . 'app_id',
            'title'    => sprintf(
                '%1$s<svg size="20" viewBox="0 0 24 24" class="sc-htoDjs brCwHT"><g><path fill-rule="evenodd" d="M12 22C6.4771 22 2 17.5228 2 12 2 6.4771 6.4771 2 12 2c5.5228 0 10 4.4771 10 10 0 5.5228-4.4772 10-10 10zm0-2c4.4183 0 8-3.5817 8-8s-3.5817-8-8-8-8 3.5817-8 8 3.5817 8 8 8zm-.9023-2.1289c.2369.2057.5143.3086.832.3086.3073 0 .5781-.1042.8125-.3125.2344-.2083.3516-.4948.3516-.8594 0-.3281-.112-.6042-.336-.8281-.2239-.224-.5-.3359-.8281-.3359-.3333 0-.6146.1119-.8438.3359-.2291.2239-.3437.5-.3437.8281 0 .3698.1185.6576.3555.8633zm-2.504-9.7578c-.3177.5078-.4765 1.009-.4765 1.5039 0 .2396.1002.4622.3008.668.2005.2057.4466.3086.7383.3086.4947 0 .8307-.2943 1.0078-.8829.1875-.5625.4166-.9882.6875-1.2773.2708-.289.6927-.4336 1.2656-.4336.4896 0 .8893.1432 1.1992.4297.3099.2865.4648.638.4648 1.0547 0 .2135-.0507.4114-.1523.5937a2.242 2.242 0 0 1-.375.4961c-.1484.1485-.3893.3685-.7227.6602-.3802.3333-.6822.6211-.9062.8633-.224.2421-.4036.5234-.5391.8437-.1354.3203-.2031.6992-.2031 1.1367 0 .349.0925.612.2774.7891.1849.1771.4127.2656.6836.2656.5208 0 .8307-.2708.9296-.8125.0573-.2552.1003-.4336.1289-.5351a1.6557 1.6557 0 0 1 .1211-.3047c.0521-.1016.1315-.2136.2383-.336.1068-.1224.2487-.2643.4258-.4257.6406-.573 1.0846-.9805 1.332-1.2227.2474-.2422.461-.53.6407-.8633.1796-.3333.2695-.7213.2695-1.164 0-.5625-.1576-1.0834-.4727-1.5625-.3151-.4792-.7617-.8581-1.3398-1.1368-.5781-.2786-1.2448-.418-2-.418-.8125 0-1.5234.1667-2.1328.5-.6094.3334-1.073.754-1.3907 1.2618z" clip-rule="evenodd"></path></g></svg><span class="description">%2$s<a target="blank" href="%3$s">%4$s</a></span><span class="jitsi-admin-tooltip"><span class="tooltip-arrow"></span><img src="%5$s" alt="%6$s"/><strong>%6$s</strong></span>',
                __('APP ID', 'jitsi-pro'),
                __('Get it from ', 'jitsi-pro'),
                esc_url('https://jaas.8x8.vc/#/apikeys'),
                __('JAAS Admin', 'jitsi-pro'),
                JITSI_PRO_URL.'/assets/img/admin-setting-app.gif',
                __('Copy the APP ID from JaaS admin', 'jitsi-pro')
            ),
            'callback' => [$this->callbacks, 'jitsi_textrea'],
            'page'     => 'jitsi-pro-api',
            'section'  => $this->prefix . 'api_section',
            'args'     => [
                'label_for' => $this->prefix . 'app_id',
                'depend'    => array(
                    array(
                        'field'     => $this->prefix . 'select_api',
                        'value'     => 'jaas'
                    )
                )
            ]
        ];

        $args[] = [
            'id'       => $this->prefix . 'api_key',
            'title'    => sprintf(
                '%1$s<svg size="20" viewBox="0 0 24 24" class="sc-htoDjs brCwHT"><g><path fill-rule="evenodd" d="M12 22C6.4771 22 2 17.5228 2 12 2 6.4771 6.4771 2 12 2c5.5228 0 10 4.4771 10 10 0 5.5228-4.4772 10-10 10zm0-2c4.4183 0 8-3.5817 8-8s-3.5817-8-8-8-8 3.5817-8 8 3.5817 8 8 8zm-.9023-2.1289c.2369.2057.5143.3086.832.3086.3073 0 .5781-.1042.8125-.3125.2344-.2083.3516-.4948.3516-.8594 0-.3281-.112-.6042-.336-.8281-.2239-.224-.5-.3359-.8281-.3359-.3333 0-.6146.1119-.8438.3359-.2291.2239-.3437.5-.3437.8281 0 .3698.1185.6576.3555.8633zm-2.504-9.7578c-.3177.5078-.4765 1.009-.4765 1.5039 0 .2396.1002.4622.3008.668.2005.2057.4466.3086.7383.3086.4947 0 .8307-.2943 1.0078-.8829.1875-.5625.4166-.9882.6875-1.2773.2708-.289.6927-.4336 1.2656-.4336.4896 0 .8893.1432 1.1992.4297.3099.2865.4648.638.4648 1.0547 0 .2135-.0507.4114-.1523.5937a2.242 2.242 0 0 1-.375.4961c-.1484.1485-.3893.3685-.7227.6602-.3802.3333-.6822.6211-.9062.8633-.224.2421-.4036.5234-.5391.8437-.1354.3203-.2031.6992-.2031 1.1367 0 .349.0925.612.2774.7891.1849.1771.4127.2656.6836.2656.5208 0 .8307-.2708.9296-.8125.0573-.2552.1003-.4336.1289-.5351a1.6557 1.6557 0 0 1 .1211-.3047c.0521-.1016.1315-.2136.2383-.336.1068-.1224.2487-.2643.4258-.4257.6406-.573 1.0846-.9805 1.332-1.2227.2474-.2422.461-.53.6407-.8633.1796-.3333.2695-.7213.2695-1.164 0-.5625-.1576-1.0834-.4727-1.5625-.3151-.4792-.7617-.8581-1.3398-1.1368-.5781-.2786-1.2448-.418-2-.418-.8125 0-1.5234.1667-2.1328.5-.6094.3334-1.073.754-1.3907 1.2618z" clip-rule="evenodd"></path></g></svg><span class="description">%2$s<a target="blank" href="%3$s">%4$s</a></span><span class="jitsi-admin-tooltip"><span class="tooltip-arrow"></span><img src="%5$s" alt="%6$s"/><strong>%6$s</strong></span>',
                __('API Key', 'jitsi-pro'),
                __('Get it from ', 'jitsi-pro'),
                esc_url('https://jaas.8x8.vc/#/apikeys'),
                __('JAAS Admin', 'jitsi-pro'),
                JITSI_PRO_URL.'/assets/img/admin-setting-api.gif',
                __('Copy the API Key from JaaS admin', 'jitsi-pro')
            ),
            'callback' => [$this->callbacks, 'jitsi_textrea'],
            'page'     => 'jitsi-pro-api',
            'section'  => $this->prefix . 'api_section',
            'args'     => [
                'label_for' => $this->prefix . 'api_key',
                'depend'    => array(
                    array(
                        'field'     => $this->prefix . 'select_api',
                        'value'     => 'jaas'
                    )
                )
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'private_key',
            'title'		=> sprintf(
                '%1$s<svg size="20" viewBox="0 0 24 24" class="sc-htoDjs brCwHT"><g><path fill-rule="evenodd" d="M12 22C6.4771 22 2 17.5228 2 12 2 6.4771 6.4771 2 12 2c5.5228 0 10 4.4771 10 10 0 5.5228-4.4772 10-10 10zm0-2c4.4183 0 8-3.5817 8-8s-3.5817-8-8-8-8 3.5817-8 8 3.5817 8 8 8zm-.9023-2.1289c.2369.2057.5143.3086.832.3086.3073 0 .5781-.1042.8125-.3125.2344-.2083.3516-.4948.3516-.8594 0-.3281-.112-.6042-.336-.8281-.2239-.224-.5-.3359-.8281-.3359-.3333 0-.6146.1119-.8438.3359-.2291.2239-.3437.5-.3437.8281 0 .3698.1185.6576.3555.8633zm-2.504-9.7578c-.3177.5078-.4765 1.009-.4765 1.5039 0 .2396.1002.4622.3008.668.2005.2057.4466.3086.7383.3086.4947 0 .8307-.2943 1.0078-.8829.1875-.5625.4166-.9882.6875-1.2773.2708-.289.6927-.4336 1.2656-.4336.4896 0 .8893.1432 1.1992.4297.3099.2865.4648.638.4648 1.0547 0 .2135-.0507.4114-.1523.5937a2.242 2.242 0 0 1-.375.4961c-.1484.1485-.3893.3685-.7227.6602-.3802.3333-.6822.6211-.9062.8633-.224.2421-.4036.5234-.5391.8437-.1354.3203-.2031.6992-.2031 1.1367 0 .349.0925.612.2774.7891.1849.1771.4127.2656.6836.2656.5208 0 .8307-.2708.9296-.8125.0573-.2552.1003-.4336.1289-.5351a1.6557 1.6557 0 0 1 .1211-.3047c.0521-.1016.1315-.2136.2383-.336.1068-.1224.2487-.2643.4258-.4257.6406-.573 1.0846-.9805 1.332-1.2227.2474-.2422.461-.53.6407-.8633.1796-.3333.2695-.7213.2695-1.164 0-.5625-.1576-1.0834-.4727-1.5625-.3151-.4792-.7617-.8581-1.3398-1.1368-.5781-.2786-1.2448-.418-2-.418-.8125 0-1.5234.1667-2.1328.5-.6094.3334-1.073.754-1.3907 1.2618z" clip-rule="evenodd"></path></g></svg><span class="description">%2$s</span><span class="jitsi-admin-tooltip"><span class="tooltip-arrow"></span><img src="%3$s" alt="%4$s"/><strong>%4$s</strong></span>',
                __('Private Key', 'jitsi-pro'),
                __('Paste private key content here ', 'jitsi-pro'),
                JITSI_PRO_URL.'/assets/img/admin-setting-genkey.gif',
                __('Generate private key and download', 'jitsi-pro')
            ),
           'callback'	=> [$this->callbacks, 'jitsi_textrea'],
            'page'		=> 'jitsi-pro-api',
            'section'	=> $this->prefix . 'api_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'private_key',
                'depend'    => array(
                    array(
                        'field'     => $this->prefix . 'select_api',
                        'value'     => 'jaas'
                    )
                )
            ]
        ];

        $args[] = [
            'id'       => $this->prefix . 'user_email',
            'title'    => __('User Email', 'jitsi-pro') . '<span class="description">' . __('Email address of admin', 'jitsi-pro') . '</span>',
            'callback' => [$this->callbacks, 'jitsi_general'],
            'page'     => 'jitsi-pro-admin',
            'section'  => $this->prefix . 'admin_section',
            'args'     => [
                'label_for' => $this->prefix . 'user_email'
            ]
        ];

        $args[] = [
            'id'       => $this->prefix . 'user_name',
            'title'    => __('User Username', 'jitsi-pro') . '<span class="description">' . __('Username of the admin', 'jitsi-pro') . '</span>',
            'callback' => [$this->callbacks, 'jitsi_general'],
            'page'     => 'jitsi-pro-admin',
            'section'  => $this->prefix . 'admin_section',
            'args'     => [
                'label_for' => $this->prefix . 'user_name'
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'user_avatar',
            'title'		=> __('User Avatar', 'jitsti-pro') . '<span class="description">' . __('The avatar url of the admin user', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_single_img'],
            'page'		=> 'jitsi-pro-admin',
            'section'	=> $this->prefix . 'admin_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'user_avatar',
                'default'   => JITSI_PRO_URL.'/assets/img/avatar.png',
                'size'      => __('280x70px png, up to 4MB', 'jitsi-pro')
            ]
        ];

        $args[] = [
            'id'       => $this->prefix . 'user_is_moderator',
            'title'    => __('User is Moderator', 'jitsi-pro') . '<span class="description">' . __('Set default user as moderator', 'jitsi-pro') . '</span>',
            'callback' => [$this->callbacks, 'jitsi_switch'],
            'page'     => 'jitsi-pro-admin',
            'section'  => $this->prefix . 'admin_section',
            'args'     => [
                'label_for' => $this->prefix . 'user_is_moderator',
                'default'   => 1
            ]
        ];

        $args[] = [
            'id'       => $this->prefix . 'other_admin_config',
            'title'    => __('Other Admin Config', 'jitsi-pro') . '<span class="description">' . __('Those are on https://jaas.8x8.vc/#/branding', 'jitsi-pro') . '</span>',
            'callback' => [$this->callbacks, 'jitsi_other_admin'],
            'page'     => 'jitsi-pro-admin',
            'section'  => $this->prefix . 'admin_section',
            'args'     => [
                'label_for' => $this->prefix . 'other_admin_config',
                'default'   => 1
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'width',
            'title'		=> __('Meeting Width', 'jitsti-pro') . '<span class="description">' . __('Width in pixel when not on fullscreen', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_number'],
            'page'		=> 'jitsi-pro-config',
            'section'	=> $this->prefix . 'config_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'width',
                'default'   => 1080
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'height',
            'title'		=> __('Meeting Height', 'jitsti-pro') . '<span class="description">' . __('Height in pixel when not on fullscreen', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_number'],
            'page'		=> 'jitsi-pro-config',
            'section'	=> $this->prefix . 'config_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'height',
                'default'   => 720
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'enableWelcomePage',
            'title'		=> __('Welcome Page', 'jitsti-pro') . '<span class="description">' . __('Enable/Disable welcome page', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_switch'],
            'page'		=> 'jitsi-pro-config',
            'section'	=> $this->prefix . 'config_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'enableWelcomePage',
                'default'   => 1
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'invite',
            'title'		=> __('Enable Inviting', 'jitsti-pro') . '<span class="description">' . __('Attendee can invite people', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_switch'],
            'page'		=> 'jitsi-pro-config',
            'section'	=> $this->prefix . 'config_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'invite',
                'default'   => 1
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'start_audio_only',
            'title'		=> __('Audio Only', 'jitsti-pro') . '<span class="description">' . __('Start conference on audio only', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_switch'],
            'page'		=> 'jitsi-pro-audio',
            'section'	=> $this->prefix . 'audio_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'start_audio_only',
                'default'   => 0
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'start_audio_muted',
            'title'		=> __('Audio Muted', 'jitsti-pro') . '<span class="description">' . __('Participant after nth will be muted', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_number'],
            'page'		=> 'jitsi-pro-audio',
            'section'	=> $this->prefix . 'audio_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'start_audio_muted',
                'default'   => 10
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'start_local_audio_muted',
            'title'		=> __('Yourself muted', 'jitsti-pro') . '<span class="description">' . __('Start with yourself muted', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_switch'],
            'page'		=> 'jitsi-pro-audio',
            'section'	=> $this->prefix . 'audio_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'start_local_audio_muted',
                'default'   => 0
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'start_silent',
            'title'		=> __('Start Silent', 'jitsti-pro') . '<span class="description">' . __('Disable local audio output', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_switch'],
            'page'		=> 'jitsi-pro-audio',
            'section'	=> $this->prefix . 'audio_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'start_silent',
                'default'   => 0
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'video_resolution',
            'title'		=> __('Video Resolution', 'jitsti-pro') . '<span class="description">' . __('Start with preferred resolution', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_select'],
            'page'		=> 'jitsi-pro-video',
            'section'	=> $this->prefix . 'video_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'video_resolution',
                'default'   => 720,
                'options'   => array(
                    480     => '480p',
                    720     => '720p',
                    1080    => '1080p',
                    1440    => '1440p',
                    2160    => '2160p',
                    4320    => '4320p'
                )
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'maxfullresolutionparticipant',
            'title'		=> __('Max Full Resolution', 'jitsti-pro') . '<span class="description">' . __('Number of participant with default resolution', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_number'],
            'page'		=> 'jitsi-pro-video',
            'section'	=> $this->prefix . 'video_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'maxfullresolutionparticipant',
                'default'   => 2
            ]
        ];

        
        $args[] = [
            'id'		=> $this->prefix . 'startWithVideoMuted',
            'title'		=> __('Start Video Muted', 'jitsti-pro') . '<span class="description">' . __('Start call with video muted', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_switch'],
            'page'		=> 'jitsi-pro-video',
            'section'	=> $this->prefix . 'video_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'startWithVideoMuted',
                'default'   => 0
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'startVideoMuted',
            'title'		=> __('Video Muted After', 'jitsti-pro') . '<span class="description">' . __('Every participant after nth will start video muted', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_number'],
            'page'		=> 'jitsi-pro-video',
            'section'	=> $this->prefix . 'video_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'startVideoMuted',
                'default'   => 10
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'startScreenSharing',
            'title'		=> __('Screen Sharing', 'jitsti-pro') . '<span class="description">' . __('Enabling this feature allow you to share your screen while attending a meeting', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_switch'],
            'page'		=> 'jitsi-pro-video',
            'section'	=> $this->prefix . 'video_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'startScreenSharing',
                'default'   => 0
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'enable_livestream',
            'title'		=> __('Enable Livestream', 'jitsti-pro') . '<span class="description">' . __('Turn on livestreaming', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_switch'],
            'page'		=> 'jitsi-pro-config',
            'section'	=> $this->prefix . 'config_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'enable_livestream',
                'default'   => 1
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'enable_recording',
            'title'		=> __('Enable Recording', 'jitsti-pro') . '<span class="description">' . __('Turn on to record the meeting', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_switch'],
            'page'		=> 'jitsi-pro-config',
            'section'	=> $this->prefix . 'config_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'enable_recording',
                'default'   => 1
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'enable_outbound',
            'title'		=> __('Enable Outbound', 'jitsti-pro') . '<span class="description">' . __('Allow outbound on the meeting', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_switch'],
            'page'		=> 'jitsi-pro-config',
            'section'	=> $this->prefix . 'config_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'enable_outbound',
                'default'   => 1
            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'enable_transcription',
            'title'		=> __('Enable Transcription', 'jitsti-pro') . '<span class="description">' . __('Transcript the meeting', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_switch'],
            'page'		=> 'jitsi-pro-config',
            'section'	=> $this->prefix . 'config_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'enable_transcription',
                'default'   => 1
            ]
        ];

        $args[] = [
            'id'        => $this->prefix . 'hide_jitsi_sidebar',
            'title'        => __('Hide Jitsi Sidebar', 'jitsti-pro') . '<span class="description">' . __('Enable to hide Jitsi sidebar from Jitsi Meeting room', 'jitsi-pro') . '</span>',
            'callback'    => [$this->callbacks, 'jitsi_switch'],
            'page'        => 'jitsi-pro-config',
            'section'    => $this->prefix . 'config_section',
            'args'        => [
                'label_for'    => $this->prefix . 'hide_jitsi_sidebar',
                'default'   => 0,
                //'feature_type'   => 'new',

            ]
        ];

        $args[] = [
            'id'		=> $this->prefix . 'disableSimulcast',
            'title'		=> __('Simulcast', 'jitsti-pro') . '<span class="description">' . __('Enable/Disable simulcast', 'jitsi-pro') . '</span>',
            'callback'	=> [$this->callbacks, 'jitsi_switch'],
            'page'		=> 'jitsi-pro-config',
            'section'	=> $this->prefix . 'config_section',
            'args'		=> [
                'label_for'	=> $this->prefix . 'disableSimulcast',
                'default'   => 0
            ]
        ];

        $this->settings->setFields($args);
    }
}

new Jitsi_Pro_Admin_Settings();
