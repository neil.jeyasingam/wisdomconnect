/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./blocks/src/blocks.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./blocks/src/blocks.js":
/*!******************************!*\
  !*** ./blocks/src/blocks.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _editor_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./editor.scss */ "./blocks/src/editor.scss");
/* harmony import */ var _editor_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_editor_scss__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./style.scss */ "./blocks/src/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_1__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var InspectorControls = wp.editor.InspectorControls;
var registerBlockType = wp.blocks.registerBlockType;
var __ = wp.i18n.__;
var _wp$element = wp.element,
    Component = _wp$element.Component,
    Fragment = _wp$element.Fragment;
var _wp$components = wp.components,
    PanelBody = _wp$components.PanelBody,
    TextControl = _wp$components.TextControl,
    RangeControl = _wp$components.RangeControl,
    CheckboxControl = _wp$components.CheckboxControl,
    ToggleControl = _wp$components.ToggleControl,
    SelectControl = _wp$components.SelectControl;
var withSelect = wp.data.withSelect;

var blockIcon = function blockIcon() {
  return wp.element.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    height: "40",
    viewBox: "0 0 49 28",
    fill: "none"
  }, wp.element.createElement("path", {
    "fill-rule": "evenodd",
    "clip-rule": "evenodd",
    d: "M34.4757 22.0614V17.2941L43.0323 23.4061C43.5361 23.7659 44.1987 23.814 44.7491 23.5307C45.2996 23.2474 45.6455 22.6803 45.6455 22.0612V5.53492C45.6455 4.91587 45.2996 4.34873 44.7491 4.06545C44.1987 3.78219 43.5361 3.8303 43.0323 4.19012L34.4757 10.3021V5.53504C34.4757 2.61741 31.8784 0.577148 29.0998 0.577148H8.62239C5.84387 0.577148 3.24658 2.61741 3.24658 5.53504V22.0614C3.24658 24.979 5.84387 27.0193 8.62239 27.0193H29.0998C31.8784 27.0193 34.4757 24.979 34.4757 22.0614ZM20.3316 18.1759C17.8232 16.8906 15.7668 14.8431 14.4904 12.3347L16.4404 10.3847C16.6886 10.1365 16.7596 9.79081 16.6621 9.48059C16.3341 8.48784 16.1568 7.42421 16.1568 6.31627C16.1568 5.82876 15.758 5.4299 15.2704 5.4299H12.1681C11.6807 5.4299 11.2818 5.82876 11.2818 6.31627C11.2818 14.6393 18.027 21.3845 26.35 21.3845C26.8375 21.3845 27.2364 20.9856 27.2364 20.4981V17.4047C27.2364 16.9172 26.8375 16.5183 26.35 16.5183C25.2509 16.5183 24.1784 16.341 23.1857 16.0131C22.8755 15.9068 22.5209 15.9865 22.2816 16.2258L20.3316 18.1759ZM25.8625 5.42103L26.4918 6.04149L20.8989 11.6345H24.5773V12.5209H19.2591V7.20264H20.1455V11.0051L25.8625 5.42103Z",
    fill: "#407BFF"
  }));
};




var EditBlock = /*#__PURE__*/function (_Component) {
  _inherits(EditBlock, _Component);

  var _super = _createSuper(EditBlock);

  function EditBlock(props) {
    var _this;

    _classCallCheck(this, EditBlock);

    _this = _super.call(this, props);
    _this.state = {
      postArr: []
    };
    return _this;
  }

  _createClass(EditBlock, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          setAttributes = _this$props.setAttributes,
          _this$props$attribute = _this$props.attributes,
          name = _this$props$attribute.name,
          fromGlobal = _this$props$attribute.fromGlobal;

      var _newName = Math.random().toString(36).substring(2, 15);

      if (!name) {
        setAttributes({
          name: _newName
        });
      }
    }
  }, {
    key: "toggleFromPost",
    value: function toggleFromPost() {
      if (this.props.posts && this.state.postArr.length < 1) {
        var options = [];
        this.props.posts.forEach(function (post) {
          options.push({
            value: post.id,
            label: post.title.rendered
          });
        });
        this.setState({
          postArr: options
        });
      }

      var _this$props2 = this.props,
          setAttributes = _this$props2.setAttributes,
          _this$props2$attribut = _this$props2.attributes,
          formPosts = _this$props2$attribut.formPosts,
          postId = _this$props2$attribut.postId;
      setAttributes({
        formPosts: !formPosts
      });

      if (!postId && this.props.posts.length > 0) {
        setAttributes({
          postId: this.props.posts[0].id,
          postTitle: this.props.posts[0].title.rendered
        });
      }
    }
  }, {
    key: "previewMock",
    value: function previewMock() {
      return wp.element.createElement("div", {
        className: "jitsi-preview-people-mock"
      }, wp.element.createElement("div", null, wp.element.createElement("div", {
        style: {
          backgroundImage: "url(".concat(jitsi_pro.plugin_url, "assets/img/01.png)")
        }
      })), wp.element.createElement("div", null, wp.element.createElement("div", {
        style: {
          backgroundImage: "url(".concat(jitsi_pro.plugin_url, "assets/img/02.png)")
        }
      })), wp.element.createElement("div", null, wp.element.createElement("div", {
        style: {
          backgroundImage: "url(".concat(jitsi_pro.plugin_url, "assets/img/03.png)")
        }
      })), wp.element.createElement("div", null, wp.element.createElement("div", {
        style: {
          backgroundImage: "url(".concat(jitsi_pro.plugin_url, "assets/img/04.png)")
        }
      })), wp.element.createElement("div", null, wp.element.createElement("div", {
        style: {
          backgroundImage: "url(".concat(jitsi_pro.plugin_url, "assets/img/05.png)")
        }
      })), wp.element.createElement("div", null, wp.element.createElement("div", {
        style: {
          backgroundImage: "url(".concat(jitsi_pro.plugin_url, "assets/img/06.png)")
        }
      })), wp.element.createElement("div", null, wp.element.createElement("div", {
        style: {
          backgroundImage: "url(".concat(jitsi_pro.plugin_url, "assets/img/07.png)")
        }
      })), wp.element.createElement("div", null, wp.element.createElement("div", {
        style: {
          backgroundImage: "url(".concat(jitsi_pro.plugin_url, "assets/img/08.png)")
        }
      })));
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props3 = this.props,
          attributes = _this$props3.attributes,
          setAttributes = _this$props3.setAttributes,
          posts = _this$props3.posts;
      var formPosts = attributes.formPosts,
          postId = attributes.postId,
          name = attributes.name,
          fromGlobal = attributes.fromGlobal,
          width = attributes.width,
          height = attributes.height,
          enablewelcomepage = attributes.enablewelcomepage,
          startaudioonly = attributes.startaudioonly,
          startaudiomuted = attributes.startaudiomuted,
          startwithaudiomuted = attributes.startwithaudiomuted,
          startsilent = attributes.startsilent,
          resolution = attributes.resolution,
          maxfullresolutionparticipant = attributes.maxfullresolutionparticipant,
          disablesimulcast = attributes.disablesimulcast,
          startvideomuted = attributes.startvideomuted,
          startwithvideomuted = attributes.startwithvideomuted,
          startscreensharing = attributes.startscreensharing,
          filerecordingsenabled = attributes.filerecordingsenabled,
          transcribingenabled = attributes.transcribingenabled,
          livestreamingenabled = attributes.livestreamingenabled,
          invite = attributes.invite;
      return wp.element.createElement(Fragment, null, wp.element.createElement(InspectorControls, null, wp.element.createElement(PanelBody, {
        title: __('Settings'),
        initialOpen: true
      }, wp.element.createElement(ToggleControl, {
        label: __("From Post?"),
        checked: formPosts,
        onChange: function onChange() {
          return _this2.toggleFromPost();
        }
      }), formPosts && wp.element.createElement(SelectControl, {
        label: __("Select Post"),
        value: postId,
        options: this.state.postArr,
        onChange: function onChange(val) {
          return setAttributes({
            postId: val,
            postTitle: posts.find(function (obj) {
              return obj.id == val;
            }).title.rendered
          });
        }
      }), !formPosts && wp.element.createElement(TextControl, {
        label: __('Name'),
        value: name,
        onChange: function onChange(val) {
          return setAttributes({
            name: val
          });
        }
      }), wp.element.createElement(CheckboxControl, {
        label: __('Config from global'),
        checked: fromGlobal,
        onChange: function onChange(val) {
          setAttributes({
            fromGlobal: val
          });

          if (!fromGlobal) {
            setAttributes({
              width: parseInt(jitsi_pro.meeting_width),
              height: parseInt(jitsi_pro.meeting_height),
              enablewelcomepage: parseInt(jitsi_pro.enablewelcomepage) ? true : false,
              startaudioonly: parseInt(jitsi_pro.startaudioonly) ? true : false,
              startaudiomuted: parseInt(jitsi_pro.startaudiomuted) ? parseInt(jitsi_pro.startaudiomuted) : 10,
              startwithaudiomuted: parseInt(jitsi_pro.startwithaudiomuted) ? true : false,
              startsilent: parseInt(jitsi_pro.startsilent) ? true : false,
              resolution: parseInt(jitsi_pro.resolution) ? parseInt(jitsi_pro.resolution) : 720,
              maxfullresolutionparticipant: parseInt(jitsi_pro.maxfullresolutionparticipant) ? parseInt(jitsi_pro.maxfullresolutionparticipant) : 2,
              disablesimulcast: parseInt(jitsi_pro.disablesimulcast) ? true : false,
              startvideomuted: parseInt(jitsi_pro.startvideomuted) ? true : false,
              startwithvideomuted: parseInt(jitsi_pro.startwithvideomuted) ? parseInt(jitsi_pro.startwithvideomuted) : 10,
              startscreensharing: parseInt(jitsi_pro.startscreensharing) ? true : false,
              filerecordingsenabled: parseInt(jitsi_pro.filerecordingsenabled) ? true : false,
              transcribingenabled: parseInt(jitsi_pro.transcribingenabled) ? true : false,
              livestreamingenabled: parseInt(jitsi_pro.livestreamingenabled) ? true : false,
              invite: parseInt(jitsi_pro.invite) ? true : false
            });
          }
        }
      }), !fromGlobal && wp.element.createElement("div", null, wp.element.createElement(RangeControl, {
        label: __('Width'),
        value: width,
        onChange: function onChange(val) {
          return setAttributes({
            width: val
          });
        },
        min: 100,
        max: 2000,
        step: 10
      }), wp.element.createElement(RangeControl, {
        label: __('Height'),
        value: height,
        onChange: function onChange(val) {
          return setAttributes({
            height: val
          });
        },
        min: 100,
        max: 2000,
        step: 10
      }), wp.element.createElement(CheckboxControl, {
        label: __('Welcome Page'),
        checked: enablewelcomepage,
        onChange: function onChange(val) {
          return setAttributes({
            enablewelcomepage: val
          });
        }
      }), wp.element.createElement(CheckboxControl, {
        label: __('Start Audio Only'),
        checked: startaudioonly,
        onChange: function onChange(val) {
          return setAttributes({
            startaudioonly: val
          });
        }
      }), wp.element.createElement(RangeControl, {
        label: __('Audio Muted After'),
        value: startaudiomuted,
        onChange: function onChange(val) {
          return setAttributes({
            startaudiomuted: val
          });
        },
        min: 0,
        max: 20,
        step: 1
      }), wp.element.createElement(CheckboxControl, {
        label: __('Yourself Muted'),
        checked: startwithaudiomuted,
        onChange: function onChange(val) {
          return setAttributes({
            startwithaudiomuted: val
          });
        }
      }), wp.element.createElement(CheckboxControl, {
        label: __('Start Silent'),
        checked: startsilent,
        onChange: function onChange(val) {
          return setAttributes({
            startsilent: val
          });
        }
      }), wp.element.createElement(SelectControl, {
        label: __("Resolution"),
        value: resolution,
        options: [{
          label: __('480p'),
          value: 480
        }, {
          label: __('720p'),
          value: 720
        }, {
          label: __('1080p'),
          value: 1080
        }, {
          label: __('1440p'),
          value: 1440
        }, {
          label: __('2160p'),
          value: 2160
        }, {
          label: __('4320p'),
          value: 4320
        }],
        onChange: function onChange(val) {
          return setAttributes({
            resolution: val
          });
        }
      }), wp.element.createElement(RangeControl, {
        label: __('Max Full Resolution'),
        value: maxfullresolutionparticipant,
        onChange: function onChange(val) {
          return setAttributes({
            maxfullresolutionparticipant: val
          });
        },
        min: 0,
        max: 20,
        step: 1
      }), wp.element.createElement(CheckboxControl, {
        label: __('Start Video Muted'),
        checked: startvideomuted,
        onChange: function onChange(val) {
          return setAttributes({
            startvideomuted: val
          });
        }
      }), wp.element.createElement(RangeControl, {
        label: __('Video Muted After'),
        value: startwithvideomuted,
        onChange: function onChange(val) {
          return setAttributes({
            startwithvideomuted: val
          });
        },
        min: 0,
        max: 50,
        step: 1
      }), wp.element.createElement(CheckboxControl, {
        label: __('Screen Sharing'),
        checked: startscreensharing,
        onChange: function onChange(val) {
          return setAttributes({
            startscreensharing: val
          });
        }
      }), wp.element.createElement(CheckboxControl, {
        label: __('Enable Recording'),
        checked: filerecordingsenabled,
        onChange: function onChange(val) {
          return setAttributes({
            filerecordingsenabled: val
          });
        }
      }), wp.element.createElement(CheckboxControl, {
        label: __('Enable Transcription'),
        checked: transcribingenabled,
        onChange: function onChange(val) {
          return setAttributes({
            transcribingenabled: val
          });
        }
      }), wp.element.createElement(CheckboxControl, {
        label: __('Enable Livestream'),
        checked: livestreamingenabled,
        onChange: function onChange(val) {
          return setAttributes({
            livestreamingenabled: val
          });
        }
      }), wp.element.createElement(CheckboxControl, {
        label: __('Simulcast'),
        checked: disablesimulcast,
        onChange: function onChange(val) {
          return setAttributes({
            disablesimulcast: val
          });
        }
      }), wp.element.createElement(CheckboxControl, {
        label: __('Enable Inviting'),
        checked: invite,
        onChange: function onChange(val) {
          return setAttributes({
            invite: val
          });
        }
      })))), wp.element.createElement("div", {
        id: "meeting-ui-preview",
        className: "preview-success preview-block"
      }, this.previewMock()));
    }
  }]);

  return EditBlock;
}(Component);

registerBlockType('jitsi-pro/jitsi-pro', {
  title: __('Jitsi Pro', 'jitsi-pro'),
  icon: blockIcon,
  category: 'embed',
  keywords: [__('jitsi', 'jitsi-pro'), __('meeting', 'jitsi-pro'), __('video', 'jitsi-pro'), __('conference', 'jitsi-pro'), __('zoom', 'jitsi-pro')],
  attributes: {
    formPosts: {
      type: 'boolean',
      "default": false
    },
    postId: {
      type: 'number',
      "default": ''
    },
    postTitle: {
      type: 'string',
      "default": ''
    },
    name: {
      type: 'string',
      "default": ''
    },
    width: {
      type: 'number',
      "default": 1080
    },
    height: {
      type: 'number',
      "default": 720
    },
    fromGlobal: {
      type: 'boolean',
      "default": false
    },
    enablewelcomepage: {
      type: 'boolean',
      "default": true
    },
    startaudioonly: {
      type: 'boolean',
      "default": false
    },
    startaudiomuted: {
      type: 'number',
      "default": 10
    },
    startwithaudiomuted: {
      type: 'boolean',
      "default": false
    },
    startsilent: {
      type: 'boolean',
      "default": false
    },
    resolution: {
      type: 'number',
      "default": 720
    },
    maxfullresolutionparticipant: {
      type: 'number',
      "default": 2
    },
    startvideomuted: {
      type: 'boolean',
      "default": true
    },
    startwithvideomuted: {
      type: 'number',
      "default": 10
    },
    startscreensharing: {
      type: 'boolean',
      "default": false
    },
    filerecordingsenabled: {
      type: 'boolean',
      "default": false
    },
    transcribingenabled: {
      type: 'boolean',
      "default": false
    },
    livestreamingenabled: {
      type: 'boolean',
      "default": false
    },
    disablesimulcast: {
      type: 'boolean',
      "default": false
    },
    invite: {
      type: 'boolean',
      "default": true
    }
  },
  edit: withSelect(function (select) {
    return {
      posts: select('core').getEntityRecords('postType', 'meeting', {
        per_page: -1
      })
    };
  })(EditBlock),
  save: function save(props) {
    var _props$attributes = props.attributes,
        formPosts = _props$attributes.formPosts,
        postId = _props$attributes.postId,
        postTitle = _props$attributes.postTitle,
        name = _props$attributes.name,
        width = _props$attributes.width,
        height = _props$attributes.height,
        enablewelcomepage = _props$attributes.enablewelcomepage,
        startaudioonly = _props$attributes.startaudioonly,
        startaudiomuted = _props$attributes.startaudiomuted,
        startwithaudiomuted = _props$attributes.startwithaudiomuted,
        startsilent = _props$attributes.startsilent,
        resolution = _props$attributes.resolution,
        maxfullresolutionparticipant = _props$attributes.maxfullresolutionparticipant,
        disablesimulcast = _props$attributes.disablesimulcast,
        startvideomuted = _props$attributes.startvideomuted,
        startwithvideomuted = _props$attributes.startwithvideomuted,
        startscreensharing = _props$attributes.startscreensharing,
        filerecordingsenabled = _props$attributes.filerecordingsenabled,
        transcribingenabled = _props$attributes.transcribingenabled,
        livestreamingenabled = _props$attributes.livestreamingenabled,
        invite = _props$attributes.invite;
    return wp.element.createElement("div", null, wp.element.createElement("div", {
      className: "jitsi-wrapper",
      "data-name": formPosts ? postTitle : name,
      "data-width": width,
      "data-height": height,
      "data-startaudioonly": startaudioonly,
      "data-startaudiomuted": startaudiomuted,
      "data-startwithaudiomuted": startwithaudiomuted,
      "data-startsilent": startsilent,
      "data-resolution": resolution,
      "data-maxfullresolutionparticipant": maxfullresolutionparticipant,
      "data-disablesimulcast": disablesimulcast,
      "data-startvideomuted": startvideomuted,
      "data-startwithvideomuted": startwithvideomuted,
      "data-startscreensharing": startscreensharing,
      "data-filerecordingsenabled": filerecordingsenabled,
      "data-transcribingenabled": transcribingenabled,
      "data-livestreamingenabled": livestreamingenabled,
      "data-enablewelcomepage": enablewelcomepage,
      "data-invite": invite,
      style: {
        width: "".concat(width, "px")
      }
    }));
  }
});

/***/ }),

/***/ "./blocks/src/editor.scss":
/*!********************************!*\
  !*** ./blocks/src/editor.scss ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./blocks/src/style.scss":
/*!*******************************!*\
  !*** ./blocks/src/style.scss ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vYmxvY2tzL3NyYy9ibG9ja3MuanMiLCJ3ZWJwYWNrOi8vLy4vYmxvY2tzL3NyYy9lZGl0b3Iuc2NzcyIsIndlYnBhY2s6Ly8vLi9ibG9ja3Mvc3JjL3N0eWxlLnNjc3MiXSwibmFtZXMiOlsiSW5zcGVjdG9yQ29udHJvbHMiLCJ3cCIsImVkaXRvciIsInJlZ2lzdGVyQmxvY2tUeXBlIiwiYmxvY2tzIiwiX18iLCJpMThuIiwiZWxlbWVudCIsIkNvbXBvbmVudCIsIkZyYWdtZW50IiwiY29tcG9uZW50cyIsIlBhbmVsQm9keSIsIlRleHRDb250cm9sIiwiUmFuZ2VDb250cm9sIiwiQ2hlY2tib3hDb250cm9sIiwiVG9nZ2xlQ29udHJvbCIsIlNlbGVjdENvbnRyb2wiLCJ3aXRoU2VsZWN0IiwiZGF0YSIsImJsb2NrSWNvbiIsIkVkaXRCbG9jayIsInByb3BzIiwic3RhdGUiLCJwb3N0QXJyIiwic2V0QXR0cmlidXRlcyIsImF0dHJpYnV0ZXMiLCJuYW1lIiwiZnJvbUdsb2JhbCIsIl9uZXdOYW1lIiwiTWF0aCIsInJhbmRvbSIsInRvU3RyaW5nIiwic3Vic3RyaW5nIiwicG9zdHMiLCJsZW5ndGgiLCJvcHRpb25zIiwiZm9yRWFjaCIsInBvc3QiLCJwdXNoIiwidmFsdWUiLCJpZCIsImxhYmVsIiwidGl0bGUiLCJyZW5kZXJlZCIsInNldFN0YXRlIiwiZm9ybVBvc3RzIiwicG9zdElkIiwicG9zdFRpdGxlIiwiYmFja2dyb3VuZEltYWdlIiwiaml0c2lfcHJvIiwicGx1Z2luX3VybCIsIndpZHRoIiwiaGVpZ2h0IiwiZW5hYmxld2VsY29tZXBhZ2UiLCJzdGFydGF1ZGlvb25seSIsInN0YXJ0YXVkaW9tdXRlZCIsInN0YXJ0d2l0aGF1ZGlvbXV0ZWQiLCJzdGFydHNpbGVudCIsInJlc29sdXRpb24iLCJtYXhmdWxscmVzb2x1dGlvbnBhcnRpY2lwYW50IiwiZGlzYWJsZXNpbXVsY2FzdCIsInN0YXJ0dmlkZW9tdXRlZCIsInN0YXJ0d2l0aHZpZGVvbXV0ZWQiLCJzdGFydHNjcmVlbnNoYXJpbmciLCJmaWxlcmVjb3JkaW5nc2VuYWJsZWQiLCJ0cmFuc2NyaWJpbmdlbmFibGVkIiwibGl2ZXN0cmVhbWluZ2VuYWJsZWQiLCJpbnZpdGUiLCJ0b2dnbGVGcm9tUG9zdCIsInZhbCIsImZpbmQiLCJvYmoiLCJwYXJzZUludCIsIm1lZXRpbmdfd2lkdGgiLCJtZWV0aW5nX2hlaWdodCIsInByZXZpZXdNb2NrIiwiaWNvbiIsImNhdGVnb3J5Iiwia2V5d29yZHMiLCJ0eXBlIiwiZWRpdCIsInNlbGVjdCIsImdldEVudGl0eVJlY29yZHMiLCJwZXJfcGFnZSIsInNhdmUiXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsRkEsSUFBUUEsaUJBQVIsR0FBOEJDLEVBQUUsQ0FBQ0MsTUFBakMsQ0FBUUYsaUJBQVI7QUFDQSxJQUFRRyxpQkFBUixHQUE4QkYsRUFBRSxDQUFDRyxNQUFqQyxDQUFRRCxpQkFBUjtBQUNBLElBQVFFLEVBQVIsR0FBZUosRUFBRSxDQUFDSyxJQUFsQixDQUFRRCxFQUFSO0FBQ0Esa0JBQWdDSixFQUFFLENBQUNNLE9BQW5DO0FBQUEsSUFBUUMsU0FBUixlQUFRQSxTQUFSO0FBQUEsSUFBbUJDLFFBQW5CLGVBQW1CQSxRQUFuQjtBQUNBLHFCQUFnR1IsRUFBRSxDQUFDUyxVQUFuRztBQUFBLElBQVFDLFNBQVIsa0JBQVFBLFNBQVI7QUFBQSxJQUFtQkMsV0FBbkIsa0JBQW1CQSxXQUFuQjtBQUFBLElBQWdDQyxZQUFoQyxrQkFBZ0NBLFlBQWhDO0FBQUEsSUFBOENDLGVBQTlDLGtCQUE4Q0EsZUFBOUM7QUFBQSxJQUErREMsYUFBL0Qsa0JBQStEQSxhQUEvRDtBQUFBLElBQThFQyxhQUE5RSxrQkFBOEVBLGFBQTlFO0FBQ0EsSUFBUUMsVUFBUixHQUF1QmhCLEVBQUUsQ0FBQ2lCLElBQTFCLENBQVFELFVBQVI7O0FBRUEsSUFBTUUsU0FBUyxHQUFHLFNBQVpBLFNBQVksR0FBTTtBQUN2QixTQUNDO0FBQUssU0FBSyxFQUFDLDRCQUFYO0FBQXdDLFVBQU0sRUFBQyxJQUEvQztBQUFvRCxXQUFPLEVBQUMsV0FBNUQ7QUFBd0UsUUFBSSxFQUFDO0FBQTdFLEtBQ1U7QUFBTSxpQkFBVSxTQUFoQjtBQUEwQixpQkFBVSxTQUFwQztBQUE4QyxLQUFDLEVBQUMsMG1DQUFoRDtBQUEycEMsUUFBSSxFQUFDO0FBQWhxQyxJQURWLENBREQ7QUFLQSxDQU5EOztBQVFBO0FBQ0E7O0lBRU1DLFM7Ozs7O0FBQ0wscUJBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQTs7QUFDbEIsOEJBQU9BLEtBQVA7QUFDTSxVQUFLQyxLQUFMLEdBQWE7QUFDVEMsYUFBTyxFQUFFO0FBREEsS0FBYjtBQUZZO0FBS2Y7Ozs7V0FFRCw2QkFBb0I7QUFDaEIsd0JBQTRELEtBQUtGLEtBQWpFO0FBQUEsVUFBUUcsYUFBUixlQUFRQSxhQUFSO0FBQUEsOENBQXVCQyxVQUF2QjtBQUFBLFVBQXFDQyxJQUFyQyx5QkFBcUNBLElBQXJDO0FBQUEsVUFBMkNDLFVBQTNDLHlCQUEyQ0EsVUFBM0M7O0FBQ0EsVUFBTUMsUUFBUSxHQUFHQyxJQUFJLENBQUNDLE1BQUwsR0FBY0MsUUFBZCxDQUF1QixFQUF2QixFQUEyQkMsU0FBM0IsQ0FBcUMsQ0FBckMsRUFBd0MsRUFBeEMsQ0FBakI7O0FBQ0EsVUFBSyxDQUFDTixJQUFOLEVBQWE7QUFDVEYscUJBQWEsQ0FBQztBQUFFRSxjQUFJLEVBQUVFO0FBQVIsU0FBRCxDQUFiO0FBQ0g7QUFDSjs7O1dBRUQsMEJBQWdCO0FBQ1osVUFBSSxLQUFLUCxLQUFMLENBQVdZLEtBQVgsSUFBb0IsS0FBS1gsS0FBTCxDQUFXQyxPQUFYLENBQW1CVyxNQUFuQixHQUE0QixDQUFwRCxFQUF1RDtBQUNuRCxZQUFJQyxPQUFPLEdBQUcsRUFBZDtBQUNBLGFBQUtkLEtBQUwsQ0FBV1ksS0FBWCxDQUFpQkcsT0FBakIsQ0FBeUIsVUFBQ0MsSUFBRCxFQUFVO0FBQy9CRixpQkFBTyxDQUFDRyxJQUFSLENBQ0k7QUFDSUMsaUJBQUssRUFBRUYsSUFBSSxDQUFDRyxFQURoQjtBQUVJQyxpQkFBSyxFQUFFSixJQUFJLENBQUNLLEtBQUwsQ0FBV0M7QUFGdEIsV0FESjtBQUtILFNBTkQ7QUFPQSxhQUFLQyxRQUFMLENBQWM7QUFBQ3JCLGlCQUFPLEVBQUVZO0FBQVYsU0FBZDtBQUNIOztBQUNELHlCQUE2RCxLQUFLZCxLQUFsRTtBQUFBLFVBQVFHLGFBQVIsZ0JBQVFBLGFBQVI7QUFBQSwrQ0FBdUJDLFVBQXZCO0FBQUEsVUFBcUNvQixTQUFyQyx5QkFBcUNBLFNBQXJDO0FBQUEsVUFBZ0RDLE1BQWhELHlCQUFnREEsTUFBaEQ7QUFDQXRCLG1CQUFhLENBQUM7QUFBRXFCLGlCQUFTLEVBQUUsQ0FBQ0E7QUFBZCxPQUFELENBQWI7O0FBQ0EsVUFBRyxDQUFDQyxNQUFELElBQVcsS0FBS3pCLEtBQUwsQ0FBV1ksS0FBWCxDQUFpQkMsTUFBakIsR0FBMEIsQ0FBeEMsRUFBMEM7QUFDdENWLHFCQUFhLENBQUM7QUFBRXNCLGdCQUFNLEVBQUUsS0FBS3pCLEtBQUwsQ0FBV1ksS0FBWCxDQUFpQixDQUFqQixFQUFvQk8sRUFBOUI7QUFBa0NPLG1CQUFTLEVBQUUsS0FBSzFCLEtBQUwsQ0FBV1ksS0FBWCxDQUFpQixDQUFqQixFQUFvQlMsS0FBcEIsQ0FBMEJDO0FBQXZFLFNBQUQsQ0FBYjtBQUNIO0FBQ0o7OztXQUVELHVCQUFhO0FBQ1QsYUFDSTtBQUFLLGlCQUFTLEVBQUM7QUFBZixTQUNJLHNDQUFLO0FBQUssYUFBSyxFQUFFO0FBQUNLLHlCQUFlLGdCQUFTQyxTQUFTLENBQUNDLFVBQW5CO0FBQWhCO0FBQVosUUFBTCxDQURKLEVBRUksc0NBQUs7QUFBSyxhQUFLLEVBQUU7QUFBQ0YseUJBQWUsZ0JBQVNDLFNBQVMsQ0FBQ0MsVUFBbkI7QUFBaEI7QUFBWixRQUFMLENBRkosRUFHSSxzQ0FBSztBQUFLLGFBQUssRUFBRTtBQUFDRix5QkFBZSxnQkFBU0MsU0FBUyxDQUFDQyxVQUFuQjtBQUFoQjtBQUFaLFFBQUwsQ0FISixFQUlJLHNDQUFLO0FBQUssYUFBSyxFQUFFO0FBQUNGLHlCQUFlLGdCQUFTQyxTQUFTLENBQUNDLFVBQW5CO0FBQWhCO0FBQVosUUFBTCxDQUpKLEVBS0ksc0NBQUs7QUFBSyxhQUFLLEVBQUU7QUFBQ0YseUJBQWUsZ0JBQVNDLFNBQVMsQ0FBQ0MsVUFBbkI7QUFBaEI7QUFBWixRQUFMLENBTEosRUFNSSxzQ0FBSztBQUFLLGFBQUssRUFBRTtBQUFDRix5QkFBZSxnQkFBU0MsU0FBUyxDQUFDQyxVQUFuQjtBQUFoQjtBQUFaLFFBQUwsQ0FOSixFQU9JLHNDQUFLO0FBQUssYUFBSyxFQUFFO0FBQUNGLHlCQUFlLGdCQUFTQyxTQUFTLENBQUNDLFVBQW5CO0FBQWhCO0FBQVosUUFBTCxDQVBKLEVBUUksc0NBQUs7QUFBSyxhQUFLLEVBQUU7QUFBQ0YseUJBQWUsZ0JBQVNDLFNBQVMsQ0FBQ0MsVUFBbkI7QUFBaEI7QUFBWixRQUFMLENBUkosQ0FESjtBQVlIOzs7V0FFSixrQkFBUTtBQUFBOztBQUNQLHlCQUlVLEtBQUs3QixLQUpmO0FBQUEsVUFDQ0ksVUFERCxnQkFDQ0EsVUFERDtBQUFBLFVBRUNELGFBRkQsZ0JBRUNBLGFBRkQ7QUFBQSxVQUdVUyxLQUhWLGdCQUdVQSxLQUhWO0FBTU0sVUFDSVksU0FESixHQXNCSXBCLFVBdEJKLENBQ0lvQixTQURKO0FBQUEsVUFFSUMsTUFGSixHQXNCSXJCLFVBdEJKLENBRUlxQixNQUZKO0FBQUEsVUFHSXBCLElBSEosR0FzQklELFVBdEJKLENBR0lDLElBSEo7QUFBQSxVQUlJQyxVQUpKLEdBc0JJRixVQXRCSixDQUlJRSxVQUpKO0FBQUEsVUFLSXdCLEtBTEosR0FzQkkxQixVQXRCSixDQUtJMEIsS0FMSjtBQUFBLFVBTUlDLE1BTkosR0FzQkkzQixVQXRCSixDQU1JMkIsTUFOSjtBQUFBLFVBT0lDLGlCQVBKLEdBc0JJNUIsVUF0QkosQ0FPSTRCLGlCQVBKO0FBQUEsVUFRSUMsY0FSSixHQXNCSTdCLFVBdEJKLENBUUk2QixjQVJKO0FBQUEsVUFTSUMsZUFUSixHQXNCSTlCLFVBdEJKLENBU0k4QixlQVRKO0FBQUEsVUFVSUMsbUJBVkosR0FzQkkvQixVQXRCSixDQVVJK0IsbUJBVko7QUFBQSxVQVdJQyxXQVhKLEdBc0JJaEMsVUF0QkosQ0FXSWdDLFdBWEo7QUFBQSxVQVlJQyxVQVpKLEdBc0JJakMsVUF0QkosQ0FZSWlDLFVBWko7QUFBQSxVQWFJQyw0QkFiSixHQXNCSWxDLFVBdEJKLENBYUlrQyw0QkFiSjtBQUFBLFVBY0lDLGdCQWRKLEdBc0JJbkMsVUF0QkosQ0FjSW1DLGdCQWRKO0FBQUEsVUFlSUMsZUFmSixHQXNCSXBDLFVBdEJKLENBZUlvQyxlQWZKO0FBQUEsVUFnQklDLG1CQWhCSixHQXNCSXJDLFVBdEJKLENBZ0JJcUMsbUJBaEJKO0FBQUEsVUFpQklDLGtCQWpCSixHQXNCSXRDLFVBdEJKLENBaUJJc0Msa0JBakJKO0FBQUEsVUFrQklDLHFCQWxCSixHQXNCSXZDLFVBdEJKLENBa0JJdUMscUJBbEJKO0FBQUEsVUFtQklDLG1CQW5CSixHQXNCSXhDLFVBdEJKLENBbUJJd0MsbUJBbkJKO0FBQUEsVUFvQklDLG9CQXBCSixHQXNCSXpDLFVBdEJKLENBb0JJeUMsb0JBcEJKO0FBQUEsVUFxQklDLE1BckJKLEdBc0JJMUMsVUF0QkosQ0FxQkkwQyxNQXJCSjtBQXdCTixhQUNDLHlCQUFDLFFBQUQsUUFDYSx5QkFBQyxpQkFBRCxRQUNJLHlCQUFDLFNBQUQ7QUFBVyxhQUFLLEVBQUU5RCxFQUFFLENBQUMsVUFBRCxDQUFwQjtBQUFrQyxtQkFBVyxFQUFFO0FBQS9DLFNBQ0kseUJBQUMsYUFBRDtBQUNJLGFBQUssRUFBRUEsRUFBRSxDQUFDLFlBQUQsQ0FEYjtBQUVJLGVBQU8sRUFBRXdDLFNBRmI7QUFHSSxnQkFBUSxFQUFFO0FBQUEsaUJBQU0sTUFBSSxDQUFDdUIsY0FBTCxFQUFOO0FBQUE7QUFIZCxRQURKLEVBTUt2QixTQUFTLElBQ04seUJBQUMsYUFBRDtBQUNJLGFBQUssRUFBRXhDLEVBQUUsQ0FBQyxhQUFELENBRGI7QUFFSSxhQUFLLEVBQUd5QyxNQUZaO0FBR0ksZUFBTyxFQUFFLEtBQUt4QixLQUFMLENBQVdDLE9BSHhCO0FBSUksZ0JBQVEsRUFBRyxrQkFBRThDLEdBQUY7QUFBQSxpQkFBVzdDLGFBQWEsQ0FBQztBQUFDc0Isa0JBQU0sRUFBRXVCLEdBQVQ7QUFBY3RCLHFCQUFTLEVBQUVkLEtBQUssQ0FBQ3FDLElBQU4sQ0FBVyxVQUFBQyxHQUFHO0FBQUEscUJBQUlBLEdBQUcsQ0FBQy9CLEVBQUosSUFBVTZCLEdBQWQ7QUFBQSxhQUFkLEVBQWlDM0IsS0FBakMsQ0FBdUNDO0FBQWhFLFdBQUQsQ0FBeEI7QUFBQTtBQUpmLFFBUFIsRUFjSyxDQUFDRSxTQUFELElBQ0cseUJBQUMsV0FBRDtBQUNJLGFBQUssRUFBRXhDLEVBQUUsQ0FBQyxNQUFELENBRGI7QUFFSSxhQUFLLEVBQUdxQixJQUZaO0FBR0ksZ0JBQVEsRUFBRyxrQkFBRTJDLEdBQUY7QUFBQSxpQkFBVzdDLGFBQWEsQ0FBQztBQUFDRSxnQkFBSSxFQUFFMkM7QUFBUCxXQUFELENBQXhCO0FBQUE7QUFIZixRQWZSLEVBcUJJLHlCQUFDLGVBQUQ7QUFDSSxhQUFLLEVBQUVoRSxFQUFFLENBQUMsb0JBQUQsQ0FEYjtBQUVJLGVBQU8sRUFBR3NCLFVBRmQ7QUFHSSxnQkFBUSxFQUFHLGtCQUFBMEMsR0FBRyxFQUFJO0FBQ2Q3Qyx1QkFBYSxDQUFDO0FBQUNHLHNCQUFVLEVBQUUwQztBQUFiLFdBQUQsQ0FBYjs7QUFDQSxjQUFHLENBQUMxQyxVQUFKLEVBQWU7QUFDWEgseUJBQWEsQ0FBQztBQUNWMkIsbUJBQUssRUFBRXFCLFFBQVEsQ0FBQ3ZCLFNBQVMsQ0FBQ3dCLGFBQVgsQ0FETDtBQUVWckIsb0JBQU0sRUFBRW9CLFFBQVEsQ0FBQ3ZCLFNBQVMsQ0FBQ3lCLGNBQVgsQ0FGTjtBQUdWckIsK0JBQWlCLEVBQUVtQixRQUFRLENBQUN2QixTQUFTLENBQUNJLGlCQUFYLENBQVIsR0FBd0MsSUFBeEMsR0FBK0MsS0FIeEQ7QUFJVkMsNEJBQWMsRUFBRWtCLFFBQVEsQ0FBQ3ZCLFNBQVMsQ0FBQ0ssY0FBWCxDQUFSLEdBQXFDLElBQXJDLEdBQTRDLEtBSmxEO0FBS1ZDLDZCQUFlLEVBQUVpQixRQUFRLENBQUN2QixTQUFTLENBQUNNLGVBQVgsQ0FBUixHQUFzQ2lCLFFBQVEsQ0FBQ3ZCLFNBQVMsQ0FBQ00sZUFBWCxDQUE5QyxHQUE0RSxFQUxuRjtBQU1WQyxpQ0FBbUIsRUFBRWdCLFFBQVEsQ0FBQ3ZCLFNBQVMsQ0FBQ08sbUJBQVgsQ0FBUixHQUEwQyxJQUExQyxHQUFpRCxLQU41RDtBQU9WQyx5QkFBVyxFQUFFZSxRQUFRLENBQUN2QixTQUFTLENBQUNRLFdBQVgsQ0FBUixHQUFrQyxJQUFsQyxHQUF5QyxLQVA1QztBQVFWQyx3QkFBVSxFQUFFYyxRQUFRLENBQUN2QixTQUFTLENBQUNTLFVBQVgsQ0FBUixHQUFpQ2MsUUFBUSxDQUFDdkIsU0FBUyxDQUFDUyxVQUFYLENBQXpDLEdBQWtFLEdBUnBFO0FBU1ZDLDBDQUE0QixFQUFFYSxRQUFRLENBQUN2QixTQUFTLENBQUNVLDRCQUFYLENBQVIsR0FBbURhLFFBQVEsQ0FBQ3ZCLFNBQVMsQ0FBQ1UsNEJBQVgsQ0FBM0QsR0FBc0csQ0FUMUg7QUFVVkMsOEJBQWdCLEVBQUVZLFFBQVEsQ0FBQ3ZCLFNBQVMsQ0FBQ1csZ0JBQVgsQ0FBUixHQUF1QyxJQUF2QyxHQUE4QyxLQVZ0RDtBQVdWQyw2QkFBZSxFQUFFVyxRQUFRLENBQUN2QixTQUFTLENBQUNZLGVBQVgsQ0FBUixHQUFzQyxJQUF0QyxHQUE2QyxLQVhwRDtBQVlWQyxpQ0FBbUIsRUFBRVUsUUFBUSxDQUFDdkIsU0FBUyxDQUFDYSxtQkFBWCxDQUFSLEdBQTBDVSxRQUFRLENBQUN2QixTQUFTLENBQUNhLG1CQUFYLENBQWxELEdBQW9GLEVBWi9GO0FBYVZDLGdDQUFrQixFQUFFUyxRQUFRLENBQUN2QixTQUFTLENBQUNjLGtCQUFYLENBQVIsR0FBeUMsSUFBekMsR0FBZ0QsS0FiMUQ7QUFjVkMsbUNBQXFCLEVBQUVRLFFBQVEsQ0FBQ3ZCLFNBQVMsQ0FBQ2UscUJBQVgsQ0FBUixHQUE0QyxJQUE1QyxHQUFtRCxLQWRoRTtBQWVWQyxpQ0FBbUIsRUFBRU8sUUFBUSxDQUFDdkIsU0FBUyxDQUFDZ0IsbUJBQVgsQ0FBUixHQUEwQyxJQUExQyxHQUFpRCxLQWY1RDtBQWdCVkMsa0NBQW9CLEVBQUVNLFFBQVEsQ0FBQ3ZCLFNBQVMsQ0FBQ2lCLG9CQUFYLENBQVIsR0FBMkMsSUFBM0MsR0FBa0QsS0FoQjlEO0FBaUJWQyxvQkFBTSxFQUFFSyxRQUFRLENBQUN2QixTQUFTLENBQUNrQixNQUFYLENBQVIsR0FBNkIsSUFBN0IsR0FBb0M7QUFqQmxDLGFBQUQsQ0FBYjtBQW1CSDtBQUNKO0FBMUJMLFFBckJKLEVBaURLLENBQUN4QyxVQUFELElBQ0csc0NBQ0kseUJBQUMsWUFBRDtBQUNJLGFBQUssRUFBRXRCLEVBQUUsQ0FBQyxPQUFELENBRGI7QUFFSSxhQUFLLEVBQUc4QyxLQUZaO0FBR0ksZ0JBQVEsRUFBRyxrQkFBRWtCLEdBQUY7QUFBQSxpQkFBVzdDLGFBQWEsQ0FBQztBQUFDMkIsaUJBQUssRUFBRWtCO0FBQVIsV0FBRCxDQUF4QjtBQUFBLFNBSGY7QUFJSSxXQUFHLEVBQUcsR0FKVjtBQUtJLFdBQUcsRUFBRyxJQUxWO0FBTUksWUFBSSxFQUFHO0FBTlgsUUFESixFQVNJLHlCQUFDLFlBQUQ7QUFDSSxhQUFLLEVBQUVoRSxFQUFFLENBQUMsUUFBRCxDQURiO0FBRUksYUFBSyxFQUFHK0MsTUFGWjtBQUdJLGdCQUFRLEVBQUcsa0JBQUVpQixHQUFGO0FBQUEsaUJBQVc3QyxhQUFhLENBQUM7QUFBQzRCLGtCQUFNLEVBQUVpQjtBQUFULFdBQUQsQ0FBeEI7QUFBQSxTQUhmO0FBSUksV0FBRyxFQUFHLEdBSlY7QUFLSSxXQUFHLEVBQUcsSUFMVjtBQU1JLFlBQUksRUFBRztBQU5YLFFBVEosRUFpQkkseUJBQUMsZUFBRDtBQUNJLGFBQUssRUFBRWhFLEVBQUUsQ0FBQyxjQUFELENBRGI7QUFFSSxlQUFPLEVBQUdnRCxpQkFGZDtBQUdJLGdCQUFRLEVBQUcsa0JBQUFnQixHQUFHO0FBQUEsaUJBQUk3QyxhQUFhLENBQUM7QUFBQzZCLDZCQUFpQixFQUFFZ0I7QUFBcEIsV0FBRCxDQUFqQjtBQUFBO0FBSGxCLFFBakJKLEVBc0JJLHlCQUFDLGVBQUQ7QUFDSSxhQUFLLEVBQUVoRSxFQUFFLENBQUMsa0JBQUQsQ0FEYjtBQUVJLGVBQU8sRUFBR2lELGNBRmQ7QUFHSSxnQkFBUSxFQUFHLGtCQUFBZSxHQUFHO0FBQUEsaUJBQUk3QyxhQUFhLENBQUM7QUFBQzhCLDBCQUFjLEVBQUVlO0FBQWpCLFdBQUQsQ0FBakI7QUFBQTtBQUhsQixRQXRCSixFQTJCSSx5QkFBQyxZQUFEO0FBQ0ksYUFBSyxFQUFFaEUsRUFBRSxDQUFDLG1CQUFELENBRGI7QUFFSSxhQUFLLEVBQUdrRCxlQUZaO0FBR0ksZ0JBQVEsRUFBRyxrQkFBRWMsR0FBRjtBQUFBLGlCQUFXN0MsYUFBYSxDQUFDO0FBQUMrQiwyQkFBZSxFQUFFYztBQUFsQixXQUFELENBQXhCO0FBQUEsU0FIZjtBQUlJLFdBQUcsRUFBRyxDQUpWO0FBS0ksV0FBRyxFQUFHLEVBTFY7QUFNSSxZQUFJLEVBQUc7QUFOWCxRQTNCSixFQW1DSSx5QkFBQyxlQUFEO0FBQ0ksYUFBSyxFQUFFaEUsRUFBRSxDQUFDLGdCQUFELENBRGI7QUFFSSxlQUFPLEVBQUdtRCxtQkFGZDtBQUdJLGdCQUFRLEVBQUcsa0JBQUFhLEdBQUc7QUFBQSxpQkFBSTdDLGFBQWEsQ0FBQztBQUFDZ0MsK0JBQW1CLEVBQUVhO0FBQXRCLFdBQUQsQ0FBakI7QUFBQTtBQUhsQixRQW5DSixFQXdDSSx5QkFBQyxlQUFEO0FBQ0ksYUFBSyxFQUFFaEUsRUFBRSxDQUFDLGNBQUQsQ0FEYjtBQUVJLGVBQU8sRUFBR29ELFdBRmQ7QUFHSSxnQkFBUSxFQUFHLGtCQUFBWSxHQUFHO0FBQUEsaUJBQUk3QyxhQUFhLENBQUM7QUFBQ2lDLHVCQUFXLEVBQUVZO0FBQWQsV0FBRCxDQUFqQjtBQUFBO0FBSGxCLFFBeENKLEVBNkNJLHlCQUFDLGFBQUQ7QUFDSSxhQUFLLEVBQUVoRSxFQUFFLENBQUMsWUFBRCxDQURiO0FBRUksYUFBSyxFQUFHcUQsVUFGWjtBQUdJLGVBQU8sRUFBRSxDQUNMO0FBQUVqQixlQUFLLEVBQUVwQyxFQUFFLENBQUMsTUFBRCxDQUFYO0FBQXFCa0MsZUFBSyxFQUFFO0FBQTVCLFNBREssRUFFTDtBQUFFRSxlQUFLLEVBQUVwQyxFQUFFLENBQUMsTUFBRCxDQUFYO0FBQXFCa0MsZUFBSyxFQUFFO0FBQTVCLFNBRkssRUFHTDtBQUFFRSxlQUFLLEVBQUVwQyxFQUFFLENBQUMsT0FBRCxDQUFYO0FBQXNCa0MsZUFBSyxFQUFFO0FBQTdCLFNBSEssRUFJTDtBQUFFRSxlQUFLLEVBQUVwQyxFQUFFLENBQUMsT0FBRCxDQUFYO0FBQXNCa0MsZUFBSyxFQUFFO0FBQTdCLFNBSkssRUFLTDtBQUFFRSxlQUFLLEVBQUVwQyxFQUFFLENBQUMsT0FBRCxDQUFYO0FBQXNCa0MsZUFBSyxFQUFFO0FBQTdCLFNBTEssRUFNTDtBQUFFRSxlQUFLLEVBQUVwQyxFQUFFLENBQUMsT0FBRCxDQUFYO0FBQXNCa0MsZUFBSyxFQUFFO0FBQTdCLFNBTkssQ0FIYjtBQVdJLGdCQUFRLEVBQUcsa0JBQUU4QixHQUFGO0FBQUEsaUJBQVc3QyxhQUFhLENBQUM7QUFBRWtDLHNCQUFVLEVBQUVXO0FBQWQsV0FBRCxDQUF4QjtBQUFBO0FBWGYsUUE3Q0osRUEwREkseUJBQUMsWUFBRDtBQUNJLGFBQUssRUFBRWhFLEVBQUUsQ0FBQyxxQkFBRCxDQURiO0FBRUksYUFBSyxFQUFHc0QsNEJBRlo7QUFHSSxnQkFBUSxFQUFHLGtCQUFFVSxHQUFGO0FBQUEsaUJBQVc3QyxhQUFhLENBQUM7QUFBQ21DLHdDQUE0QixFQUFFVTtBQUEvQixXQUFELENBQXhCO0FBQUEsU0FIZjtBQUlJLFdBQUcsRUFBRyxDQUpWO0FBS0ksV0FBRyxFQUFHLEVBTFY7QUFNSSxZQUFJLEVBQUc7QUFOWCxRQTFESixFQWtFSSx5QkFBQyxlQUFEO0FBQ0ksYUFBSyxFQUFFaEUsRUFBRSxDQUFDLG1CQUFELENBRGI7QUFFSSxlQUFPLEVBQUd3RCxlQUZkO0FBR0ksZ0JBQVEsRUFBRyxrQkFBQVEsR0FBRztBQUFBLGlCQUFJN0MsYUFBYSxDQUFDO0FBQUVxQywyQkFBZSxFQUFFUTtBQUFuQixXQUFELENBQWpCO0FBQUE7QUFIbEIsUUFsRUosRUF1RUkseUJBQUMsWUFBRDtBQUNJLGFBQUssRUFBRWhFLEVBQUUsQ0FBQyxtQkFBRCxDQURiO0FBRUksYUFBSyxFQUFHeUQsbUJBRlo7QUFHSSxnQkFBUSxFQUFHLGtCQUFFTyxHQUFGO0FBQUEsaUJBQVc3QyxhQUFhLENBQUM7QUFBQ3NDLCtCQUFtQixFQUFFTztBQUF0QixXQUFELENBQXhCO0FBQUEsU0FIZjtBQUlJLFdBQUcsRUFBRyxDQUpWO0FBS0ksV0FBRyxFQUFHLEVBTFY7QUFNSSxZQUFJLEVBQUc7QUFOWCxRQXZFSixFQStFSSx5QkFBQyxlQUFEO0FBQ0ksYUFBSyxFQUFFaEUsRUFBRSxDQUFDLGdCQUFELENBRGI7QUFFSSxlQUFPLEVBQUcwRCxrQkFGZDtBQUdJLGdCQUFRLEVBQUcsa0JBQUFNLEdBQUc7QUFBQSxpQkFBSTdDLGFBQWEsQ0FBQztBQUFFdUMsOEJBQWtCLEVBQUVNO0FBQXRCLFdBQUQsQ0FBakI7QUFBQTtBQUhsQixRQS9FSixFQW9GSSx5QkFBQyxlQUFEO0FBQ0ksYUFBSyxFQUFFaEUsRUFBRSxDQUFDLGtCQUFELENBRGI7QUFFSSxlQUFPLEVBQUcyRCxxQkFGZDtBQUdJLGdCQUFRLEVBQUcsa0JBQUFLLEdBQUc7QUFBQSxpQkFBSTdDLGFBQWEsQ0FBQztBQUFFd0MsaUNBQXFCLEVBQUVLO0FBQXpCLFdBQUQsQ0FBakI7QUFBQTtBQUhsQixRQXBGSixFQXlGSSx5QkFBQyxlQUFEO0FBQ0ksYUFBSyxFQUFFaEUsRUFBRSxDQUFDLHNCQUFELENBRGI7QUFFSSxlQUFPLEVBQUc0RCxtQkFGZDtBQUdJLGdCQUFRLEVBQUcsa0JBQUFJLEdBQUc7QUFBQSxpQkFBSTdDLGFBQWEsQ0FBQztBQUFFeUMsK0JBQW1CLEVBQUVJO0FBQXZCLFdBQUQsQ0FBakI7QUFBQTtBQUhsQixRQXpGSixFQThGSSx5QkFBQyxlQUFEO0FBQ0ksYUFBSyxFQUFFaEUsRUFBRSxDQUFDLG1CQUFELENBRGI7QUFFSSxlQUFPLEVBQUc2RCxvQkFGZDtBQUdJLGdCQUFRLEVBQUcsa0JBQUFHLEdBQUc7QUFBQSxpQkFBSTdDLGFBQWEsQ0FBQztBQUFFMEMsZ0NBQW9CLEVBQUVHO0FBQXhCLFdBQUQsQ0FBakI7QUFBQTtBQUhsQixRQTlGSixFQW1HSSx5QkFBQyxlQUFEO0FBQ0ksYUFBSyxFQUFFaEUsRUFBRSxDQUFDLFdBQUQsQ0FEYjtBQUVJLGVBQU8sRUFBR3VELGdCQUZkO0FBR0ksZ0JBQVEsRUFBRyxrQkFBQVMsR0FBRztBQUFBLGlCQUFJN0MsYUFBYSxDQUFDO0FBQUVvQyw0QkFBZ0IsRUFBRVM7QUFBcEIsV0FBRCxDQUFqQjtBQUFBO0FBSGxCLFFBbkdKLEVBd0dJLHlCQUFDLGVBQUQ7QUFDSSxhQUFLLEVBQUVoRSxFQUFFLENBQUMsaUJBQUQsQ0FEYjtBQUVJLGVBQU8sRUFBRzhELE1BRmQ7QUFHSSxnQkFBUSxFQUFHLGtCQUFBRSxHQUFHO0FBQUEsaUJBQUk3QyxhQUFhLENBQUM7QUFBRTJDLGtCQUFNLEVBQUVFO0FBQVYsV0FBRCxDQUFqQjtBQUFBO0FBSGxCLFFBeEdKLENBbERSLENBREosQ0FEYixFQXFLYTtBQUFLLFVBQUUsRUFBQyxvQkFBUjtBQUE2QixpQkFBUyxFQUFDO0FBQXZDLFNBQ0ssS0FBS00sV0FBTCxFQURMLENBcktiLENBREQ7QUEyS0E7Ozs7RUE1UHNCbkUsUzs7QUErUHhCTCxpQkFBaUIsQ0FBQyxxQkFBRCxFQUF3QjtBQUN2Q3VDLE9BQUssRUFBRXJDLEVBQUUsQ0FBQyxXQUFELEVBQWMsV0FBZCxDQUQ4QjtBQUV2Q3VFLE1BQUksRUFBRXpELFNBRmlDO0FBR3ZDMEQsVUFBUSxFQUFFLE9BSDZCO0FBSXZDQyxVQUFRLEVBQUUsQ0FDUnpFLEVBQUUsQ0FBRSxPQUFGLEVBQVcsV0FBWCxDQURNLEVBRVJBLEVBQUUsQ0FBRSxTQUFGLEVBQWEsV0FBYixDQUZNLEVBR1JBLEVBQUUsQ0FBRSxPQUFGLEVBQVcsV0FBWCxDQUhNLEVBSVJBLEVBQUUsQ0FBRSxZQUFGLEVBQWdCLFdBQWhCLENBSk0sRUFLUkEsRUFBRSxDQUFFLE1BQUYsRUFBVSxXQUFWLENBTE0sQ0FKNkI7QUFXdkNvQixZQUFVLEVBQUU7QUFDVm9CLGFBQVMsRUFBRTtBQUNQa0MsVUFBSSxFQUFFLFNBREM7QUFFUCxpQkFBUztBQUZGLEtBREQ7QUFLVmpDLFVBQU0sRUFBRTtBQUNKaUMsVUFBSSxFQUFFLFFBREY7QUFFSixpQkFBUztBQUZMLEtBTEU7QUFTVmhDLGFBQVMsRUFBRTtBQUNQZ0MsVUFBSSxFQUFFLFFBREM7QUFFUCxpQkFBUztBQUZGLEtBVEQ7QUFhVnJELFFBQUksRUFBRTtBQUNGcUQsVUFBSSxFQUFFLFFBREo7QUFFRixpQkFBUztBQUZQLEtBYkk7QUFpQlY1QixTQUFLLEVBQUU7QUFDSDRCLFVBQUksRUFBRSxRQURIO0FBRUgsaUJBQVM7QUFGTixLQWpCRztBQXFCVjNCLFVBQU0sRUFBRTtBQUNKMkIsVUFBSSxFQUFFLFFBREY7QUFFSixpQkFBUztBQUZMLEtBckJFO0FBeUJWcEQsY0FBVSxFQUFFO0FBQ1JvRCxVQUFJLEVBQUUsU0FERTtBQUVSLGlCQUFTO0FBRkQsS0F6QkY7QUE2QlYxQixxQkFBaUIsRUFBRTtBQUNmMEIsVUFBSSxFQUFFLFNBRFM7QUFFZixpQkFBUztBQUZNLEtBN0JUO0FBaUNWekIsa0JBQWMsRUFBRTtBQUNaeUIsVUFBSSxFQUFFLFNBRE07QUFFWixpQkFBUztBQUZHLEtBakNOO0FBcUNWeEIsbUJBQWUsRUFBRTtBQUNid0IsVUFBSSxFQUFFLFFBRE87QUFFYixpQkFBUztBQUZJLEtBckNQO0FBeUNWdkIsdUJBQW1CLEVBQUU7QUFDakJ1QixVQUFJLEVBQUUsU0FEVztBQUVqQixpQkFBUztBQUZRLEtBekNYO0FBNkNWdEIsZUFBVyxFQUFFO0FBQ1RzQixVQUFJLEVBQUUsU0FERztBQUVULGlCQUFTO0FBRkEsS0E3Q0g7QUFpRFZyQixjQUFVLEVBQUU7QUFDUnFCLFVBQUksRUFBRSxRQURFO0FBRVIsaUJBQVM7QUFGRCxLQWpERjtBQXFEVnBCLGdDQUE0QixFQUFFO0FBQzFCb0IsVUFBSSxFQUFFLFFBRG9CO0FBRTFCLGlCQUFTO0FBRmlCLEtBckRwQjtBQXlEVmxCLG1CQUFlLEVBQUU7QUFDYmtCLFVBQUksRUFBRSxTQURPO0FBRWIsaUJBQVM7QUFGSSxLQXpEUDtBQTZEVmpCLHVCQUFtQixFQUFFO0FBQ2pCaUIsVUFBSSxFQUFFLFFBRFc7QUFFakIsaUJBQVM7QUFGUSxLQTdEWDtBQWlFVmhCLHNCQUFrQixFQUFFO0FBQ2hCZ0IsVUFBSSxFQUFFLFNBRFU7QUFFaEIsaUJBQVM7QUFGTyxLQWpFVjtBQXFFVmYseUJBQXFCLEVBQUU7QUFDbkJlLFVBQUksRUFBRSxTQURhO0FBRW5CLGlCQUFTO0FBRlUsS0FyRWI7QUF5RVZkLHVCQUFtQixFQUFFO0FBQ2pCYyxVQUFJLEVBQUUsU0FEVztBQUVqQixpQkFBUztBQUZRLEtBekVYO0FBNkVWYix3QkFBb0IsRUFBRTtBQUNsQmEsVUFBSSxFQUFFLFNBRFk7QUFFbEIsaUJBQVM7QUFGUyxLQTdFWjtBQWlGVm5CLG9CQUFnQixFQUFFO0FBQ2RtQixVQUFJLEVBQUUsU0FEUTtBQUVkLGlCQUFTO0FBRkssS0FqRlI7QUFxRlZaLFVBQU0sRUFBRTtBQUNKWSxVQUFJLEVBQUUsU0FERjtBQUVKLGlCQUFTO0FBRkw7QUFyRkUsR0FYMkI7QUFxR3ZDQyxNQUFJLEVBQUUvRCxVQUFVLENBQUMsVUFBQ2dFLE1BQUQsRUFBWTtBQUMzQixXQUFPO0FBQ0hoRCxXQUFLLEVBQUVnRCxNQUFNLENBQUMsTUFBRCxDQUFOLENBQWVDLGdCQUFmLENBQWdDLFVBQWhDLEVBQTRDLFNBQTVDLEVBQXVEO0FBQUNDLGdCQUFRLEVBQUUsQ0FBQztBQUFaLE9BQXZEO0FBREosS0FBUDtBQUdELEdBSmUsQ0FBVixDQUlIL0QsU0FKRyxDQXJHaUM7QUEwR3ZDZ0UsTUFBSSxFQUFFLGNBQVUvRCxLQUFWLEVBQWtCO0FBQ3hCLDRCQXNCSUEsS0FBSyxDQUFDSSxVQXRCVjtBQUFBLFFBQ1VvQixTQURWLHFCQUNVQSxTQURWO0FBQUEsUUFFVUMsTUFGVixxQkFFVUEsTUFGVjtBQUFBLFFBR1VDLFNBSFYscUJBR1VBLFNBSFY7QUFBQSxRQUlVckIsSUFKVixxQkFJVUEsSUFKVjtBQUFBLFFBS1V5QixLQUxWLHFCQUtVQSxLQUxWO0FBQUEsUUFNVUMsTUFOVixxQkFNVUEsTUFOVjtBQUFBLFFBT1VDLGlCQVBWLHFCQU9VQSxpQkFQVjtBQUFBLFFBUVVDLGNBUlYscUJBUVVBLGNBUlY7QUFBQSxRQVNVQyxlQVRWLHFCQVNVQSxlQVRWO0FBQUEsUUFVVUMsbUJBVlYscUJBVVVBLG1CQVZWO0FBQUEsUUFXVUMsV0FYVixxQkFXVUEsV0FYVjtBQUFBLFFBWVVDLFVBWlYscUJBWVVBLFVBWlY7QUFBQSxRQWFVQyw0QkFiVixxQkFhVUEsNEJBYlY7QUFBQSxRQWNVQyxnQkFkVixxQkFjVUEsZ0JBZFY7QUFBQSxRQWVVQyxlQWZWLHFCQWVVQSxlQWZWO0FBQUEsUUFnQlVDLG1CQWhCVixxQkFnQlVBLG1CQWhCVjtBQUFBLFFBaUJVQyxrQkFqQlYscUJBaUJVQSxrQkFqQlY7QUFBQSxRQWtCVUMscUJBbEJWLHFCQWtCVUEscUJBbEJWO0FBQUEsUUFtQlVDLG1CQW5CVixxQkFtQlVBLG1CQW5CVjtBQUFBLFFBb0JVQyxvQkFwQlYscUJBb0JVQSxvQkFwQlY7QUFBQSxRQXFCVUMsTUFyQlYscUJBcUJVQSxNQXJCVjtBQXdCQSxXQUNVLHNDQUNJO0FBQ0ksZUFBUyxFQUFDLGVBRGQ7QUFFSSxtQkFBV3RCLFNBQVMsR0FBR0UsU0FBSCxHQUFlckIsSUFGdkM7QUFHSSxvQkFBWXlCLEtBSGhCO0FBSUkscUJBQWFDLE1BSmpCO0FBS0ksNkJBQXFCRSxjQUx6QjtBQU1JLDhCQUFzQkMsZUFOMUI7QUFPSSxrQ0FBMEJDLG1CQVA5QjtBQVFJLDBCQUFrQkMsV0FSdEI7QUFTSSx5QkFBaUJDLFVBVHJCO0FBVUksMkNBQW1DQyw0QkFWdkM7QUFXSSwrQkFBdUJDLGdCQVgzQjtBQVlJLDhCQUFzQkMsZUFaMUI7QUFhSSxrQ0FBMEJDLG1CQWI5QjtBQWNJLGlDQUF5QkMsa0JBZDdCO0FBZUksb0NBQTRCQyxxQkFmaEM7QUFnQkksa0NBQTBCQyxtQkFoQjlCO0FBaUJJLG1DQUEyQkMsb0JBakIvQjtBQWtCSSxnQ0FBd0JiLGlCQWxCNUI7QUFtQkkscUJBQWFjLE1BbkJqQjtBQW9CSSxXQUFLLEVBQUU7QUFDSGhCLGFBQUssWUFBS0EsS0FBTDtBQURGO0FBcEJYLE1BREosQ0FEVjtBQTRCQTtBQS9KdUMsQ0FBeEIsQ0FBakIsQzs7Ozs7Ozs7Ozs7QUNqUkEseUM7Ozs7Ozs7Ozs7O0FDQUEseUMiLCJmaWxlIjoiYmxvY2tzLmJ1aWxkLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9ibG9ja3Mvc3JjL2Jsb2Nrcy5qc1wiKTtcbiIsImNvbnN0IHsgSW5zcGVjdG9yQ29udHJvbHMgfSA9IHdwLmVkaXRvcjtcclxuY29uc3QgeyByZWdpc3RlckJsb2NrVHlwZSB9ID0gd3AuYmxvY2tzO1xyXG5jb25zdCB7IF9fIH0gPSB3cC5pMThuO1xyXG5jb25zdCB7IENvbXBvbmVudCwgRnJhZ21lbnQgfSA9IHdwLmVsZW1lbnQ7XHJcbmNvbnN0IHsgUGFuZWxCb2R5LCBUZXh0Q29udHJvbCwgUmFuZ2VDb250cm9sLCBDaGVja2JveENvbnRyb2wsIFRvZ2dsZUNvbnRyb2wsIFNlbGVjdENvbnRyb2wgfSA9IHdwLmNvbXBvbmVudHM7XHJcbmNvbnN0IHsgd2l0aFNlbGVjdCB9ID0gd3AuZGF0YTtcclxuXHJcbmNvbnN0IGJsb2NrSWNvbiA9ICgpID0+IHtcclxuXHRyZXR1cm4gKFxyXG5cdFx0PHN2ZyB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCIgaGVpZ2h0PVwiNDBcIiB2aWV3Qm94PVwiMCAwIDQ5IDI4XCIgZmlsbD1cIm5vbmVcIj5cclxuICAgICAgICAgICAgPHBhdGggZmlsbC1ydWxlPVwiZXZlbm9kZFwiIGNsaXAtcnVsZT1cImV2ZW5vZGRcIiBkPVwiTTM0LjQ3NTcgMjIuMDYxNFYxNy4yOTQxTDQzLjAzMjMgMjMuNDA2MUM0My41MzYxIDIzLjc2NTkgNDQuMTk4NyAyMy44MTQgNDQuNzQ5MSAyMy41MzA3QzQ1LjI5OTYgMjMuMjQ3NCA0NS42NDU1IDIyLjY4MDMgNDUuNjQ1NSAyMi4wNjEyVjUuNTM0OTJDNDUuNjQ1NSA0LjkxNTg3IDQ1LjI5OTYgNC4zNDg3MyA0NC43NDkxIDQuMDY1NDVDNDQuMTk4NyAzLjc4MjE5IDQzLjUzNjEgMy44MzAzIDQzLjAzMjMgNC4xOTAxMkwzNC40NzU3IDEwLjMwMjFWNS41MzUwNEMzNC40NzU3IDIuNjE3NDEgMzEuODc4NCAwLjU3NzE0OCAyOS4wOTk4IDAuNTc3MTQ4SDguNjIyMzlDNS44NDM4NyAwLjU3NzE0OCAzLjI0NjU4IDIuNjE3NDEgMy4yNDY1OCA1LjUzNTA0VjIyLjA2MTRDMy4yNDY1OCAyNC45NzkgNS44NDM4NyAyNy4wMTkzIDguNjIyMzkgMjcuMDE5M0gyOS4wOTk4QzMxLjg3ODQgMjcuMDE5MyAzNC40NzU3IDI0Ljk3OSAzNC40NzU3IDIyLjA2MTRaTTIwLjMzMTYgMTguMTc1OUMxNy44MjMyIDE2Ljg5MDYgMTUuNzY2OCAxNC44NDMxIDE0LjQ5MDQgMTIuMzM0N0wxNi40NDA0IDEwLjM4NDdDMTYuNjg4NiAxMC4xMzY1IDE2Ljc1OTYgOS43OTA4MSAxNi42NjIxIDkuNDgwNTlDMTYuMzM0MSA4LjQ4Nzg0IDE2LjE1NjggNy40MjQyMSAxNi4xNTY4IDYuMzE2MjdDMTYuMTU2OCA1LjgyODc2IDE1Ljc1OCA1LjQyOTkgMTUuMjcwNCA1LjQyOTlIMTIuMTY4MUMxMS42ODA3IDUuNDI5OSAxMS4yODE4IDUuODI4NzYgMTEuMjgxOCA2LjMxNjI3QzExLjI4MTggMTQuNjM5MyAxOC4wMjcgMjEuMzg0NSAyNi4zNSAyMS4zODQ1QzI2LjgzNzUgMjEuMzg0NSAyNy4yMzY0IDIwLjk4NTYgMjcuMjM2NCAyMC40OTgxVjE3LjQwNDdDMjcuMjM2NCAxNi45MTcyIDI2LjgzNzUgMTYuNTE4MyAyNi4zNSAxNi41MTgzQzI1LjI1MDkgMTYuNTE4MyAyNC4xNzg0IDE2LjM0MSAyMy4xODU3IDE2LjAxMzFDMjIuODc1NSAxNS45MDY4IDIyLjUyMDkgMTUuOTg2NSAyMi4yODE2IDE2LjIyNThMMjAuMzMxNiAxOC4xNzU5Wk0yNS44NjI1IDUuNDIxMDNMMjYuNDkxOCA2LjA0MTQ5TDIwLjg5ODkgMTEuNjM0NUgyNC41NzczVjEyLjUyMDlIMTkuMjU5MVY3LjIwMjY0SDIwLjE0NTVWMTEuMDA1MUwyNS44NjI1IDUuNDIxMDNaXCIgZmlsbD1cIiM0MDdCRkZcIi8+XHJcbiAgICAgICAgPC9zdmc+XHJcblx0KVxyXG59XHJcblxyXG5pbXBvcnQgJy4vZWRpdG9yLnNjc3MnO1xyXG5pbXBvcnQgJy4vc3R5bGUuc2Nzcyc7XHJcblxyXG5jbGFzcyBFZGl0QmxvY2sgZXh0ZW5kcyBDb21wb25lbnR7XHJcblx0Y29uc3RydWN0b3IocHJvcHMpIHtcclxuXHRcdHN1cGVyKCBwcm9wcyApO1xyXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XHJcbiAgICAgICAgICAgIHBvc3RBcnI6IFtdXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcclxuICAgICAgICBjb25zdCB7IHNldEF0dHJpYnV0ZXMsIGF0dHJpYnV0ZXM6IHsgbmFtZSwgZnJvbUdsb2JhbCB9IH0gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgIGNvbnN0IF9uZXdOYW1lID0gTWF0aC5yYW5kb20oKS50b1N0cmluZygzNikuc3Vic3RyaW5nKDIsIDE1KTtcclxuICAgICAgICBpZiAoICFuYW1lICkge1xyXG4gICAgICAgICAgICBzZXRBdHRyaWJ1dGVzKHsgbmFtZTogX25ld05hbWUgfSk7XHJcbiAgICAgICAgfSAgICAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICB0b2dnbGVGcm9tUG9zdCgpe1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLnBvc3RzICYmIHRoaXMuc3RhdGUucG9zdEFyci5sZW5ndGggPCAxKSB7XHJcbiAgICAgICAgICAgIGxldCBvcHRpb25zID0gW107XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMucG9zdHMuZm9yRWFjaCgocG9zdCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgb3B0aW9ucy5wdXNoKFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHBvc3QuaWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsOiBwb3N0LnRpdGxlLnJlbmRlcmVkXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtwb3N0QXJyOiBvcHRpb25zfSlcclxuICAgICAgICB9IFxyXG4gICAgICAgIGNvbnN0IHsgc2V0QXR0cmlidXRlcywgYXR0cmlidXRlczogeyBmb3JtUG9zdHMsIHBvc3RJZCB9IH0gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgIHNldEF0dHJpYnV0ZXMoeyBmb3JtUG9zdHM6ICFmb3JtUG9zdHMgfSk7XHJcbiAgICAgICAgaWYoIXBvc3RJZCAmJiB0aGlzLnByb3BzLnBvc3RzLmxlbmd0aCA+IDApe1xyXG4gICAgICAgICAgICBzZXRBdHRyaWJ1dGVzKHsgcG9zdElkOiB0aGlzLnByb3BzLnBvc3RzWzBdLmlkLCBwb3N0VGl0bGU6IHRoaXMucHJvcHMucG9zdHNbMF0udGl0bGUucmVuZGVyZWQgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByZXZpZXdNb2NrKCl7XHJcbiAgICAgICAgcmV0dXJuKFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImppdHNpLXByZXZpZXctcGVvcGxlLW1vY2tcIj5cclxuICAgICAgICAgICAgICAgIDxkaXY+PGRpdiBzdHlsZT17e2JhY2tncm91bmRJbWFnZTogYHVybCgke2ppdHNpX3Byby5wbHVnaW5fdXJsfWFzc2V0cy9pbWcvMDEucG5nKWB9fT48L2Rpdj48L2Rpdj4gIFxyXG4gICAgICAgICAgICAgICAgPGRpdj48ZGl2IHN0eWxlPXt7YmFja2dyb3VuZEltYWdlOiBgdXJsKCR7aml0c2lfcHJvLnBsdWdpbl91cmx9YXNzZXRzL2ltZy8wMi5wbmcpYH19PjwvZGl2PjwvZGl2PiAgXHJcbiAgICAgICAgICAgICAgICA8ZGl2PjxkaXYgc3R5bGU9e3tiYWNrZ3JvdW5kSW1hZ2U6IGB1cmwoJHtqaXRzaV9wcm8ucGx1Z2luX3VybH1hc3NldHMvaW1nLzAzLnBuZylgfX0+PC9kaXY+PC9kaXY+ICBcclxuICAgICAgICAgICAgICAgIDxkaXY+PGRpdiBzdHlsZT17e2JhY2tncm91bmRJbWFnZTogYHVybCgke2ppdHNpX3Byby5wbHVnaW5fdXJsfWFzc2V0cy9pbWcvMDQucG5nKWB9fT48L2Rpdj48L2Rpdj4gIFxyXG4gICAgICAgICAgICAgICAgPGRpdj48ZGl2IHN0eWxlPXt7YmFja2dyb3VuZEltYWdlOiBgdXJsKCR7aml0c2lfcHJvLnBsdWdpbl91cmx9YXNzZXRzL2ltZy8wNS5wbmcpYH19PjwvZGl2PjwvZGl2PiAgXHJcbiAgICAgICAgICAgICAgICA8ZGl2PjxkaXYgc3R5bGU9e3tiYWNrZ3JvdW5kSW1hZ2U6IGB1cmwoJHtqaXRzaV9wcm8ucGx1Z2luX3VybH1hc3NldHMvaW1nLzA2LnBuZylgfX0+PC9kaXY+PC9kaXY+ICBcclxuICAgICAgICAgICAgICAgIDxkaXY+PGRpdiBzdHlsZT17e2JhY2tncm91bmRJbWFnZTogYHVybCgke2ppdHNpX3Byby5wbHVnaW5fdXJsfWFzc2V0cy9pbWcvMDcucG5nKWB9fT48L2Rpdj48L2Rpdj4gIFxyXG4gICAgICAgICAgICAgICAgPGRpdj48ZGl2IHN0eWxlPXt7YmFja2dyb3VuZEltYWdlOiBgdXJsKCR7aml0c2lfcHJvLnBsdWdpbl91cmx9YXNzZXRzL2ltZy8wOC5wbmcpYH19PjwvZGl2PjwvZGl2PiAgXHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIClcclxuICAgIH1cclxuXHJcblx0cmVuZGVyKCl7XHJcblx0XHRjb25zdCB7XHJcblx0XHRcdGF0dHJpYnV0ZXMsXHJcblx0XHRcdHNldEF0dHJpYnV0ZXMsXHJcbiAgICAgICAgICAgIHBvc3RzXHJcbiAgICAgICAgfSA9IHRoaXMucHJvcHM7XHJcbiAgICAgICAgXHJcbiAgICAgICAgY29uc3Qge1xyXG4gICAgICAgICAgICBmb3JtUG9zdHMsXHJcbiAgICAgICAgICAgIHBvc3RJZCxcclxuICAgICAgICAgICAgbmFtZSxcclxuICAgICAgICAgICAgZnJvbUdsb2JhbCxcclxuICAgICAgICAgICAgd2lkdGgsXHJcbiAgICAgICAgICAgIGhlaWdodCxcclxuICAgICAgICAgICAgZW5hYmxld2VsY29tZXBhZ2UsXHJcbiAgICAgICAgICAgIHN0YXJ0YXVkaW9vbmx5LFxyXG4gICAgICAgICAgICBzdGFydGF1ZGlvbXV0ZWQsXHJcbiAgICAgICAgICAgIHN0YXJ0d2l0aGF1ZGlvbXV0ZWQsXHJcbiAgICAgICAgICAgIHN0YXJ0c2lsZW50LFxyXG4gICAgICAgICAgICByZXNvbHV0aW9uLFxyXG4gICAgICAgICAgICBtYXhmdWxscmVzb2x1dGlvbnBhcnRpY2lwYW50LFxyXG4gICAgICAgICAgICBkaXNhYmxlc2ltdWxjYXN0LFxyXG4gICAgICAgICAgICBzdGFydHZpZGVvbXV0ZWQsXHJcbiAgICAgICAgICAgIHN0YXJ0d2l0aHZpZGVvbXV0ZWQsXHJcbiAgICAgICAgICAgIHN0YXJ0c2NyZWVuc2hhcmluZyxcclxuICAgICAgICAgICAgZmlsZXJlY29yZGluZ3NlbmFibGVkLFxyXG4gICAgICAgICAgICB0cmFuc2NyaWJpbmdlbmFibGVkLFxyXG4gICAgICAgICAgICBsaXZlc3RyZWFtaW5nZW5hYmxlZCxcclxuICAgICAgICAgICAgaW52aXRlXHJcbiAgICAgICAgfSA9IGF0dHJpYnV0ZXM7XHJcblxyXG5cdFx0cmV0dXJuKFxyXG5cdFx0XHQ8RnJhZ21lbnQ+XHJcbiAgICAgICAgICAgICAgICA8SW5zcGVjdG9yQ29udHJvbHM+XHJcbiAgICAgICAgICAgICAgICAgICAgPFBhbmVsQm9keSB0aXRsZT17X18oJ1NldHRpbmdzJyl9IGluaXRpYWxPcGVuPXt0cnVlfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPFRvZ2dsZUNvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfXyhcIkZyb20gUG9zdD9cIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkPXtmb3JtUG9zdHN9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KCkgPT4gdGhpcy50b2dnbGVGcm9tUG9zdCgpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7Zm9ybVBvc3RzICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U2VsZWN0Q29udHJvbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfXyhcIlNlbGVjdCBQb3N0XCIpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXsgcG9zdElkIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zPXt0aGlzLnN0YXRlLnBvc3RBcnJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyAoIHZhbCApID0+IHNldEF0dHJpYnV0ZXMoe3Bvc3RJZDogdmFsLCBwb3N0VGl0bGU6IHBvc3RzLmZpbmQob2JqID0+IG9iai5pZCA9PSB2YWwpLnRpdGxlLnJlbmRlcmVkfSkgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7IWZvcm1Qb3N0cyAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRleHRDb250cm9sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdOYW1lJyl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9eyBuYW1lIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17ICggdmFsICkgPT4gc2V0QXR0cmlidXRlcyh7bmFtZTogdmFsfSkgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgPENoZWNrYm94Q29udHJvbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdDb25maWcgZnJvbSBnbG9iYWwnKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9eyBmcm9tR2xvYmFsIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsgdmFsID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRBdHRyaWJ1dGVzKHtmcm9tR2xvYmFsOiB2YWx9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZighZnJvbUdsb2JhbCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNldEF0dHJpYnV0ZXMoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IHBhcnNlSW50KGppdHNpX3Byby5tZWV0aW5nX3dpZHRoKSwgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IHBhcnNlSW50KGppdHNpX3Byby5tZWV0aW5nX2hlaWdodCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbmFibGV3ZWxjb21lcGFnZTogcGFyc2VJbnQoaml0c2lfcHJvLmVuYWJsZXdlbGNvbWVwYWdlKSA/IHRydWUgOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0YXVkaW9vbmx5OiBwYXJzZUludChqaXRzaV9wcm8uc3RhcnRhdWRpb29ubHkpID8gdHJ1ZSA6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhcnRhdWRpb211dGVkOiBwYXJzZUludChqaXRzaV9wcm8uc3RhcnRhdWRpb211dGVkKSA/IHBhcnNlSW50KGppdHNpX3Byby5zdGFydGF1ZGlvbXV0ZWQpIDogMTAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGFydHdpdGhhdWRpb211dGVkOiBwYXJzZUludChqaXRzaV9wcm8uc3RhcnR3aXRoYXVkaW9tdXRlZCkgPyB0cnVlIDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGFydHNpbGVudDogcGFyc2VJbnQoaml0c2lfcHJvLnN0YXJ0c2lsZW50KSA/IHRydWUgOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdXRpb246IHBhcnNlSW50KGppdHNpX3Byby5yZXNvbHV0aW9uKSA/IHBhcnNlSW50KGppdHNpX3Byby5yZXNvbHV0aW9uKSA6IDcyMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heGZ1bGxyZXNvbHV0aW9ucGFydGljaXBhbnQ6IHBhcnNlSW50KGppdHNpX3Byby5tYXhmdWxscmVzb2x1dGlvbnBhcnRpY2lwYW50KSA/IHBhcnNlSW50KGppdHNpX3Byby5tYXhmdWxscmVzb2x1dGlvbnBhcnRpY2lwYW50KSA6IDIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlc2ltdWxjYXN0OiBwYXJzZUludChqaXRzaV9wcm8uZGlzYWJsZXNpbXVsY2FzdCkgPyB0cnVlIDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGFydHZpZGVvbXV0ZWQ6IHBhcnNlSW50KGppdHNpX3Byby5zdGFydHZpZGVvbXV0ZWQpID8gdHJ1ZSA6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhcnR3aXRodmlkZW9tdXRlZDogcGFyc2VJbnQoaml0c2lfcHJvLnN0YXJ0d2l0aHZpZGVvbXV0ZWQpID8gcGFyc2VJbnQoaml0c2lfcHJvLnN0YXJ0d2l0aHZpZGVvbXV0ZWQpIDogMTAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGFydHNjcmVlbnNoYXJpbmc6IHBhcnNlSW50KGppdHNpX3Byby5zdGFydHNjcmVlbnNoYXJpbmcpID8gdHJ1ZSA6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsZXJlY29yZGluZ3NlbmFibGVkOiBwYXJzZUludChqaXRzaV9wcm8uZmlsZXJlY29yZGluZ3NlbmFibGVkKSA/IHRydWUgOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zY3JpYmluZ2VuYWJsZWQ6IHBhcnNlSW50KGppdHNpX3Byby50cmFuc2NyaWJpbmdlbmFibGVkKSA/IHRydWUgOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpdmVzdHJlYW1pbmdlbmFibGVkOiBwYXJzZUludChqaXRzaV9wcm8ubGl2ZXN0cmVhbWluZ2VuYWJsZWQpID8gdHJ1ZSA6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW52aXRlOiBwYXJzZUludChqaXRzaV9wcm8uaW52aXRlKSA/IHRydWUgOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8+ICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgeyFmcm9tR2xvYmFsICYmIChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFJhbmdlQ29udHJvbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17X18oJ1dpZHRoJyl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXsgd2lkdGggfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17ICggdmFsICkgPT4gc2V0QXR0cmlidXRlcyh7d2lkdGg6IHZhbH0pIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWluPXsgMTAwIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF4PXsgMjAwMCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0ZXA9eyAxMCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8UmFuZ2VDb250cm9sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfXygnSGVpZ2h0Jyl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXsgaGVpZ2h0IH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyAoIHZhbCApID0+IHNldEF0dHJpYnV0ZXMoe2hlaWdodDogdmFsfSkgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtaW49eyAxMDAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXg9eyAyMDAwIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RlcD17IDEwIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDaGVja2JveENvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdXZWxjb21lIFBhZ2UnKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17IGVuYWJsZXdlbGNvbWVwYWdlIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyB2YWwgPT4gc2V0QXR0cmlidXRlcyh7ZW5hYmxld2VsY29tZXBhZ2U6IHZhbH0pIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDaGVja2JveENvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdTdGFydCBBdWRpbyBPbmx5Jyl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9eyBzdGFydGF1ZGlvb25seSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsgdmFsID0+IHNldEF0dHJpYnV0ZXMoe3N0YXJ0YXVkaW9vbmx5OiB2YWx9KSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8UmFuZ2VDb250cm9sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfXygnQXVkaW8gTXV0ZWQgQWZ0ZXInKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9eyBzdGFydGF1ZGlvbXV0ZWQgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17ICggdmFsICkgPT4gc2V0QXR0cmlidXRlcyh7c3RhcnRhdWRpb211dGVkOiB2YWx9KSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1pbj17IDAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXg9eyAyMCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0ZXA9eyAxIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDaGVja2JveENvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdZb3Vyc2VsZiBNdXRlZCcpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkPXsgc3RhcnR3aXRoYXVkaW9tdXRlZCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsgdmFsID0+IHNldEF0dHJpYnV0ZXMoe3N0YXJ0d2l0aGF1ZGlvbXV0ZWQ6IHZhbH0pIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDaGVja2JveENvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdTdGFydCBTaWxlbnQnKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17IHN0YXJ0c2lsZW50IH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyB2YWwgPT4gc2V0QXR0cmlidXRlcyh7c3RhcnRzaWxlbnQ6IHZhbH0pIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTZWxlY3RDb250cm9sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfXyhcIlJlc29sdXRpb25cIil9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXsgcmVzb2x1dGlvbiB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM9e1tcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgbGFiZWw6IF9fKCc0ODBwJyksIHZhbHVlOiA0ODAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgbGFiZWw6IF9fKCc3MjBwJyksIHZhbHVlOiA3MjAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgbGFiZWw6IF9fKCcxMDgwcCcpLCB2YWx1ZTogMTA4MCB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBsYWJlbDogX18oJzE0NDBwJyksIHZhbHVlOiAxNDQwIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGxhYmVsOiBfXygnMjE2MHAnKSwgdmFsdWU6IDIxNjAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgbGFiZWw6IF9fKCc0MzIwcCcpLCB2YWx1ZTogNDMyMCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF19XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsgKCB2YWwgKSA9PiBzZXRBdHRyaWJ1dGVzKHsgcmVzb2x1dGlvbjogdmFsIH0pIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxSYW5nZUNvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdNYXggRnVsbCBSZXNvbHV0aW9uJyl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXsgbWF4ZnVsbHJlc29sdXRpb25wYXJ0aWNpcGFudCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsgKCB2YWwgKSA9PiBzZXRBdHRyaWJ1dGVzKHttYXhmdWxscmVzb2x1dGlvbnBhcnRpY2lwYW50OiB2YWx9KSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1pbj17IDAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXg9eyAyMCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0ZXA9eyAxIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDaGVja2JveENvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdTdGFydCBWaWRlbyBNdXRlZCcpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkPXsgc3RhcnR2aWRlb211dGVkIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyB2YWwgPT4gc2V0QXR0cmlidXRlcyh7IHN0YXJ0dmlkZW9tdXRlZDogdmFsIH0pIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxSYW5nZUNvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdWaWRlbyBNdXRlZCBBZnRlcicpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17IHN0YXJ0d2l0aHZpZGVvbXV0ZWQgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17ICggdmFsICkgPT4gc2V0QXR0cmlidXRlcyh7c3RhcnR3aXRodmlkZW9tdXRlZDogdmFsfSkgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtaW49eyAwIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF4PXsgNTAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGVwPXsgMSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q2hlY2tib3hDb250cm9sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfXygnU2NyZWVuIFNoYXJpbmcnKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17IHN0YXJ0c2NyZWVuc2hhcmluZyB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsgdmFsID0+IHNldEF0dHJpYnV0ZXMoeyBzdGFydHNjcmVlbnNoYXJpbmc6IHZhbCB9KSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q2hlY2tib3hDb250cm9sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtfXygnRW5hYmxlIFJlY29yZGluZycpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkPXsgZmlsZXJlY29yZGluZ3NlbmFibGVkIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyB2YWwgPT4gc2V0QXR0cmlidXRlcyh7IGZpbGVyZWNvcmRpbmdzZW5hYmxlZDogdmFsIH0pIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDaGVja2JveENvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdFbmFibGUgVHJhbnNjcmlwdGlvbicpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkPXsgdHJhbnNjcmliaW5nZW5hYmxlZCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsgdmFsID0+IHNldEF0dHJpYnV0ZXMoeyB0cmFuc2NyaWJpbmdlbmFibGVkOiB2YWwgfSkgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPENoZWNrYm94Q29udHJvbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17X18oJ0VuYWJsZSBMaXZlc3RyZWFtJyl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9eyBsaXZlc3RyZWFtaW5nZW5hYmxlZCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsgdmFsID0+IHNldEF0dHJpYnV0ZXMoeyBsaXZlc3RyZWFtaW5nZW5hYmxlZDogdmFsIH0pIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDaGVja2JveENvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdTaW11bGNhc3QnKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17IGRpc2FibGVzaW11bGNhc3QgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17IHZhbCA9PiBzZXRBdHRyaWJ1dGVzKHsgZGlzYWJsZXNpbXVsY2FzdDogdmFsIH0pIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDaGVja2JveENvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e19fKCdFbmFibGUgSW52aXRpbmcnKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17IGludml0ZSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsgdmFsID0+IHNldEF0dHJpYnV0ZXMoeyBpbnZpdGU6IHZhbCB9KSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICApfSAgIFxyXG4gICAgICAgICAgICAgICAgICAgIDwvUGFuZWxCb2R5PiAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICA8L0luc3BlY3RvckNvbnRyb2xzPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBpZD1cIm1lZXRpbmctdWktcHJldmlld1wiIGNsYXNzTmFtZT1cInByZXZpZXctc3VjY2VzcyBwcmV2aWV3LWJsb2NrXCI+XHJcbiAgICAgICAgICAgICAgICAgICAge3RoaXMucHJldmlld01vY2soKX1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L0ZyYWdtZW50PlxyXG5cdFx0KTtcclxuXHR9XHJcbn1cclxuXHJcbnJlZ2lzdGVyQmxvY2tUeXBlKCdqaXRzaS1wcm8vaml0c2ktcHJvJywge1xyXG4gIHRpdGxlOiBfXygnSml0c2kgUHJvJywgJ2ppdHNpLXBybycpLFxyXG4gIGljb246IGJsb2NrSWNvbixcclxuICBjYXRlZ29yeTogJ2VtYmVkJyxcclxuICBrZXl3b3JkczogW1xyXG4gICAgX18oICdqaXRzaScsICdqaXRzaS1wcm8nICksXHJcbiAgICBfXyggJ21lZXRpbmcnLCAnaml0c2ktcHJvJyApLFxyXG4gICAgX18oICd2aWRlbycsICdqaXRzaS1wcm8nICksXHJcbiAgICBfXyggJ2NvbmZlcmVuY2UnLCAnaml0c2ktcHJvJyApLFxyXG4gICAgX18oICd6b29tJywgJ2ppdHNpLXBybycgKVxyXG4gIF0sXHJcbiAgYXR0cmlidXRlczoge1xyXG4gICAgZm9ybVBvc3RzOiB7XHJcbiAgICAgICAgdHlwZTogJ2Jvb2xlYW4nLFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlXHJcbiAgICB9LFxyXG4gICAgcG9zdElkOiB7XHJcbiAgICAgICAgdHlwZTogJ251bWJlcicsXHJcbiAgICAgICAgZGVmYXVsdDogJydcclxuICAgIH0sXHJcbiAgICBwb3N0VGl0bGU6IHtcclxuICAgICAgICB0eXBlOiAnc3RyaW5nJyxcclxuICAgICAgICBkZWZhdWx0OiAnJ1xyXG4gICAgfSxcclxuICAgIG5hbWU6IHtcclxuICAgICAgICB0eXBlOiAnc3RyaW5nJyxcclxuICAgICAgICBkZWZhdWx0OiAnJ1xyXG4gICAgfSxcclxuICAgIHdpZHRoOiB7XHJcbiAgICAgICAgdHlwZTogJ251bWJlcicsXHJcbiAgICAgICAgZGVmYXVsdDogMTA4MFxyXG4gICAgfSxcclxuICAgIGhlaWdodDoge1xyXG4gICAgICAgIHR5cGU6ICdudW1iZXInLFxyXG4gICAgICAgIGRlZmF1bHQ6IDcyMFxyXG4gICAgfSxcclxuICAgIGZyb21HbG9iYWw6IHtcclxuICAgICAgICB0eXBlOiAnYm9vbGVhbicsXHJcbiAgICAgICAgZGVmYXVsdDogZmFsc2VcclxuICAgIH0sXHJcbiAgICBlbmFibGV3ZWxjb21lcGFnZToge1xyXG4gICAgICAgIHR5cGU6ICdib29sZWFuJyxcclxuICAgICAgICBkZWZhdWx0OiB0cnVlXHJcbiAgICB9LFxyXG4gICAgc3RhcnRhdWRpb29ubHk6IHtcclxuICAgICAgICB0eXBlOiAnYm9vbGVhbicsXHJcbiAgICAgICAgZGVmYXVsdDogZmFsc2VcclxuICAgIH0sXHJcbiAgICBzdGFydGF1ZGlvbXV0ZWQ6IHtcclxuICAgICAgICB0eXBlOiAnbnVtYmVyJyxcclxuICAgICAgICBkZWZhdWx0OiAxMFxyXG4gICAgfSxcclxuICAgIHN0YXJ0d2l0aGF1ZGlvbXV0ZWQ6IHtcclxuICAgICAgICB0eXBlOiAnYm9vbGVhbicsXHJcbiAgICAgICAgZGVmYXVsdDogZmFsc2VcclxuICAgIH0sXHJcbiAgICBzdGFydHNpbGVudDoge1xyXG4gICAgICAgIHR5cGU6ICdib29sZWFuJyxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZVxyXG4gICAgfSxcclxuICAgIHJlc29sdXRpb246IHtcclxuICAgICAgICB0eXBlOiAnbnVtYmVyJyxcclxuICAgICAgICBkZWZhdWx0OiA3MjBcclxuICAgIH0sXHJcbiAgICBtYXhmdWxscmVzb2x1dGlvbnBhcnRpY2lwYW50OiB7XHJcbiAgICAgICAgdHlwZTogJ251bWJlcicsXHJcbiAgICAgICAgZGVmYXVsdDogMlxyXG4gICAgfSxcclxuICAgIHN0YXJ0dmlkZW9tdXRlZDoge1xyXG4gICAgICAgIHR5cGU6ICdib29sZWFuJyxcclxuICAgICAgICBkZWZhdWx0OiB0cnVlXHJcbiAgICB9LFxyXG4gICAgc3RhcnR3aXRodmlkZW9tdXRlZDoge1xyXG4gICAgICAgIHR5cGU6ICdudW1iZXInLFxyXG4gICAgICAgIGRlZmF1bHQ6IDEwXHJcbiAgICB9LFxyXG4gICAgc3RhcnRzY3JlZW5zaGFyaW5nOiB7XHJcbiAgICAgICAgdHlwZTogJ2Jvb2xlYW4nLFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlXHJcbiAgICB9LFxyXG4gICAgZmlsZXJlY29yZGluZ3NlbmFibGVkOiB7XHJcbiAgICAgICAgdHlwZTogJ2Jvb2xlYW4nLFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlXHJcbiAgICB9LFxyXG4gICAgdHJhbnNjcmliaW5nZW5hYmxlZDoge1xyXG4gICAgICAgIHR5cGU6ICdib29sZWFuJyxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZVxyXG4gICAgfSxcclxuICAgIGxpdmVzdHJlYW1pbmdlbmFibGVkOiB7XHJcbiAgICAgICAgdHlwZTogJ2Jvb2xlYW4nLFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlXHJcbiAgICB9LFxyXG4gICAgZGlzYWJsZXNpbXVsY2FzdDoge1xyXG4gICAgICAgIHR5cGU6ICdib29sZWFuJyxcclxuICAgICAgICBkZWZhdWx0OiBmYWxzZVxyXG4gICAgfSxcclxuICAgIGludml0ZToge1xyXG4gICAgICAgIHR5cGU6ICdib29sZWFuJyxcclxuICAgICAgICBkZWZhdWx0OiB0cnVlXHJcbiAgICB9XHJcbiAgfSxcclxuICBlZGl0OiB3aXRoU2VsZWN0KChzZWxlY3QpID0+IHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgcG9zdHM6IHNlbGVjdCgnY29yZScpLmdldEVudGl0eVJlY29yZHMoJ3Bvc3RUeXBlJywgJ21lZXRpbmcnLCB7cGVyX3BhZ2U6IC0xfSksXHJcbiAgICB9O1xyXG4gIH0pKEVkaXRCbG9jayksXHJcbiAgc2F2ZTogZnVuY3Rpb24oIHByb3BzICkge1xyXG5cdFx0Y29uc3Qge1xyXG4gICAgICAgICAgICBmb3JtUG9zdHMsXHJcbiAgICAgICAgICAgIHBvc3RJZCxcclxuICAgICAgICAgICAgcG9zdFRpdGxlLFxyXG4gICAgICAgICAgICBuYW1lLFxyXG4gICAgICAgICAgICB3aWR0aCxcclxuICAgICAgICAgICAgaGVpZ2h0LFxyXG4gICAgICAgICAgICBlbmFibGV3ZWxjb21lcGFnZSxcclxuICAgICAgICAgICAgc3RhcnRhdWRpb29ubHksXHJcbiAgICAgICAgICAgIHN0YXJ0YXVkaW9tdXRlZCxcclxuICAgICAgICAgICAgc3RhcnR3aXRoYXVkaW9tdXRlZCxcclxuICAgICAgICAgICAgc3RhcnRzaWxlbnQsXHJcbiAgICAgICAgICAgIHJlc29sdXRpb24sXHJcbiAgICAgICAgICAgIG1heGZ1bGxyZXNvbHV0aW9ucGFydGljaXBhbnQsXHJcbiAgICAgICAgICAgIGRpc2FibGVzaW11bGNhc3QsXHJcbiAgICAgICAgICAgIHN0YXJ0dmlkZW9tdXRlZCxcclxuICAgICAgICAgICAgc3RhcnR3aXRodmlkZW9tdXRlZCxcclxuICAgICAgICAgICAgc3RhcnRzY3JlZW5zaGFyaW5nLFxyXG4gICAgICAgICAgICBmaWxlcmVjb3JkaW5nc2VuYWJsZWQsXHJcbiAgICAgICAgICAgIHRyYW5zY3JpYmluZ2VuYWJsZWQsXHJcbiAgICAgICAgICAgIGxpdmVzdHJlYW1pbmdlbmFibGVkLFxyXG4gICAgICAgICAgICBpbnZpdGVcclxuXHRcdH0gPSBwcm9wcy5hdHRyaWJ1dGVzO1xyXG5cclxuXHRcdHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IFxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImppdHNpLXdyYXBwZXJcIiBcclxuICAgICAgICAgICAgICAgICAgICBkYXRhLW5hbWU9e2Zvcm1Qb3N0cyA/IHBvc3RUaXRsZSA6IG5hbWV9IFxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEtd2lkdGg9e3dpZHRofSBcclxuICAgICAgICAgICAgICAgICAgICBkYXRhLWhlaWdodD17aGVpZ2h0fVxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEtc3RhcnRhdWRpb29ubHk9e3N0YXJ0YXVkaW9vbmx5fVxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEtc3RhcnRhdWRpb211dGVkPXtzdGFydGF1ZGlvbXV0ZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YS1zdGFydHdpdGhhdWRpb211dGVkPXtzdGFydHdpdGhhdWRpb211dGVkfVxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEtc3RhcnRzaWxlbnQ9e3N0YXJ0c2lsZW50fVxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEtcmVzb2x1dGlvbj17cmVzb2x1dGlvbn1cclxuICAgICAgICAgICAgICAgICAgICBkYXRhLW1heGZ1bGxyZXNvbHV0aW9ucGFydGljaXBhbnQ9e21heGZ1bGxyZXNvbHV0aW9ucGFydGljaXBhbnR9XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YS1kaXNhYmxlc2ltdWxjYXN0PXtkaXNhYmxlc2ltdWxjYXN0fVxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEtc3RhcnR2aWRlb211dGVkPXtzdGFydHZpZGVvbXV0ZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YS1zdGFydHdpdGh2aWRlb211dGVkPXtzdGFydHdpdGh2aWRlb211dGVkfVxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEtc3RhcnRzY3JlZW5zaGFyaW5nPXtzdGFydHNjcmVlbnNoYXJpbmd9XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YS1maWxlcmVjb3JkaW5nc2VuYWJsZWQ9e2ZpbGVyZWNvcmRpbmdzZW5hYmxlZH1cclxuICAgICAgICAgICAgICAgICAgICBkYXRhLXRyYW5zY3JpYmluZ2VuYWJsZWQ9e3RyYW5zY3JpYmluZ2VuYWJsZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YS1saXZlc3RyZWFtaW5nZW5hYmxlZD17bGl2ZXN0cmVhbWluZ2VuYWJsZWR9XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YS1lbmFibGV3ZWxjb21lcGFnZT17ZW5hYmxld2VsY29tZXBhZ2V9XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YS1pbnZpdGU9e2ludml0ZX1cclxuICAgICAgICAgICAgICAgICAgICBzdHlsZT17e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogYCR7d2lkdGh9cHhgXHJcbiAgICAgICAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICAgID48L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcblx0XHQpO1xyXG5cdH1cclxufSk7XHJcbiIsIi8vIHJlbW92ZWQgYnkgZXh0cmFjdC10ZXh0LXdlYnBhY2stcGx1Z2luIiwiLy8gcmVtb3ZlZCBieSBleHRyYWN0LXRleHQtd2VicGFjay1wbHVnaW4iXSwic291cmNlUm9vdCI6IiJ9