<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wisdong8_WPRZH');

/** MySQL database username */
define('DB_USER', 'wisdong8_WPRZH');

/** MySQL database password */
define('DB_PASSWORD', 'HYNSCAgWoCB4YS8RC');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '9c5fb4de26ef6a0161137e6e85e9d7aa19b2d151ef19fbbc97ab98ad4ec77d98');
define('SECURE_AUTH_KEY', 'c77c0aca6e00148f5408677967cc0e8c5a405348fa6adb2c646f113d835715a3');
define('LOGGED_IN_KEY', '8773b4abf4897a99029ffa07c16454db0aac9a49f180ea40a29ecc971c14fb25');
define('NONCE_KEY', '839d6e1cdc788f65c5944b4432a001d26094f90ebbb8c96105c4395e60e65032');
define('AUTH_SALT', '933423024013ab5967e945aaa02cd27538829a9b25765800e09fe9044d4e1b80');
define('SECURE_AUTH_SALT', 'e8ec09a8743dabe7692ac01811892005276e825fa8295340e5b2f3d75d09b395');
define('LOGGED_IN_SALT', '612cd39f064062aca3de7d71815b572c5850131df7e6006d1796e5f8a190a21c');
define('NONCE_SALT', '4763522242d06e3f13a39388d8ab0d45433a2e4d28e8ddb635a2a32bbdd9a687');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = '_RZH_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);


// Settings modified by hosting provider
define( 'WP_CRON_LOCK_TIMEOUT', 120   );
define( 'AUTOSAVE_INTERVAL',    300   );
define( 'WP_POST_REVISIONS',    5     );
define( 'EMPTY_TRASH_DAYS',     7     );
define( 'WP_AUTO_UPDATE_CORE',  true  );
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
